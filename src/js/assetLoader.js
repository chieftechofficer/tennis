var assetLoaderInit = function(initializeUIandComms) {

	GDK.notice('assetLoaderAddSounds');
	//We create the assetLoader instance
	/** http://library.odobo.com/docs/current/api.html#GDK.AssetLoader **/
	assetLoader = new GDK.AssetLoader();

	//Load Sounds USING SPRITES as recommended
	var spriteFile = "audio/sprites/sprite";
	var spriteSetup = {
		"sound_1"  :{type:"sfx", start:     0, length: 5800, level: 100, alternate: "audio/individuals/sound_1"},
		"sound_2"  :{type:"sfx", start: 6000, length:  2000, level: 100, alternate: "audio/individuals/sound_2"}
	}
	/** http://library.odobo.com/docs/current/api.html#assetLoader.addSounds **/
	assetLoader.addSounds(spriteFile, spriteSetup);

	//Load Sounds one by one (no recommended)
	var soundFile = "audio/individuals/sound_3";
	var soundSetup = {
		"sound_3"  :{type:"music", start:     0, length: 12000, level: 100, alternate: "audio/individuals/sound_3"}
	}

	assetLoader.addSounds(soundFile, soundSetup);

	var soundFile = "audio/individuals/sound_4";
	var soundSetup = {
		"sound_4"  :{type:"sfx",   start: 0, length:  6000, level: 100, alternate: "audio/individuals/sound_4"}
	}

	assetLoader.addSounds(soundFile, soundSetup);

	//Array of images from assetsArray.js
	var images = imageAssets;


	//Load Images, we store them on arrays, take a look on config.js
	assetLoader.addImages(imageAssets);

	//Load a single font
	/** http://library.odobo.com/docs/current/api.html#assetLoader.addFont **/
	assetLoader.addFont('bigsquaredots', 'fonts/bigsquaredots.ttf');

	//Load multiple fonts
	/** http://library.odobo.ws/docs/current/api.html#assetLoader.addFonts **/
	var fontAssets = [
		{ name: "MinglerTipsy", url: "fonts/MinglerTipsy.ttf" },
		{ name: "ObelixPro-cyr", url: "fonts/ObelixPro-cyr.ttf" }
	];
	assetLoader.addFonts(fontAssets);


	//Asset loader start with the callback
	/* http://library.odobo.com/docs/current/api.html#assetLoader.start */
	assetLoader.start(function(){

		//On the complete function of the loader we initialize the UI and the communications
		initializeUIandComms();

		var imageHTML = "";
		//Adding the images to the html
		for(var i = 0 ; i < imageAssets.length ; i++){
			/** http://library.odobo.com/docs/current/api.html#GDK.game.path **/
			var imagePath = GDK.game.path(imageAssets[i]);
			imageHTML += '<img src="'+ imagePath + '" />';

		}
		document.getElementById("imageAssets").innerHTML =imageHTML;
	});

}