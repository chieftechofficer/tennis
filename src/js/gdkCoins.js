function gdkCoinsValue(){

	var chipValue = document.getElementById("gdkCoinsValueParam").value;
	var html = "<p>"+ "Parameter: "+chipValue + "</p>";
	/** http://library.odobo.com/docs/current/api.html#GDK.coins.value **/
	html += "<p><strong>"+ "GDK.coins.value(" + chipValue + "):</strong> " + GDK.coins.value(chipValue) + "</p>"
	document.getElementById("gdkCoinsValueExample").innerHTML = html;

}

function gdkCoinsFormat(){

	var chipValue = document.getElementById("gdkCoinsFormatParam").value;
	var html = "<p>"+ "Parameter: "+chipValue + "</p>";
	/** http://library.odobo.com/docs/current/api.html#GDK.coins.format **/
	html += "<p><strong>"+ "GDK.coins.format(" + chipValue + "):</strong> " + GDK.coins.format(chipValue) + "</p>"
	document.getElementById("gdkCoinsFormatExample").innerHTML = html;

}