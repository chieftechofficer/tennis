var audio = [];

function loopAudio(i){
	/** http://library.odobo.com/docs/current/api.html#GDK.audio.loop **/
	audio[i] = GDK.audio.loop("sound_"+i);
}

function stopAudio(i){
	/** http://library.odobo.com/docs/current/api.html#soundInstance.stop **/
	audio[i].stop();
}
