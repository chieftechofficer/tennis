//GDK Sample Game Functions

function loadNavigationMenu(){

	//Menu Button Event Listener
	var menuButtons = document.getElementsByClassName('menu');
	for(var m = 0; m < menuButtons.length ; m++){
		menuButtons[m].addEventListener('click', function(){
			GDK.ui.menu();
		});
	}

	var topics = document.getElementsByClassName("page");
	var navigation = document.getElementById("navigation");
	var codeBlock = new Array();

	for(var i = 0; i< topics.length ; i++){

		var tittleTopic = (topics[i].getElementsByTagName("h1")[0]).innerHTML;
		var subPage = topics[i].getElementsByClassName("subPage");
		var subMenu = document.createElement("UL");
		codeBlock[i] = new Array();

		for(var k = 0; k < subPage.length ; k++){
			//per subPage
			//CodeBlock code
			var jsFile = subPage[k].getElementsByClassName("jsFile");
			codeBlock[i][k] = new Array();
			var divCodeBlocks = document.createElement("DIV");
			divCodeBlocks.classList.add("codeBlocksContainer");
			//Navigation Menu code
			var sectionTittle = (subPage[k].getElementsByTagName("h2")[0]).innerHTML;
			var sectionLI = document.createElement("LI");
			sectionLI.innerHTML = sectionTittle;
			showSection(sectionLI,sectionTittle,tittleTopic);
			subMenu.appendChild(sectionLI);

			if(jsFile.length > 0){
				var jsFile = (subPage[k].getElementsByClassName("jsFile")[0]).value;
				var files = jsFile.split(" ");

				subPage[k].appendChild(divCodeBlocks);
				for(var j = 0 ; j < files.length ; j++){
					codeBlock[i][k][j] = document.createElement("PRE");
					codeBlock[i][k][j].classList.add("codeBlock");
					if( j === 0 ){
						codeBlock[i][k][j].classList.add("firstCodeBlock");
					}else{
						codeBlock[i][k][j].classList.add("tailCodeBlock");
					}
					divCodeBlocks.appendChild(codeBlock[i][k][j]);
					writeCodeBlock(codeBlock,k,j,files,i);
				}
			}

		}

		var elementToAdd = document.createElement("DIV");
		elementToAdd.classList.add("menuSection");
		elementToAdd.innerHTML = tittleTopic;
		elementToAdd.addEventListener("click", function(){
			//Change to Expand the Menu List
			changeVisibility(this);
		}, false);
		elementToAdd.appendChild(subMenu);
		navigation.appendChild(elementToAdd);
	}

}

function writeCodeBlock(codeBlock,k,j,files,i){
	var fileRequest = new XMLHttpRequest();
	fileRequest.onload = function(){

		(codeBlock[i][k][j]).innerHTML = escapeHTML(this.responseText);

	};
	var fileName;
	if((files[j]).indexOf(".") >= 0){
		fileName = "/game/"+files[j];
	}else{
		fileName ="/game/js/"+files[j]+".js";
	}

	fileRequest.open("get", fileName, true);
	fileRequest.send();

}

function escapeHTML(string) {

	return	string.replace(/>/g,'&gt;').
			replace(/</g,'&lt;').
			replace(/"/g,'&quot;')

};


function showSection(sectionLI,h2,h1){

	sectionLI.addEventListener("click", function(){
		//Send with all the parameters we need to show this section/subpage
		var topics = document.getElementsByClassName("page");

		for(var i = 0; i< topics.length ; i++){

			var subPage = topics[i].getElementsByClassName("subPage");
			var tittleTopic = (topics[i].getElementsByTagName("h1")[0]).innerHTML;

			if(tittleTopic === h1){
				//SHOW PAGE
				topics[i].style.display = 'block';
				for(var k = 0; k < subPage.length ; k++){
					var sectionTitle = (subPage[k].getElementsByTagName("h2")[0]).innerHTML;
					if(sectionTitle === h2){
						//SHOW SUBPAGE
						subPage[k].style.display = 'block';
					}else{
						//HIDE SUBPAGE
						subPage[k].style.display = 'none';
					}

				}

			}else{
				//HIDE PAGE
				topics[i].style.display = 'none';
			}

		}

		adjustUI();
	}, false);
}

function changeVisibility(element){
	var test = element;
}

function show(element) {
	var indexElement = element.innerHTML[0];
	var ele = document.getElementById('Page'+indexElement);
	if (!ele) {
		alert("no such element");
		return;
	}
	var pages = document.getElementsByClassName('page');
	for(var i = 0; i < pages.length; i++) {
		pages[i].style.display = 'none';
	}
	ele.style.display = 'block';

	adjustUI();
}

function uiSampleGame(){

	GDK.ui.on(GDK.UI.RESIZE, resizeUI = function(){

		adjustUI();

	});
	resizeUI();
}

function adjustUI(){

	var mainHeight = document.getElementById("main").clientHeight;
	var activePage = searchActivePage();
	var activeSubPage = searchActiveSubPage(activePage);

	var playGroundDiv = activeSubPage.getElementsByClassName("playGround")[0];

	var playGroundHeight = playGroundDiv.clientHeight;

	var codeBlocksContainer = activeSubPage.getElementsByClassName("codeBlocksContainer")[0];
	codeBlocksContainer.style.height = (mainHeight - playGroundHeight - 250) + "px";


}

function searchActiveSubPage(activePage){
	var subPages = activePage.getElementsByClassName("subPage");

	for(var i = 0; i < subPages.length ; i++){
		if(subPages[i].style.display === "block"){
			return subPages[i];
		}
	}

	console.log("No active page found, crash!!!")

	return false;
}

function searchActivePage(){
	var pages = document.getElementsByClassName("page");

	for(var i = 0; i < pages.length ; i++){
		if(pages[i].style.display === "block"){
			return pages[i];
		}
	}

	console.log("No active page found, crash!!!")

	return false;
}