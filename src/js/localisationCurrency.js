function localisationCurrency(){

	var floatNumber = document.getElementById("localisationCurrencyNumber").value;
	var html = "<p>"+ "Number: "+floatNumber + "</p>";
	/** http://library.odobo.com/docs/current/api.html#GDK.locale.formatNumber **/
	html += "<p><strong>"+ "GDK.locale.formatNumber(" + floatNumber + "):</strong> " + GDK.locale.formatNumber(floatNumber) + "</p>"
	html += "<p><strong>" + "GDK.locale.formatNumber(" + floatNumber + ",2):</strong> " + GDK.locale.formatNumber(floatNumber,2) +"</p>"

	/** http://library.odobo.com/docs/current/api.html#GDK.locale.formatCurrency **/
	html += "<p><strong>" + "GDK.locale.formatCurrency("+ floatNumber + "):</strong> "  + GDK.locale.formatCurrency(floatNumber) +"</p>"
	html += "<p><strong>" + "GDK.locale.formatCurrency(" + floatNumber + ",true):</strong> " + GDK.locale.formatCurrency(floatNumber,true) + "</p>";
	/** http://library.odobo.com/docs/current/api.html#GDK.locale.getCurrency **/
	html += "<p><strong>" +"GDK.locale.getCurrency():</strong> " + GDK.locale.getCurrency() + "</p>"
	document.getElementById("localisationCurrencyExample").innerHTML = html;

}