/**********************************************
	Assets to load
**********************************************/
var imageAssets = ["images/NotifierLeft.png","images/NotifierRight.png","images/Trophy_Icon.png","images/Chips/Table-chips/Table-chips-10.png","images/Chips/Table-chips/Table-chips-20.png","images/Chips/Table-chips/Table-chips-50.png","images/Chips/Table-chips/Table-chips-100.png","images/Chips/Table-chips/Table-chips-200.png","images/Chips/Table-chips/Table-chips-500.png","images/Chips/Table-chips/Table-chips-1000.png","images/Chips/Table-chips/Table-chips-2000.png","images/Chips/Table-chips/Table-chips-5000.png","images/Chips/Table-chips/Table-chips-10000.png","images/Chips/Table-chips/Table-chips-20000.png","images/Chips/Table-chips/Table-chips-50000.png","images/Chips/Table-chips/Table-chips-100000.png","images/Chips/Table-chips/Table-chips-200000.png","images/Chips/Table-chips/Table-chips-500000.png","images/Chips/Table-chips/Table-chips-1000000.png","images/Chips/Table-chips/Table-chips-10000000.png","images/Chips/Table-chips/Table-chips-100000000.png","images/Chips/Table-chips/Table-chips-200000000.png","images/Chips/Table-chips/Table-chips-500000000.png","images/Chips/Table-chips/Table-chips-1000000000.png","images/Chips/Table-chips/Table-chips-2000000000.png","images/Chips/Table-chips/Table-chips-5000000000.png","images/Chips/Table-chips/Table-chips-10000000000.png","images/Chips/Selector-chips/Selector-chips-10.png","images/Chips/Selector-chips/Selector-chips-20.png","images/Chips/Selector-chips/Selector-chips-50.png","images/Chips/Selector-chips/Selector-chips-100.png","images/Chips/Selector-chips/Selector-chips-200.png","images/Chips/Selector-chips/Selector-chips-500.png","images/Chips/Selector-chips/Selector-chips-1000.png","images/Chips/Selector-chips/Selector-chips-2000.png","images/Chips/Selector-chips/Selector-chips-5000.png","images/Chips/Selector-chips/Selector-chips-10000.png","images/Chips/Selector-chips/Selector-chips-20000.png","images/Chips/Selector-chips/Selector-chips-50000.png","images/Chips/Selector-chips/Selector-chips-100000.png","images/Chips/Selector-chips/Selector-chips-200000.png","images/Chips/Selector-chips/Selector-chips-500000.png","images/Chips/Selector-chips/Selector-chips-1000000.png","images/Chips/Selector-chips/Selector-chips-10000000.png","images/Chips/Selector-chips/Selector-chips-100000000.png","images/Chips/Selector-chips/Selector-chips-200000000.png","images/Chips/Selector-chips/Selector-chips-500000000.png","images/Chips/Selector-chips/Selector-chips-1000000000.png","images/Chips/Selector-chips/Selector-chips-2000000000.png","images/Chips/Selector-chips/Selector-chips-5000000000.png","images/Chips/Selector-chips/Selector-chips-10000000000.png","images/ResultDice/Result-dice-1.png","images/ResultDice/Result-dice-2.png","images/ResultDice/Result-dice-3.png","images/ResultDice/Result-dice-4.png","images/ResultDice/Result-dice-5.png","images/ResultDice/Result-dice-6.png","images/Dice-outcome-bg.png","images/InfoBtn.png","images/Stage_Logo.jpg","images/MenuBtn.png","images/BlackBG.jpg","images/GreyBG.jpg","images/Court.png","images/Court_left_highlight.png","images/Court_right_highlight.png","images/Court_middle_highlight.png","images/Chips/1_down.png","images/Chips/1_up.png","images/Chips/1_active.png","images/Chips/2_down.png","images/Chips/2_up.png","images/Chips/2_active.png","images/Chips/5_down.png","images/Chips/5_up.png","images/Chips/5_active.png","images/Chips/10_up.png","images/Chips/10_down.png","images/Chips/10_active.png","images/Chips/25_down.png","images/Chips/25_up.png","images/Chips/25_active.png","images/Chips/50_up.png","images/Chips/50_active.png","images/Chips/50_down.png","images/BallLeft.png","images/BallLeftOff.png","images/BallLeftSel.png","images/BallRight.png","images/BallRightWin.png","images/BallRightOff.png","images/BallRightSel.png","images/InputBackground.jpg","images/G_btn_hover.png","images/G_btn_down.png","images/G_btn_up.png","images/G_btn_up_dice_rolling.png","images/G_btn_down_m.png","images/G_btn_up_m.png","images/G_btn_up_dice_rolling_m.png","images/Game_buttons/Game_buttons_down.png","images/Game_buttons/Game_buttons_up.png","images/Game_buttons/Game_buttons_hover.png","images/Game_buttons/Game_buttons_red_down.png","images/Game_buttons/Game_buttons_red_up.png","images/Game_buttons/Game_buttons_red_hover.png","images/BallRightAnimV2.png","images/BallLeftAnimDownstate.png","images/BallRightAnimDownstate.png","images/Arrow_left.png","images/Arrow_right.png","images/Arrow_up.png","images/Arrow_down.png","images/BallRightWin.png","images/BallLeftWin.png","images/TableBG.jpg","images/Close_points_history.png","images/Chip_select_up.png","images/Chip_select_down.png","images/Chip_select_up_m.png","images/Chip_select_down_m.png","images/Close_dd_menu.png","images/MeterBG_Left.png","images/MeterBG_Center.png","images/MeterBG_Right.png","images/Control_panel_bg.png","images/PointsHistory.png","images/blank.png","images/StageDice/Stage-dice1.png","images/StageDice/Stage-dice2.png","images/StageDice/Stage-dice3.png","images/StageDice/Stage-dice4.png","images/StageDice/Stage-dice5.png","images/StageDice/Stage-dice6.png","images/G_btn_up_po.png","images/G_btn_hover_po.png","images/G_btn_down_po.png","images/HelpScreens/Game-screen-slide1.jpg","images/HelpScreens/Game-screen-slide2.jpg","images/HelpScreens/Game-screen-slide3.jpg","images/HelpScreens/Game-screen-slide4.jpg","images/HelpScreens/Game-screen-slide5.jpg","images/HelpScreens/Game-screen-slide6.jpg","images/HelpScreens/Game-screen-slide7.jpg","images/HelpScreens/Game-screen-slide8.jpg","images/HelpScreens/Game-screen-slide9.jpg","images/HelpScreens/Game-screen-slide10.jpg","images/SplashScreen-mobile.jpg","images/SplashScreen.jpg","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_04.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_05.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_06.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_07.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_08.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_09.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_010.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_011.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_012.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_013.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_014.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_015.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_016.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_017.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_018.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_019.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_020.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_021.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_022.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_023.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_024.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_025.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_026.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_027.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_028.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_029.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_030.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_031.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_032.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_033.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_034.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_035.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_036.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_037.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_038.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_039.png","images/DiceAnim/ThrowONE_R1/ThrowONE_R1_040.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_04.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_05.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_06.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_07.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_08.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_09.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_010.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_011.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_012.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_013.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_014.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_015.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_016.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_017.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_018.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_019.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_020.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_021.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_022.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_023.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_024.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_025.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_026.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_027.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_028.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_029.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_030.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_031.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_032.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_033.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_034.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_035.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_036.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_037.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_038.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_039.png","images/DiceAnim/ThrowONE_R6/ThrowONE_R6_040.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_04.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_05.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_06.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_07.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_08.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_09.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_010.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_011.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_012.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_013.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_014.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_015.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_016.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_017.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_018.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_019.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_020.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_021.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_022.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_023.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_024.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_025.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_026.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_027.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_028.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_029.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_030.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_031.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_032.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_033.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_034.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_035.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_036.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_037.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_038.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_039.png","images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_040.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_04.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_05.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_06.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_07.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_08.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_09.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_010.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_011.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_012.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_013.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_014.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_015.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_016.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_017.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_018.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_019.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_020.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_021.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_022.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_023.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_024.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_025.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_026.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_027.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_028.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_029.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_030.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_031.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_032.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_033.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_034.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_035.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_036.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_037.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_038.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_039.png","images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_040.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_04.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_05.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_06.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_07.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_08.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_09.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_010.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_011.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_012.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_013.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_014.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_015.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_016.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_017.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_018.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_019.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_020.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_021.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_022.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_023.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_024.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_025.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_026.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_027.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_028.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_029.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_030.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_031.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_032.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_033.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_034.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_035.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_036.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_037.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_038.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_039.png","images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_040.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_04.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_05.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_06.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_07.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_08.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_09.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_010.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_011.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_012.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_013.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_014.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_015.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_016.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_017.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_018.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_019.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_020.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_021.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_022.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_023.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_024.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_025.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_026.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_027.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_028.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_029.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_030.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_031.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_032.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_033.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_034.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_035.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_036.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_037.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_038.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_039.png","images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_040.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_04.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_05.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_06.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_07.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_08.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_09.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_010.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_011.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_012.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_013.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_014.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_015.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_016.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_017.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_018.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_019.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_020.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_021.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_022.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_023.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_024.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_025.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_026.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_027.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_028.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_029.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_030.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_031.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_032.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_033.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_034.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_035.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_036.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_037.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_038.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_039.png","images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_040.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_04.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_05.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_06.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_07.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_08.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_09.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_010.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_011.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_012.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_013.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_014.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_015.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_016.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_017.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_018.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_019.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_020.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_021.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_022.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_023.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_024.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_025.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_026.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_027.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_028.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_029.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_030.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_031.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_032.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_033.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_034.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_035.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_036.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_037.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_038.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_039.png","images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_040.png","images/Bonus/RedChip_Spin/RedChip_Spin_00.png","images/Bonus/RedChip_Spin/RedChip_Spin_01.png","images/Bonus/RedChip_Spin/RedChip_Spin_02.png","images/Bonus/RedChip_Spin/RedChip_Spin_03.png","images/Bonus/RedChip_Spin/RedChip_Spin_04.png","images/Bonus/RedChip_Spin/RedChip_Spin_05.png","images/Bonus/RedChip_Spin/RedChip_Spin_06.png","images/Bonus/RedChip_Spin/RedChip_Spin_07.png","images/Bonus/RedChip_Spin/RedChip_Spin_08.png","images/Bonus/RedChip_Spin/RedChip_Spin_09.png","images/Bonus/RedChip_Spin/RedChip_Spin_010.png","images/Bonus/RedChip_Spin/RedChip_Spin_011.png","images/Bonus/RedChip_Spin/RedChip_Spin_012.png","images/Bonus/RedChip_Spin/RedChip_Spin_013.png","images/Bonus/RedChip_Spin/RedChip_Spin_014.png","images/Bonus/RedChip_Spin/RedChip_Spin_015.png","images/Bonus/RedChip_Spin/RedChip_Spin_016.png","images/Bonus/RedChip_Spin/RedChip_Spin_017.png","images/Bonus/RedChip_Spin/RedChip_Spin_018.png","images/Bonus/RedChip_Spin/RedChip_Spin_019.png","images/Bonus/RedChip_Spin/RedChip_Spin_020.png","images/Bonus/RedChip_Spin/RedChip_Spin_021.png","images/Bonus/RedChip_Spin/RedChip_Spin_022.png","images/Bonus/RedChip_Spin/RedChip_Spin_023.png","images/Bonus/RedChip_Spin/RedChip_Spin_024.png","images/Bonus/RedChip_Spin/RedChip_Spin_025.png","images/Bonus/RedChip_Spin/RedChip_Spin_026.png","images/Bonus/RedChip_Spin/RedChip_Spin_027.png","images/Bonus/RedChip_Spin/RedChip_Spin_028.png","images/Bonus/RedChip_Spin/RedChip_Spin_029.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_00.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_01.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_02.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_03.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_04.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_05.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_06.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_07.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_08.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_09.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_010.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_011.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_012.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_013.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_014.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_015.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_016.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_017.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_018.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_019.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_020.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_021.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_022.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_023.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_024.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_025.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_026.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_027.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_028.png","images/Bonus/BlueChip_Spin/BlueChip_Spin_029.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_00.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_01.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_02.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_03.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_04.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_05.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_06.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_07.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_08.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_09.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_010.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_011.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_012.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_013.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_014.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_015.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_016.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_017.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_018.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_019.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_020.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_021.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_022.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_023.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_024.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_025.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_026.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_027.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_028.png","images/Bonus/GreenChip_Spin/GreenChip_Spin_029.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_00.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_01.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_02.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_03.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_04.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_05.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_06.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_07.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_08.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_09.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_010.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_011.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_012.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_013.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_014.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_015.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_016.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_017.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_018.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_019.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_020.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_021.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_022.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_023.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_024.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_025.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_026.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_027.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_028.png","images/Bonus/PurpleChip_Spin/PurpleChip_Spin_029.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_00.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_01.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_02.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_03.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_04.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_05.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_06.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_07.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_08.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_09.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_010.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_011.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_012.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_013.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_014.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_015.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_016.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_017.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_018.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_019.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_020.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_021.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_022.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_023.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_024.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_025.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_026.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_027.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_028.png","images/Bonus/YellowChip_Spin/YellowChip_Spin_029.png","images/Bonus/Dice_Spin/Dice_Spin_00.png","images/Bonus/Dice_Spin/Dice_Spin_00.png","images/Bonus/Dice_Spin/Dice_Spin_00.png","images/Bonus/Dice_Spin/Dice_Spin_00.png","images/Bonus/Dice_Spin/Dice_Spin_01.png","images/Bonus/Dice_Spin/Dice_Spin_02.png","images/Bonus/Dice_Spin/Dice_Spin_03.png","images/Bonus/Dice_Spin/Dice_Spin_04.png","images/Bonus/Dice_Spin/Dice_Spin_05.png","images/Bonus/Dice_Spin/Dice_Spin_06.png","images/Bonus/Dice_Spin/Dice_Spin_07.png","images/Bonus/Dice_Spin/Dice_Spin_08.png","images/Bonus/Dice_Spin/Dice_Spin_09.png","images/Bonus/Dice_Spin/Dice_Spin_010.png","images/Bonus/Dice_Spin/Dice_Spin_011.png","images/Bonus/Dice_Spin/Dice_Spin_012.png","images/Bonus/Dice_Spin/Dice_Spin_013.png","images/Bonus/Dice_Spin/Dice_Spin_014.png","images/Bonus/Dice_Spin/Dice_Spin_015.png","images/Bonus/Dice_Spin/Dice_Spin_016.png","images/Bonus/Dice_Spin/Dice_Spin_017.png","images/Bonus/Dice_Spin/Dice_Spin_018.png","images/Bonus/Dice_Spin/Dice_Spin_019.png","images/Bonus/Dice_Spin/Dice_Spin_020.png","images/Bonus/Dice_Spin/Dice_Spin_021.png","images/Bonus/Dice_Spin/Dice_Spin_022.png","images/Bonus/Dice_Spin/Dice_Spin_023.png","images/Bonus/Dice_Spin/Dice_Spin_024.png","images/Bonus/Dice_Spin/Dice_Spin_025.png","images/Bonus/Dice_Spin/Dice_Spin_026.png","images/Bonus/Dice_Spin/Dice_Spin_027.png","images/Bonus/Dice_Spin/Dice_Spin_028.png","images/Bonus/Dice_Spin/Dice_Spin_029.png","images/Bonus/Dice_Spin/Dice_Spin_030.png","images/Bonus/Dice_Spin/Dice_Spin_031.png","images/Bonus/Dice_Spin/Dice_Spin_032.png","images/Bonus/Dice_Spin/Dice_Spin_033.png","images/Bonus/Dice_Spin/Dice_Spin_034.png","images/Bonus/Dice_Spin/Dice_Spin_035.png","images/Bonus/Dice_Spin/Dice_Spin_036.png","images/Bonus/Dice_Spin/Dice_Spin_037.png","images/Bonus/Dice_Spin/Dice_Spin_038.png","images/Bonus/Dice_Spin/Dice_Spin_039.png"];
	
/**********************************************
	Audio
**********************************************/
	
var spriteFile = "audio/sprites/sfxsprite";
	
var spriteData = {
	"singlechip": {
		type: "sfx", start: 25, length: 100, level: 100, alternate: "audio/individuals/singlechip"
	},
	"stackedchip": {
		type: "sfx", start: 1150, length: 200, level: 100, alternate: "audio/individuals/Stackchip"
	},
	"sensorhover": {
		type: "sfx", start: 2200, length: 200, level: 100, alternate: "audio/individuals/Sensor_hover"
	},
	"sensorwin": {
		type: "sfx", start: 66380, length: 1200, level: 100, alternate: "audio/individuals/Win_chyme"
	},
	"sensorloose": {
		type: "sfx", start: 13120, length: 400, level: 100, alternate: "audio/individuals/Pop"
	},
	"diceroll": {
		type: "sfx", start: 14300, length: 701, level: 100, alternate: "audio/individuals/Dice_roll"
	},
	"applause": {
		type: "sfx", start: 3290, length: 4400, level: 100, alternate: "audio/individuals/Applause"
	},
	"mainloop": {
		type: "music", start: 16235, length: 46814, level: 20, alternate: "audio/individuals/Backgroundloop_mixdown"
	}
}

var initialise = function()
{
	
	// Create a loading DOM structure, to which your custom CSS styling is applied 
	var loadingDiv = document.createElement('div');
	loadingDiv.setAttribute("id", "preLoader");
		 
	// Add the div to the game 
	document.getElementById('gameBG').appendChild(loadingDiv);
		 
	// Apply the class to the preloader
	document.getElementById('preLoader').classList.add("loading-screen");
		 
	// Create progress bar div
	var progressBar = document.createElement('div');
	progressBar.setAttribute("id", "progressBar");
		 
	// Add the progress bar to the preloader div
	document.getElementById('preLoader').appendChild(progressBar);
		 
	// Apply the class to the progress bar
	document.getElementById('progressBar').classList.add("loading-bar");
		 
	// Create a div for the progress
	var progress = document.createElement('div');
	progress.setAttribute("id", "progress");
		 
	// Add the progress to the progress bar div
	document.getElementById('progressBar').appendChild(progress);
	 
	// Apply the class to the progress bar
	document.getElementById('progress').classList.add("loading-progress");
		
	// create an asset loader with true passed to the constructor 
	var assetLoader = new GDK.AssetLoader(true); 
		
	// Override the GDK loading UI
	// Add assets
	// Loader
	assetLoader = new GDK.AssetLoader();
	// Set sound assets
	assetLoader.addSounds(spriteFile, spriteData);
	// Set image assets
	assetLoader.addImages(imageAssets);
		
	/**********************************************
		Odobo GDK
	**********************************************/
		
	// Orientation
	GDK.ui.setSupportedOrientations(GDK.UI.LANDSCAPE);
	
	// Initiate gdk and comms
	GDK.game.init.ui();
			
	// Initiate GDK communications
	GDK.game.init.comms();
	
	// Start the asset loader 
	assetLoader.start(_ready, _onProgress); 
		
	assetLoader = null;
		
},handleBalanceEvent = function(packet, control) {
	//console.log("Packet: " + packet);
	balance = packet.balance;
	control.done();	
}, handleGameConfigEvent = function(packet, control) {
	/*for (var i = 0; i < 6; i++) 
	{
		console.log("packet.coinSizes[i]: " + packet.coinSizes[i]);
		chip_values.push(packet.coinSizes[i]);
	}*/
    current_chip=chip_values[0];
	control.done();
}, handleIsAliveEvent = function(packet, control) {
	control.done();
}, handleBonusEvent = function(packet, control) {
	if (packet.bonusWon) 
	{
		bonus = true;
		bonus_amount = packet.bonusWinnings;
		total_game_winnings += bonus_amount;
	}
	control.done();
}, handleRollEvent = function(packet, control) {
	
	var _dr = [];
	
	var _stc = 0;
					
	for (i=0;i<5;i++)
	{	
		// Set the total game winnings
		total_game_winnings += packet.results[i].totalWinnings;
			
		for(j=0;j<packet.results[i].bets.length;j++)
		{
			if(packet.results[i].bets[j].winAmount>0){
				var _s = packet.results[i].bets[j].identifier;
				var _res = _s.replace("SPOT_", "");
					
				winning_sensor_switch[_res-1] = 1;
			}
		}
	
		/************************************************
			Set the dice arrays 
		************************************************/
		
		if(packet.results[i].diceResult.winnerSpot!=null) 
		{	
			var n = packet.results[i].diceResult.winnerSpot.replace("SPOT_", ""); 
			
			if(packet.results[i].roundName.indexOf("STAGE_1")>-1)
			{
				engine_dice_result[1] = dice_sensor_map[n];
			}
				
			if(packet.results[i].roundName.indexOf("STAGE_2")>-1)
			{    
				engine_dice_result[2] = dice_sensor_map[n];
			}
				
			if(packet.results[i].roundName.indexOf("STAGE_3")>-1)
			{		
				engine_dice_result[3] = dice_sensor_map[n];
			}
		}
			
		/**************************************************
			Capture stake for each stage
		**************************************************/
			
		if(packet.results[i].roundName.indexOf("STAGE_1")>-1)
		{
			stage_stake[0]+=packet.results[i].totalBet;
			stage_wins[0] = stage_wins[0]+packet.results[i].totalWinnings;
		}
				
		if(packet.results[i].roundName.indexOf("STAGE_2")>-1)
		{    
			stage_stake[1]+=packet.results[i].totalBet;
			stage_wins[1] = stage_wins[1] + packet.results[i].totalWinnings;
		}
				
		if(packet.results[i].roundName.indexOf("STAGE_3")>-1)
		{
			stage_stake[2]+=packet.results[i].totalBet;
			stage_wins[2] = stage_wins[2] + packet.results[i].totalWinnings;
		}
		
		/***************************************************
			Capture the win amount for each stage
		***************************************************/
	}
		
	// Determine the stages which will be rolled by using the selected_dice_array
	playableStages(engine_dice_result);
								
	// Now launch the dice
	launchDice();
		
	// updateDicesAndWinningSpots(packet.results);
	control.done();
	
}, handleDiceHistoryEvent = function(packet, control) {
	var pointHistory = packet.diceHistory.elements;
	updatePointHistory(pointHistory);
	control.done();
};
	
// BOOTSTRAP
GDK.ready(initialise);
GDK.game.subscribe("sportshotz:IsAliveEvent", handleIsAliveEvent);
GDK.game.subscribe("sportshotz:RollEvent", handleRollEvent);
GDK.game.subscribe("sportshotz:BonusEvent", handleBonusEvent);
GDK.game.subscribe("sportshotz:GameConfigEvent", handleGameConfigEvent);
GDK.game.subscribe("sportshotz:DiceHistoryEvent", handleDiceHistoryEvent);
GDK.game.subscribe("common:BalanceEvent", handleBalanceEvent);

function _onProgress(progress)
{ 
	 // progress is the current % of assets loaded 
	 // progress can be used to determine the length of a loading bar, etc.
	document.getElementById('progress').setAttribute("style","width:" + progress + "%");
}

function _ready()
{ 
 
	// Remove the loading div
	var div = document.getElementById("preLoader");
	div.parentNode.removeChild(div);
	 
	/*********************************************
		Screen size and resolution
	**********************************************/
	
	// Save the screen height and width 
	winHeight = window.innerHeight;
	winWidth = window.innerWidth;
	
	// Use this to place objects
	winXfactor = (winWidth/2)+152.5;
	winYfactor = winHeight;

	// Save the device pixel ratio
	if(window.devicePixelRatio !== undefined)
		dpr = window.devicePixelRatio;
	else
		dpr = 1;
	
	/*********************************************
		PIXI
	**********************************************/
	
	// Create a renderer instance
	renderer = PIXI.autoDetectRenderer(winWidth, winHeight, null, true);
	// Create an new instance of a pixi stage
	stage = new PIXI.Stage(0x97c56e, true);
	// Set stage interactivity
	stage.interactive = true;
	// Add the renderer view element to the DOM
	document.getElementById('gameBG').appendChild(renderer.view);
	// Setup PIXI anim method
	function animate() {
		requestAnimFrame( animate );
		// Render the stage   
		renderer.render(stage);
	}
	// Start the PIXI engine. Vavavoom!!!!
	animate();
	
	/*********************************************
		Load webfonts
	**********************************************/
	WebFontConfig = {
		  google: {
		families: [ 'Comfortaa:700', 'Exo:700', 'Varela Round:400', 'Raleway:900', 'Monda:700', 'Orbitron:900', 'Concert One:400' ]
		  },
		  active: function() 
		  { 
		// Once everything has been loaded initialize the game
		init(winXfactor,winYfactor);
	  }
	};
	(function() {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
	
	/**********************************************
		Background loop
	**********************************************/
	
	// Start background tune
	mainLoop = GDK.audio.loop("mainloop");
	
	/**********************************************
		Game resume
	**********************************************/
	
	// Setup stage listener for game resume
	// This will fire off when the game is
	// resumed after the Odobo menu has been opened 
	GDK.on(GDK.EVENT.RESUME_GAME, resume);
	// Resume listener method
	function resume() 
	{
		// Start background tune
		mainLoop = GDK.audio.loop("mainloop");
	}
	
	/********************************************
		Game reconnection
	********************************************/
	// Setup stage listener for game reconnection
	// This will fire off when the game is
	// reconnected after a player has been disconnected
	GDK.on(GDK.EVENT.RESET_GAME, reconnect);
	// Reconnect listener method
	function reconnect()
	{	
		showNotice(winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px",(winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px","Welcome back! We noticed you lost connection but to view the last result please check the scoreboard. Good luck!",5000);
		
		// Reset the game 
		executeReconnect();
	}
	
	/*********************************************
		Odobo settings menu
	**********************************************/
	
	/*
		Intro
	*/
	
	// Add control to hide intro at the start
	GDK.settings.game.add("hideIntro",GDK.locale.get("g_hide_intro_control_title"), GDK.locale.get("g_hide_intro_control_description"));
	// Setup listener
	GDK.on(GDK.EVENT.SETTING_CHANGED, hideIntroSetting);
	// Listener method
	function hideIntroSetting()
	{
		// Check hide intro switch
		if(GDK.settings.game.get("hideIntro"))
		{
			hideIntro(true); // true (default)
		} else {
			hideIntro(false); 
		}
	}
	
	/*
		Points history
	*/
	
	// Add control to hide the pointshistory 
	GDK.settings.game.add("hidePointsHistory",GDK.locale.get("g_ph_control_title"), GDK.locale.get("g_ph_control_description"));
	// Setup listener
	GDK.on(GDK.EVENT.SETTING_CHANGED, displayPH);
	// Listener method
	function displayPH()
	{
		// Check fractional decimal switch
		if(GDK.settings.game.get("hidePointsHistory"))
		{
			showHidePH(true); // true (default)
		} else {
			showHidePH(false); // false
		}
	}
	
	/*
		Stage label
	*/
	
	// Add control for the Fractional/Decimal odds stage labels 
	GDK.settings.game.add("labelOddsDisplay",GDK.locale.get("g_stage_label_control_title"), GDK.locale.get("g_stage_label_control_description"));
	// Setup listener
	GDK.on(GDK.EVENT.SETTING_CHANGED, oddsChange);
	// Listener method
	function oddsChange()
	{
		// Check fractional decimal switch
		if(GDK.settings.game.get("labelOddsDisplay"))
		{
			setOdds("d"); // Decimal (default)
		} else {
			setOdds("f"); // Fractional
		}
	}
	
	/*********************************************
		Bonus screen
	**********************************************/
	// Hide bonus screen div
	$("#playerNotifier").hide();
}