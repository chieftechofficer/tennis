function scaling(){
	//Game dimensions
	var width = 1440;
	var height = 900;

	//On every resize of the screen we need to scale again, so we listen to the GDK.UI.Resize event using GDK.ui.on
	/** http://library.odobo.com/docs/current/api.html#GDK.ui.on **/
	/** http://library.odobo.com/docs/current/api.html#GDK.UI **/
	GDK.ui.on(GDK.UI.RESIZE, resizeCallback = function(){

		//We get the available height and width of the screen
		/** http://library.odobo.com/docs/current/api.html#GDK.check.availWidth **/
		var scaleW = GDK.check.availWidth()/width;
		/** http://library.odobo.com/docs/current/api.html#GDK.check.availHeight **/
		var scaleH = GDK.check.availHeight()/height;

		var toScale = Math.min(scaleW, scaleH);

		// Now apply toScale to a transform: scale (value) to the main div of your game, transform 2D of CSS3. We use the div “mainGameDiv”
		set_prefixed(document.getElementById('game'),'transform', 'scale(' + toScale + ')');
	});

	//Do the first call for when the game is loaded. (on GDK.ready, the DOM is loaded, so we are ready to do the first scale)
	resizeCallback();
}

//Aux functions to do the transform scale
var set_style = function(elt, style) {
	var st = elt.style;
	for (var p in style)
		st[p] = style[p];
};

var set_prefixed = function(elt, prop, val) {
	var cap_prop = prop[0].toUpperCase() + prop.substr(1);
	var st = { };
	st[prop]                = val;
	st['O' + cap_prop]      = val;
	st['ms' + cap_prop]     = val;
	st['Moz' + cap_prop]    = val;
	st['Webkit' + cap_prop] = val;
	set_style(elt, st);
};