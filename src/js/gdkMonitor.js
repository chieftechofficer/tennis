function gdkMonitor(){

	document.getElementById("gdkMonitorExampleNotice").addEventListener("click",function(){
		var message = document.getElementById("gdkMonitorExampleText").value;
		//Notice a message in the GDK Monitor
		/** http://library.odobo.com/docs/current/api.html#GDK.notice **/
		GDK.notice(message);
	},false);

	document.getElementById("gdkMonitorExampleWarn").addEventListener("click",function(){
		var message = document.getElementById("gdkMonitorExampleText").value;
		//Warm a message in the GDK Monitor
		/** http://library.odobo.com/docs/current/api.html#GDK.warn **/
		GDK.warn(message);
	},false);

	document.getElementById("gdkMonitorExampleError").addEventListener("click",function(){
		var message = document.getElementById("gdkMonitorExampleText").value;
		//Error a message in the GDK Monitor
		/** http://library.odobo.com/docs/current/api.html#GDK.error **/
		GDK.error(message);
	},false);

}
