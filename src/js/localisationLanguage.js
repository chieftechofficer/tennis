function localisationLanguage(){

	//All the text in the games must be localizated, so the text is on the file /src/lang.json using keys for the text and the value in english by default, see lang.json
	/** http://library.odobo.com/docs/current/api.html#GDK.locale.get **/
	var text1 = GDK.locale.get("g_text_1");
	var text2 = GDK.locale.get("g_text_1");
	//Setting the value in the html
	document.getElementById("text1Localisation").innerHTML = text1;
	//Return the string capitalize
	/** http://library.odobo.com/docs/current/api.html#GDK.locale.capitalise **/
	document.getElementById("text2Localisation").innerHTML = GDK.locale.capitalise(text2);

}
