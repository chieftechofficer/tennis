function settings(){
	//All the text in the games must be localizated, so the text is on the file /src/lang.json
	/** http://library.odobo.com/docs/current/api.html#GDK.locale.get **/
	var textMenuOption1 = GDK.locale.get("g_setting_menu_option1");
	var textMenuOption2 = GDK.locale.get("g_setting_menu_option2");

	//We add the setting "menu_option1" with the default value (true), enable
	/** http://library.odobo.com/docs/current/api.html#GDK.settings.game.add **/
	GDK.settings.game.add("menu_option1",textMenuOption1,"this is a description for this menu option");
	//We add the setting "menu_option2" with the value false, disable
	GDK.settings.game.add("menu_option2",textMenuOption2,"this is a description for this menu option",false);

	document.getElementById("alertMenuOption1").addEventListener("click",function(){
		//You read the value using the getter
		/** http://library.odobo.com/docs/current/api.html#GDK.settings.menu.get **/
		var valueMenuOption1 = GDK.settings.game.get("menu_option1");
		//The value will be notice in the GDK Monitor
		/** http://library.odobo.com/docs/current/api.html#GDK.notice **/
		GDK.notice("Menu Option 1: "+valueMenuOption1);
	},false);

	document.getElementById("alertMenuOption2").addEventListener("click",function(){
		//You read the value using the getter, other option
		/** http://library.odobo.com/docs/current/api.html#GDK.settings.menu.get **/
		var valueMenuOption2 = GDK.settings.menu.get(GDK.SETTINGS_PANELS.GAME, "menu_option2");
		//The value will be notice in the GDK Monitor
		/** http://library.odobo.com/docs/current/api.html#GDK.notice **/
		GDK.notice("Menu Option 2: "+valueMenuOption2);
	},false);

	/** http://library.odobo.com/docs/current/api.html#GDK.EVENT **/
	GDK.on(GDK.EVENT.SETTING_CHANGED, function(panel, group, setting){
		GDK.notice("Panel: "+ panel);
		GDK.notice("Group: "+ group);
		GDK.notice("Setting: "+ setting);

	});
}
