function gdkEvents(){
	//WE START OUR GAME (dummy animation in this case with a red circle)
	startGame();
	//GDK EVENTS that are mandatory that the game listen and process
	/** http://library.odobo.com/docs/current/api.html#GDK.EVENT **/
	GDK.on(GDK.EVENT.SUSPEND_GAME, function(a){
		suspendGame();

	});

	GDK.on(GDK.EVENT.RESUME_GAME, function(a){
		resumeGame();

	});

	GDK.on(GDK.EVENT.RESET_GAME, function(){
		resetGame();
	});
}

//DEMO GAME FUNCTIONS
function handleTick() {
	//Circle will move 10 units to the right.
	circle.x += 10;
	//Will cause the circle to wrap back
	if (circle.x > stage.canvas.width) { circle.x = 0; }
	stage.update();
}
//function tu suspend the game
function suspendGame(){
	createjs.Ticker.removeEventListener('tick', handleTick);
	circle.graphics.beginFill("green").drawCircle(0, 0, 40);
}
//function to resume the game
function resumeGame(){
	createjs.Ticker.addEventListener('tick', handleTick);
}

//Function for starting the game
function resetGame(){

	//We should reset the game to it initial state, up to the data of the engine
	circle.x = circle.y = 50;
	circle.graphics.beginFill("yellow").drawCircle(0, 0, 40);

}

function startGame(){
	//Create a stage by getting a reference to the canvas
	stage = new createjs.Stage("demoCanvas");
	//Create a Shape DisplayObject.
	circle = new createjs.Shape();
	circle.graphics.beginFill("red").drawCircle(0, 0, 40);
	//Set position of Shape instance.
	circle.x = circle.y = 50;
	//Add Shape instance to stage display list.
	stage.addChild(circle);
	//Update stage will render next frame
	stage.update();

	//Update stage will render next frame
	createjs.Ticker.addEventListener("tick", handleTick);
}