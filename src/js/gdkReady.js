// initialise when GDK is ready
function initialise(){
	//No assetLoader related, just needed for starting the sample game
	loadNavigationMenu();

	initializeUIandComms = function(){
		//Sample GDK features
		settings();
		scaling();
		gdkMonitor();
		gdkUI();
		gdkGame();
		uiSampleGame();
		gdkEvents();
		localisationLanguage();
		localisationCurrency();
		gdkCoinsValue();
		gdkCoinsFormat();
		//Initialize game UI
		/* http://library.odobo.com/docs/current/api.html#GDK.game.init.ui */
		GDK.game.init.ui();
		//Initialize game API communication
		/* http://library.odobo.com/docs/current/api.html#GDK.game.init.comms */
		GDK.game.init.comms();
		//All that needs to be do after InitComms, example: send data to the server
	}
	assetLoaderInit(initializeUIandComms);
}
//GDK.ready will call to initialise when the GDK is ready and loaded in the browser
/** http://library.odobo.com/docs/current/api.html#GDK.ready **/
GDK.ready(initialise);