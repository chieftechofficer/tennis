function gdkUI(){

	var alertOnOrientationChanged = function(){
		window.alert("GDK.UI.ORIENTATION_CHANGED fired");
	}

	document.getElementById("buttonGDKOn").addEventListener("click",function(){
		/** http://library.odobo.com/docs/current/api.html#GDK.UI **/
		/** http://library.odobo.com/docs/current/api.html#GDK.ui.on **/
		GDK.ui.on(GDK.UI.ORIENTATION_CHANGED, alertOnOrientationChanged);
	});

	document.getElementById("buttonGDKOff").addEventListener("click",function(){
		/** http://library.odobo.com/docs/current/api.html#GDK.ui.off **/
		GDK.ui.off(GDK.UI.ORIENTATION_CHANGED, alertOnOrientationChanged);
	});

}
