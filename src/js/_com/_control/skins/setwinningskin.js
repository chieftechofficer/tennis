/******************************************
	Set winning sensor skin
******************************************/
function setWinningSkin(s)
{			

	// Remove the stacked chips off the sensor
	deletestack(s);
	
	// Set sensor skin
	if(sensor_control_array[s].position.x<(winXfactor+20))
	{
		var _t_ball_win_skin = PIXI.Texture.fromImage("game/images/BallRightWin.png");
		sensor_control_array[s].setTexture(_t_ball_win_skin);
	} 
	else
	{
		var _t_ball_win_skin = PIXI.Texture.fromImage("game/images/BallLeftWin.png");
		sensor_control_array[s].setTexture(_t_ball_win_skin);
	} 
	
	// Check at which stage the game is at and place the trophy
	switch(game_stage)
	{
		case 1:
			trophy_icon1.position.x = sensor_control_array[s].position.x;
			trophy_icon1.position.y = sensor_control_array[s].position.y;
		break;	
		case 2:
			trophy_icon2.position.x = sensor_control_array[s].position.x;
			trophy_icon2.position.y = sensor_control_array[s].position.y;
		break;
		case 3:
			trophy_icon3.position.x = sensor_control_array[s].position.x;
			trophy_icon3.position.y = sensor_control_array[s].position.y;
		break;		
	}
}