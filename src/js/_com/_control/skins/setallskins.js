function setAllSkins()
{
	// Determine which device this is and set skins accordingly
	switch(game_properties.device)
	{
		case "Desktop":
			// Main game button
			game_ind_button_up_skin = PIXI.Texture.fromImage("game/images/G_btn_up.png");
			game_ind_button_hover_skin = PIXI.Texture.fromImage("game/images/G_btn_hover.png");
			game_ind_button_down_skin = PIXI.Texture.fromImage("game/images/G_btn_down.png");
			game_ind_button_dice_rolling = PIXI.Texture.fromImage("game/images/G_btn_up_dice_rolling.png");
			
			game_ind_button_up_po_skin = PIXI.Texture.fromImage("game/images/G_btn_up_po.png");
			game_ind_button_hover_po_skin = PIXI.Texture.fromImage("game/images/G_btn_hover_po.png");
			game_ind_button_down_po_skin = PIXI.Texture.fromImage("game/images/G_btn_down_po.png");
			
			// Import court/field/pitch skins
			court_skin = PIXI.Texture.fromImage("game/images/Court.png");
			court_skin_left_hl = PIXI.Texture.fromImage("game/images/Court_left_highlight.png");
			court_skin_right_hl = PIXI.Texture.fromImage("game/images/Court_right_highlight.png");
			court_skin_mid_hl = PIXI.Texture.fromImage("game/images/Court_middle_highlight.png");
			
			// Game background skin
			bg_stage_skin = PIXI.Texture.fromImage("game/images/TableBG.jpg");
			
			// Meters
			meter_skin_left = PIXI.Texture.fromImage("game/images/MeterBG_Left.png");
			meter_skin_center = PIXI.Texture.fromImage("game/images/MeterBG_Center.png");
			meter_skin_right = PIXI.Texture.fromImage("game/images/MeterBG_Right.png");
			
			// Points history
			ph_bg_skin = PIXI.Texture.fromImage("game/images/PointsHistory.png");
			
			// Stage logo skin
			stage_logo_skin = PIXI.Texture.fromImage("game/images/Stage_Logo.jpg");
			
			// Dice result background
			dice_result_bg = PIXI.Texture.fromImage("game/images/Dice-outcome-bg.png");
			
			// Result dice
			result_dice_1 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-1.png");
			result_dice_2 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-2.png");
			result_dice_3 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-3.png");
			result_dice_4 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-4.png");
			result_dice_5 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-5.png");
			result_dice_6 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-6.png");
			
		break;
		case "Tablet":
			// Main game button
			game_ind_button_up_skin = PIXI.Texture.fromImage("game/images/G_btn_up.png");
			game_ind_button_hover_skin = PIXI.Texture.fromImage("game/images/G_btn_hover.png");
			game_ind_button_down_skin = PIXI.Texture.fromImage("game/images/G_btn_down.png");
			game_ind_button_dice_rolling = PIXI.Texture.fromImage("game/images/G_btn_up_dice_rolling.png");
			
			game_ind_button_up_po_skin = PIXI.Texture.fromImage("game/images/G_btn_up_po.png");
			game_ind_button_hover_po_skin = PIXI.Texture.fromImage("game/images/G_btn_hover_po.png");
			game_ind_button_down_po_skin = PIXI.Texture.fromImage("game/images/G_btn_down_po.png");
			
			// Import court/field/pitch skins
			court_skin = PIXI.Texture.fromImage("game/images/Court.png");
			court_skin_left_hl = PIXI.Texture.fromImage("game/images/Court_left_highlight.png");
			court_skin_right_hl = PIXI.Texture.fromImage("game/images/Court_right_highlight.png");
			court_skin_mid_hl = PIXI.Texture.fromImage("game/images/Court_middle_highlight.png");
			
			// Game background skin
			bg_stage_skin = PIXI.Texture.fromImage("game/images/TableBG.jpg");
			
			// Meters
			meter_skin_left = PIXI.Texture.fromImage("game/images/MeterBG_Left.png");
			meter_skin_center = PIXI.Texture.fromImage("game/images/MeterBG_Center.png");
			meter_skin_right = PIXI.Texture.fromImage("game/images/MeterBG_Right.png");
			
			// Points history
			ph_bg_skin = PIXI.Texture.fromImage("game/images/PointsHistory.png");
			
			// Stage logo skin
			stage_logo_skin = PIXI.Texture.fromImage("game/images/Stage_Logo.jpg");
			
			// Dice result background
			dice_result_bg = PIXI.Texture.fromImage("game/images/Dice-outcome-bg.png");
			
			// Result dice
			result_dice_1 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-1.png");
			result_dice_2 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-2.png");
			result_dice_3 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-3.png");
			result_dice_4 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-4.png");
			result_dice_5 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-5.png");
			result_dice_6 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-6.png");
		break;
		case "Mobile":
			// Main game button
			game_ind_button_up_skin = PIXI.Texture.fromImage("game/images/G_btn_up.png");
			game_ind_button_hover_skin = PIXI.Texture.fromImage("game/images/G_btn_hover.png");
			game_ind_button_down_skin = PIXI.Texture.fromImage("game/images/G_btn_down.png");
			game_ind_button_dice_rolling = PIXI.Texture.fromImage("game/images/G_btn_up_dice_rolling.png");
			
			game_ind_button_up_po_skin = PIXI.Texture.fromImage("game/images/G_btn_up_po.png");
			game_ind_button_hover_po_skin = PIXI.Texture.fromImage("game/images/G_btn_hover_po.png");
			game_ind_button_down_po_skin = PIXI.Texture.fromImage("game/images/G_btn_down_po.png");
			
			// Import court/field/pitch skins
			court_skin = PIXI.Texture.fromImage("game/images/Court.png");
			court_skin_left_hl = PIXI.Texture.fromImage("game/images/Court_left_highlight.png");
			court_skin_right_hl = PIXI.Texture.fromImage("game/images/Court_right_highlight.png");
			court_skin_mid_hl = PIXI.Texture.fromImage("game/images/Court_middle_highlight.png");
			
			// Game background skin
			bg_stage_skin = PIXI.Texture.fromImage("game/images/TableBG.jpg");
			
			// Meters
			meter_skin_left = PIXI.Texture.fromImage("game/images/MeterBG_Left.png");
			meter_skin_center = PIXI.Texture.fromImage("game/images/MeterBG_Center.png");
			meter_skin_right = PIXI.Texture.fromImage("game/images/MeterBG_Right.png");
			
			// Points history
			ph_bg_skin = PIXI.Texture.fromImage("game/images/PointsHistory.png");
			
			// Stage logo skin
			stage_logo_skin = PIXI.Texture.fromImage("game/images/Stage_Logo.jpg");
			
			// Dice result background
			dice_result_bg = PIXI.Texture.fromImage("game/images/Dice-outcome-bg.png");
			
			// Result dice
			result_dice_1 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-1.png");
			result_dice_2 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-2.png");
			result_dice_3 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-3.png");
			result_dice_4 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-4.png");
			result_dice_5 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-5.png");
			result_dice_6 = PIXI.Texture.fromImage("game/images/ResultDice/Result-dice-6.png");
		break;
	}	
}