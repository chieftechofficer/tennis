	// Determine a scaling ratio for the various elements
	// This will help resize and accurrately space out the  
	// various game elements on the stage as you move them 
	// up or down the screen. Perspective baby!
	
	function scaleRatio()
	{
		if(winWidth>=1920)
		{
			pixel_ratio=8;
			//game_properties.device = "Desktop";
		}
		
		if(winWidth>=1366 && winWidth<1920)
		{
			pixel_ratio=8.5;
			//game_properties.device = "Desktop";
		}
		
		if(winWidth>1280 && winWidth<1366)
		{
			pixel_ratio=7.3;
			//game_properties.device = "Desktop";
		}
		
		if(winWidth<=1280 && winWidth>800)
		{
			pixel_ratio=8.2;
			//game_properties.device = "Desktop";
		}
		
		if(winWidth<800)
		{
			pixel_ratio=13;
			//game_properties.device = "Mobile";
		}
		
		//$("#debuglog").append(winWidth);
	}
	
	// Set the scale ratio NOTE: Must be set first!!
	// This will set the device property which will 
	// scale/fit/organise all the visual elements to 
	// fit accordingly
	//scaleRatio();	