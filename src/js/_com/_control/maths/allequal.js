/************************************
	All equal method 
************************************/
// This method checks if the values
// passed are all equal
// usage areEqual(val1,val2,val3)

function areEqual()
{
   var len = arguments.length;
   for (var i = 1; i< len; i++){
      if (arguments[i] == null || arguments[i] != arguments[i-1])
         return false;
   }
   return true;
}