showHoverLabel = function(s)
{	
	// Sensor label values
	// These describe the sensor when a user hovers over 
	// or touches the sensor using a touch enabled device
	var sensor_label_values = [GDK.locale.get("g_sensor_0_odds_description"),GDK.locale.get("g_sensor_1_odds_description"),GDK.locale.get("g_sensor_2_odds_description"),GDK.locale.get("g_sensor_3_odds_description"),GDK.locale.get("g_sensor_4_odds_description"),GDK.locale.get("g_sensor_5_odds_description"),GDK.locale.get("g_sensor_6_odds_description"),GDK.locale.get("g_sensor_7_odds_description"),GDK.locale.get("g_sensor_8_odds_description"),GDK.locale.get("g_sensor_9_odds_description"),GDK.locale.get("g_sensor_10_odds_description"),GDK.locale.get("g_sensor_11_odds_description"),GDK.locale.get("g_sensor_12_odds_description"),GDK.locale.get("g_sensor_13_odds_description")];
	
	// Reset all the elements to invisible
	hideLabels();
	hideArrows();
		
	// Setup text
	var _display_text = sensor_label_values[s];
	// Set the height and the width of the label
	var _set_label_height = 40;
	var _set_label_width = ((_display_text.length*font_size_ratio)*pixel_ratio)+45;
	
	// Set the x value to the position of the sensor
	var x = sensor_control_array[s].position.x;
	var y = sensor_control_array[s].position.y;
	
	// Determine which side of the screen the label should be placed
	// Left label
	if(x<winXfactor) 
	{	
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-left-arrow");
	}
	// Right label
	if(x>winXfactor)
	{	
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-right-arrow");
	}
	// Top label
/*	if((x==winXfactor) && (y<=500))
	{	
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-top-arrow");
	}	*/
	// Bottom label
	if((x==winXfactor))
	{			
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-bottom-arrow");
	}
				
	var _tc_d_t = new Date();
	tc = _tc_d_t.getTime()+3000;
			
	// Hide the labels after a short time period
	hide_labels = setInterval(clearLabelInterval,4000);
}
	
showLabel = function(s)
{	
	// Reset all the elements to invisible
	hideLabels();
		
	/*******************************************
		Setup text for label
	*******************************************/
	// Set the bet amount
	var _s_bet_amount = GDK.locale.formatCurrency(sensor_values[s*2+1], true);
	// Set the winning amount
	var _s_win_amount = roundNum(sensor_odds[s]*_s_bet_amount);
	// Set the text form the lang.json file
	var _display_text = GDK.locale.get("g_sensor_bet_wins");
	
	//$("#debuglog").append("<br>_display_text.length: " + _display_text.length); // For testing purposes
	
	// Replace the bet amount placeholder
	_display_text = _display_text.replace("{{ s_bet_amount }}",_s_bet_amount);
	// Replace the winning amount placeholder
	_display_text = _display_text.replace("{{ s_win_amount }}",_s_win_amount.toFixed(2));
	
	//$("#debuglog").append("<br>_display_text.length: " + _display_text.length); // For testing purposes
	
	// Set the height and width of the label
	var _set_label_height = 40;
	var _set_label_width = ((_display_text.length)*(pixel_ratio+font_size_ratio))+35;

	/*******************************************
		Determine which side of the screen 
		the label should be placed
	*******************************************/	
	
	// Set the x value to the position of the sensor
	var x = sensor_control_array[s].position.x;
	var y = sensor_control_array[s].position.y;
	
	// Left label
	if(x<winXfactor)
	{	
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-left-arrow");
	}
	// Right label
	if(x>winXfactor)
	{	
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-right-arrow");
	}
	// Top label
	/*if((x==winXfactor) && (y<=500))
	{
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-top-arrow");
	}		*/
	// Bottom label
	if(x==winXfactor)
	{
		// Set the text for the label
		populateLabel(x,y,_display_text,"label-bottom-arrow");
	}	
	
	/*******************************************
		Set a time delay for the label
	*******************************************/
	// These are required so if the user clicks a 
	// fresh label the the interval will start fresh
	var _tc_d_t = new Date();
	tc = _tc_d_t.getTime()+3000;
		
	// Hide the labels after a short time period
	hide_labels = setInterval(clearLabelInterval,2000);	
}	
function clearLabelInterval()
{	
	var datetime = new Date();
	var _test_tc = datetime.getTime();
		
	if(_test_tc>tc)
	{
		fadeLabels();
		clearInterval(hide_labels);
	}
}	
function hideLabels()
{	
	$("#hoverLabel").hide();
}

function fadeLabels()
{
	$("#hoverLabel").fadeOut("slow");
}

function hideArrows()
{
	$(".label-left-arrow").addClass("hidden");
	$(".label-right-arrow").addClass("hidden");
	$(".label-top-arrow").addClass("hidden");
	$(".label-bottom-arrow").addClass("hidden");
}

function populateLabel(x,y,text,direction)
{
	hideArrows();
	
	$("#hoverLabelText").html(text);		
	$("." + direction).removeClass("hidden");
	
	switch(direction)
	{
		case "label-left-arrow":
			$("#hoverLabel").css("text-align", "left");
			x+=40;
			y-=30;
		break;
		case "label-right-arrow":
			$("#hoverLabel").css("text-align", "right");
			x = x - $("#hoverLabel").width()-40;
			y-=30;
		break;
		case "label-top-arrow":
			x = x - ($("#hoverLabel").width()/2);
			$("#hoverLabel").css("text-align", "center");
		break;
		case "label-bottom-arrow":
			x = x - ($("#hoverLabel").width()/2);
			y = y - ($("#hoverLabel").height()*1.5);
			$("#hoverLabel").css("text-align", "center");
			
		break;
	}
	
	$("#hoverLabel").css("top", y + "px");
	$("#hoverLabel").css("left", x + "px");
	$("#hoverLabel").fadeIn("fast");
}
