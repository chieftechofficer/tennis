/**************************************
	Points history data
**************************************/

/**************************************
	Add a value to the stage 
	win label array
**************************************/

function addValue(val)
{	
	stage_win_label.push(val);
}

/**************************************
	Update the points history array
**************************************/

function updatePointsHistoryArray(u)
{
	// Ensure there is a value in each phase of the stage win array
	if(!stage_win_label[0]) // <-- stage 1
	{
		stage_win_label[0]="-";
	}
	if(!stage_win_label[1]) // <-- stage 2
	{
		stage_win_label[1]="-";
	}
	if(!stage_win_label[2]) // <-- stage 3
	{
		stage_win_label[2]="-";
	}
	
	// Push the data into the points history array
	points_history_data.unshift(stage_win_label[2]);
	points_history_data.unshift(stage_win_label[1]);
	points_history_data.unshift(stage_win_label[0]);
	
	stage_win_label = [];
	
	// Update the points history board
	if(u)
		console.log("populate");
		populatePointsHistory();
		// Clear the array for the next game
	
}

/*******************************************
	Populate points history
*******************************************/

function populatePointsHistory()
{
	// if(game_properties.device=="Desktop")
	for (var i=0;i<9;i++)
	{ 
		points_history_box[i+3].setText(points_history_data[i]);
	}
}

/******************************************
	Show/hide pints history gdk control
******************************************/

function showHidePH(flag)
{	
	showHidePHFlag = flag;
	
	// Hide the ph 
	if(!flag)
		showStageLogo();
}

/******************************************
	Odobo dice history
******************************************/

function updatePointHistory(_ph)
{
	// Calculate length of results array
	var _phl = _ph.length;
	var _sph = _phl-3;
	
	if(_sph<0)
		_sph = 0;
	
	for(i=_sph;i<_phl;i++)
	{
		for(j=0;j<_ph[i].results.length;j++)
		{
			addValue(GDK.locale.get("g_sensor_" + spotNum(_ph[i].results[j].winnerSpot) + "_odds_label"));
		}
		updatePointsHistoryArray(false);
	}
}