/*************************************
	Set winnings text box in 
	the top bar
*************************************/

function setWinnings(n)
{
	// Set meter
	meter_control_array[15].setText(GDK.locale.formatCurrency(n, true));
}
