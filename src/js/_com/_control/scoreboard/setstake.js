/****************************************
	Set stake
****************************************/

function setStake(n)
{
	// Set meter
	meter_control_array[9].setText(GDK.locale.formatCurrency(n, true));
	
	GDK.game.report.stake(n);
}
