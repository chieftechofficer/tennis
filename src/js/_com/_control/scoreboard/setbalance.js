/****************************************
	Set balance textbox in the topbar
****************************************/

function setBalance(n)
{
	// Round off the number and ensure it has only got 2 decimal points
	y = (Math.round(((n)) * 100) / 100);
	
	// Set meter
	if(meter_control_array[3])
		meter_control_array[3].setText(GDK.locale.formatCurrency(n, true));
	
	// Set the game balance variable 
	balance = n;
}
