var c_i_dice = 4; // This is set to 4 because that is how the designer named the files and renaming them would have been time consuming.
var roll_dice_speed = 50;
var r_d_1_int,r_d_2_int,r_d_3_int;
var roll_n_d_int;

function rollDice1()
{
	var _dice1_skin = PIXI.Texture.fromImage(dice1_skin+c_i_dice+".png");
	
	// Set skin 
	animated_dice[0].setTexture(_dice1_skin);
	
	// Add 1 to the frame count
	c_i_dice++;
	
	// Play dice roll sound when the dice hits the table
	if(c_i_dice==20)
		GDK.audio.play("diceroll");
		
	if(c_i_dice>=40)
	{
		clearInterval(r_d_1_int);
		stage1();
		roll_n_d_int = setInterval(rollNextDice, 500);
	}	
}

function rollDice2()
{
	var _dice2_skin = PIXI.Texture.fromImage(dice2_skin+c_i_dice+".png");
	
	// Set skin 
	animated_dice[1].setTexture(_dice2_skin);
	
	// Add 1 to the frame count
	c_i_dice++;
	
	// Play dice roll sound when the dice hits the table
	if(c_i_dice==20)
		GDK.audio.play("diceroll");
		
	if(c_i_dice>=40)
	{
		clearInterval(r_d_2_int);
		stage2();
		roll_n_d_int = setInterval(rollNextDice, 500);
	}
}

function rollDice3()
{
	var _dice3_skin = PIXI.Texture.fromImage(dice3_skin+c_i_dice+".png");
	
	// Set skin 
	animated_dice[2].setTexture(_dice3_skin);
	
	// Add 1 to the frame count
	c_i_dice++;
	
	// Play dice roll sound when the dice hits the table
	if(c_i_dice==20)
		GDK.audio.play("diceroll");
		
	if(c_i_dice>=40)
	{
		clearInterval(r_d_3_int);
		stage3();
		roll_n_d_int = setInterval(rollNextDice, 20);
	}
}

function animDice()
{
	
	//$("#debuglog").append("ROlling dice<br>");
	
	// Set the skin for the dice
	switch(game_stage)
	{
		// Stage 1
		case 1:
			switch(engine_dice_result[game_stage])
			{
				case 1:
					dice1_skin = "game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_0";	
					
					// Set the result dice skin
					dice_result_skin = result_dice_1;
					
				break;
				case 6:
					dice1_skin = "game/images/DiceAnim/ThrowONE_R6/ThrowONE_R6_0";
					
					
					// Set the result dice skin
					dice_result_skin = result_dice_6;
				break;
			}
			
			// Highlight the court
			setCourt(court_skin_mid_hl,"middle");
			
			// Start the dice anim
			r_d_1_int = setInterval(rollDice1, roll_dice_speed);
			
		break;
		// Stage 2
		case 2:
			switch(engine_dice_result[game_stage])
			{
				case 1:
					dice2_skin = "game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_0";	
					
					// Set the result dice skin
					dice_result_skin = result_dice_1;
				break;
				case 2:
					dice2_skin = "game/images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_0";
					
					// Set the result dice skin
					dice_result_skin = result_dice_2;
				break;
				case 3:
					dice2_skin = "game/images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_0";
					
					// Set the result dice skin
					dice_result_skin = result_dice_3;	
				break;
			}
			
			// Highlight the court
			setCourt(court_skin_left_hl,"left");
			
			// Start the dice anim
			r_d_2_int = setInterval(rollDice2, roll_dice_speed);
			
		break;
		// Satge 3
		case 3:
			switch(engine_dice_result[game_stage])
			{
				case 4:
					dice3_skin = "game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_0";
					
					// Set the result dice skin
					dice_result_skin = result_dice_4;
				break;
				case 5:
					dice3_skin = "game/images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_0";
					
					// Set the result dice skin
					dice_result_skin = result_dice_5;
				break;
				case 6:
					dice3_skin = "game/images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_0";
					
					// Set the result dice skin
					dice_result_skin = result_dice_6;
				break;
			}
			
			// Highlight the court
			setCourt(court_skin_right_hl,"right");

			// Start the dice anim
			r_d_3_int = setInterval(rollDice3, roll_dice_speed);

		break;
	}		
}

function rollNextDice()
{
	// Clear the roll next dice interval
	clearInterval(roll_n_d_int);
	
	// Show the dice outcome
	placeDiceResult(dice_result_skin,game_stage);
	
	// Add 1 to gamestage
	game_stage++;
	
		// Playable stages has to be greater than or 
	// equal to the current game stage to continue
	if(playable_stages>=game_stage)
	{		
		// Reset the dice skin counter
		c_i_dice = 4;
		
		// Animate next dice 
		animDice();	
		
	} else {
		
		//$("#debuglog").append("Game over<br>");	
		
		/************************************
			GAME OVER
		************************************/
		
		gameOver();
	}
}

/*****************************************
	Place the dice back into position
*****************************************/

function resetStageDice()
{
	// Reset dice 1
	dice1_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_04.png");
	animated_dice[0].setTexture(dice1_skin);
	placeDice(animated_dice[0],dice_coords[0],dice_coords[1]);
	
	// Reset dice 2
	dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_04.png");
	animated_dice[1].setTexture(dice2_skin);
	placeDice(animated_dice[1],dice_coords[2],dice_coords[3]);

	// Reset dice 3
	dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_04.png");
	animated_dice[2].setTexture(dice2_skin);
	placeDice(animated_dice[2],dice_coords[4],dice_coords[5]);
}

/*****************************************
	Place dice
*****************************************/

function placeDice(obj,x,y)
{
	obj.position.x = x;
	obj.position.y = y;
}