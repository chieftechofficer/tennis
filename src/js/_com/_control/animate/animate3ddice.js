var c_i_dice = 4; // This is set to 4 because that is how the designer named the files and renaming them would have been time consuming.

var stg_zoom_int = "";

function startDiceAnim()
{	
	switch(game_stage)
	{
		case 1:
			animDice1(c_i_dice);
		break;
		case 2:
			animDice2(c_i_dice);
		break;
		case 3:
			animDice3(c_i_dice);
		break;
	}
	
	// Add 1 to the frame count
	c_i_dice++;
	
	// Play dice roll sound when the dice hits the table
	if(c_i_dice==20)
		GDK.audio.play("diceroll");
		
	if(c_i_dice>=40)
	{
		
		// Reset this for the next dice animation
		c_i_dice=4;	
			
		// Stop the dice animation
		clearInterval(dice_anim_interval);
		
		// Now we have got our dice value and we play the game
		playgame();
		
		// Show winning label
		showHoverLabel(label_vals[2]);
		
		//$("#debuglog").append("game_stage: " + game_stage + ",");
		
		// Show the dice outcome
		placeDiceResult(dice_result_skin,game_stage);
		
		// Clear this skin to be re-used
		dice_result_skin = "";
		
		// Add 1 to the game stage
		game_stage++;
		
		// This must come after the playgame function so the labels are set
		// If called before it will cause an error
		stg_zoom_int = setInterval(rollNextDice,1000);
	}
}

function rollNextDice()
{	
	// Check if game can continue and if yes then execute current stage
	if(game_status!="over")
		executeStage(game_stage);
	
	// Clear the interval for the stage
	clearInterval(stg_zoom_int);
}

function resetStageDice()
{
	// Reset dice 1
	dice1_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_04.png");
	animated_dice[0].setTexture(dice1_skin);
	placeDice(animated_dice[0],dice_coords[0],dice_coords[1]);
	
	// Reset dice 2
	dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_04.png");
	animated_dice[1].setTexture(dice2_skin);
	placeDice(animated_dice[1],dice_coords[2],dice_coords[3]);

	// Reset dice 3
	dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_04.png");
	animated_dice[2].setTexture(dice2_skin);
	placeDice(animated_dice[2],dice_coords[4],dice_coords[5]);
}

// New functions
// fn = the number but infact its the setInterval count position
function animDice1(fn)
{
	// Set the correct skin
	if(dice_1[random_number_value_array[1]]==6) // <-- Sensor 7 wins roll
	{
		
		dice1_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowONE_R6/ThrowONE_R6_0"+fn+".png");
		
		// Set the result dice skin
		dice_result_skin = result_dice_6;
	}
	if(dice_1[random_number_value_array[1]]==1) // <-- Sensor 6 wins roll
	{
		dice1_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_0"+fn+".png");		
		// Set the result dice skin
		dice_result_skin = result_dice_1;	
	}
	
	// Set skin 
	animated_dice[0].setTexture(dice1_skin);
	
	//Set position
	//animated_dice[0].position.x = winXfactor;
	//animated_dice[0].position.y = (winYfactor-150)-(20*fn);
}

function animDice2(fn)
{
	// Set the correct skin
	// Move the dice back into position
	if(winning_sensor_switch[6]>0) // <-- Sensor 6 won roll
	{
		switch(dice_2_sensor_6[random_number_value_array[2]]) // <-- 
		{	
			case 3: // <-- Sensor 0
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_3;	
			break;
			case 2: // <-- Sensor 1
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_2;		
			break;
			case 1: // <-- Sensor 2
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_0"+fn+".png");
			
				// Set the result dice skin
				dice_result_skin = result_dice_1;				
			break;
		}
	} 
	if(winning_sensor_switch[7]>0) // <-- Sensor 7 won roll
	{
		switch(dice_2_sensor_7[random_number_value_array[2]])
		{	
			case 1: // <-- Sensor 3
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_1;		
			break;
			case 2: // <-- Sensor 4
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R2/ThrowTWO_R2_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_2;		 
			break;
			case 3: // <-- Sensor 5
				dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R3/ThrowTWO_R3_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_3;		
			break;
		}				
	}
	// Set skin 
	animated_dice[1].setTexture(dice2_skin);
	//Set position
	//animated_dice[1].position.x = winXfactor-350;
	//animated_dice[1].position.y = (winYfactor-150)-(20*fn);
}

function animDice3(fn)
{
	// Set the correct skin
	// Move the dice back into position
	if(winning_sensor_switch[6]>0) // <-- Sensor 6 won roll
	{
		switch(dice_3_sensor_6[random_number_value_array[3]]) // <-- 
		{	
			case 4: // <-- Sensor 8
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_4;		
			break;
			case 5: // <-- Sensor 9
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_5;	
			break;
			case 6: // <-- Sensor 10
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_6;		
			break;
		}		
	} 
	if(winning_sensor_switch[7]>0) // <-- Sensor 7 won roll
	{
		switch(dice_3_sensor_7[random_number_value_array[3]]) // <-- 
		{	
			case 6: // <-- Sensor 11
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R6/ThrowTHREE_R6_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_6;		
			break;
			case 5: // <-- Sensor 12
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R5/ThrowTHREE_R5_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_5;		
			break;
			case 4: // <-- Sensor 13
				dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_0"+fn+".png");
				
				// Set the result dice skin
				dice_result_skin = result_dice_4;		
			break;
		}
	}
	// Set skin 
	animated_dice[2].setTexture(dice3_skin);
	//Set position
	//animated_dice[2].position.x = winXfactor+350;
	//animated_dice[2].position.y = (winYfactor-150)-(20*fn);
}

/*****************************************
	Place dice
*****************************************/

function placeDice(obj,x,y)
{
	obj.position.x = x;
	obj.position.y = y;
}
