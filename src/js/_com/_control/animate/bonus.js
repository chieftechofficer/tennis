/*******************************************
	Bonus screen animations
*******************************************/

// Store the bonus sprites
var bonus_sprites = [];

/******************************************
	Launch the bonus animations
******************************************/

function playBonusAnim(objects)
{
	var _specifyType = "dice";
	var _type,_frames;
	
	for(i=0;i<objects;i++)
	{
		/*createFallingDice();
		createFallingChip();*/
		// Scale
		var _scale = randomFromInterval(5,7)/10;
		
		switch(_scale)
		{
			case 0.5:
				var _speed = 35;
			break;
			case 0.6:
				var _speed = 25;
			break;
			case 0.7:
				var _speed = 20;
			break; 	
		}
		// Offset
		_offset = randomFromInterval(1,29);
		
		// Type
		switch(_specifyType)
		{
			case "dice":
				_type = "dice";
				_frames = 39;
				_specifyType = "redchip";
			break;	
			case "redchip":
				_type = "redchip";
				_frames = 29;
				_specifyType = "bluechip";
			break;
			case "bluechip":
				_type = "bluechip";
				_frames = 29;
				_specifyType = "yellowchip";
			break;	
			case "yellowchip":
				_type = "yellowchip";
				_frames = 29;
				_specifyType = "purplechip";
			break;	
			case "purplechip":
				_type = "purplechip";
				_frames = 29;
				_specifyType = "greenchip";
			break;	
			case "greenchip":
				_type = "greenchip";
				_frames = 29;
				_specifyType = "dice";
			break;	
		}
		
		// Create
		createFallingObject(_speed,_offset,_type,_frames,_scale);
	}
}

/******************************************
	Create falling object
******************************************/

// speed = interval count
// offset = which frame the animation should start on
// type = type of object
// frames is the number of frames in the animation

function createFallingObject(_speed,_offset,_type,_frames,_scale)
{
	switch(_type)
	{
		case "dice":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/Dice_Spin/Dice_Spin_00.png");
		break;	
		case "redchip":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/RedChip_Spin/RedChip_Spin_00.png"); 
		break;	
		case "bluechip":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/BlueChip_Spin/BlueChip_Spin_00.png"); 
		break;
		case "yellowchip":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/YellowChip_Spin/YellowChip_Spin_00.png"); 
		break;
		case "purplechip":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/PurpleChip_Spin/PurpleChip_Spin_00.png"); 
		break;
		case "greenchip":
			var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/GreenChip_Spin/GreenChip_Spin_00.png"); 
		break;	
	}
	
	var _sprite = new PIXI.Sprite(_sprite_skin);
	
	_sprite.position.x = (winXfactor-350) + randomFromInterval(0,600);
	_sprite.position.y = randomFromInterval(-600,0);
	
	//$("#debuglog").html(randomFromInterval(1,10)/10 + "<br>");
	
	/*
		Build a 
	*/
	
	_sprite.scale.x = _sprite.scale.y = _scale;
	
	stage.addChild(_sprite);
	
	// Skin counter
	var _skin_counter = _offset;
	
	function _animate()
	{
		
		// Set new skin
		switch(_type)
		{
			case "dice":
				_sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/Dice_Spin/Dice_Spin_0" + _skin_counter + ".png");
			break;	
			case "redchip":
				var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/RedChip_Spin/RedChip_Spin_0" + _skin_counter + ".png"); 
			break;	
			case "bluechip":
				var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/BlueChip_Spin/BlueChip_Spin_0" + _skin_counter + ".png"); 
			break;
			case "yellowchip":
				var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/YellowChip_Spin/YellowChip_Spin_0" + _skin_counter + ".png"); 
			break;
			case "purplechip":
				var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/PurpleChip_Spin/PurpleChip_Spin_0" + _skin_counter + ".png"); 
			break;
			case "greenchip":
				var _sprite_skin = PIXI.Texture.fromImage("game/images/Bonus/GreenChip_Spin/GreenChip_Spin_0" + _skin_counter + ".png"); 
			break;
		}		
		
		_sprite.setTexture(_sprite_skin);
		
		// Move the sprite down 
		_sprite.position.y += 5; 
		
		// Add 1 to skin counter
		_skin_counter++;
		
		// Ensure skin counter does not go over 39
		if(_skin_counter>_frames)
			_skin_counter = 0;
			
		// If the chip goes past the bottom of the screen
		// Reset its position
		if(_sprite.position.y>winYfactor){
			_sprite.position.y = randomFromInterval(-600,0);
			_sprite.position.x = (winXfactor-350) + randomFromInterval(0,600);
		}
	}
	
	var _interval = setInterval(_animate, _speed);
	
	bonus_sprites.push(_sprite);
	bonus_sprites.push(_interval);
	
}

/**************************************
	Clear the bonus animations
**************************************/

function clearBonusSprites()
{
	for(i=0;i<(bonus_sprites.length/2);i++)
	{
		if(bonus_sprites[i])
		{
			stage.removeChild(bonus_sprites[i*2]);
			clearInterval(bonus_sprites[i*2+1]);
			
			/*$("#debuglog").append((i*2) + "<br>");
			$("#debuglog").append((i*2+1) + "<br>");*/
		}
	}
	
	// Hide the bonus notifier
	hideNotice();
	
	// Clear the bonus animations array
	bonus_sprites = [];
	
	/*$("#debuglog").html(bonus_sprites.length);*/
}