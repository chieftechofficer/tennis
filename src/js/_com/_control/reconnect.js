/*****************************************
	Reconnect functionality
*****************************************/

/*  
	When a player is disconnected during a game 
	we need to run this function to give them 
	the money they won and reset the game to 
	its starting point.
	
	NOTE: This method should only be fired off if the game is in a rolling or a over state
*/

function testreconnect()
{

}

function executeReconnect()
{
	// Move the game chips back into position
	//showtablechips();
		
	// Switch the sensors back on
	switchsensorson();
	
	// Reset winning dice
	engine_dice_result = ["-","-","-","-"];
	r_d_1_int = "";
	r_d_2_int = "";
	r_d_3_int = "";
	
	// Reset bonus
	bonus = false;
	bonus_amount = 0;
		
	// Reset game stage
	game_stage = 1;
	
	// Set the game stage property
	game_properties.stage = "one";
	game_status="play";
	
	// Reset stage stake
	stage_stake = [0,0,0];
	
	stage_wins = [0,0,0];
	
	// Store playable stages for each game
	playable_stages = 0;
	
	// Set the balance
	setBalance(balance);
		
	// Remove the value from stake
	stake = 0;
	rec_stake = stake;
	setStake(stake);
	
	// Reset winnings
	setWinnings(0);
	total_game_winnings = 0;
	
	// Reset losses
	losses = 0;
		
	// Reset sensor values
	sensor_values = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		
	// Switch all the sensors on
	sensor_switch = [1,1,1,1,1,1,1,1,1,1,1,1,1,1];	
		
	// Reset winning switches
	winning_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	selected_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];	
		
	// Remove all the chips from the stage
	for (var p=0; p <= stacked_chips_index.length; p++){
		if(stacked_chips[p]){
			stacked_chips[p].alpha = 0;
		}
	}	
		
	// Reset stacks
	game_chips = [];
	stacked_chips = [];
	stacked_chips_index = [];
		
	/***************************************
		Reset visual elements 
	***************************************/
	
	// Set game indicator text
	setGameBtnTxt(GDK.locale.get("g_main_button_roll_dice"));
	
	// Disable main game btn
	disableMainBtn();
	
	// Set the main game button
	game_ind_elements[0].setTexture(game_ind_button_up_skin);
	
	game_ind_elements[1].setStyle(game_ind_font_style);
	game_ind_elements[1].position.x = game_ind_elements[0].width/2;
			
	// Remove highlight from the court
	setCourt(court_skin,"default");
		
	// Hide the clear and undo buttons
	hideclearundo();
	
	// Hide the labels if any
	hideLabels();
		
	// Reset the 3 dice
	resetStageDice();	
	
	// Clear dice results
	clearDice();
	
	// Reset trophy
	trophy_icon1.position.x = -5000;
	trophy_icon1.position.y = -5000;
	
	trophy_icon2.position.x = -5000;
	trophy_icon2.position.y = -5000;

	trophy_icon3.position.x = -5000;
	trophy_icon3.position.y = -5000;
}

/*******************************************
	Reconnect game status check
*******************************************/
function reconnectCheckGameStatus()
{	
	
}

/*******************************************
	Reconnect bonus win feature
*******************************************/
// The bonus feature should only kick in if 
// the player has won bets on all 3 stages
// and the stake is equal on each stage
// the bonus win is 50% of the stake placed
function reconnectBonusWin()
{	

}