/*******************************************
	Switch all sensors off
*******************************************/
function switchsensorsoff()
{
	
	//$("#debuglog").append("<br>SWITCHING SENSORS OFF<br>");
	for (var p=0; p <= sensor_switch.length-1; p++){
		if(sensor_switch[p]<1){
			
			// Switch the sensor skins to show which ones are still active
			if(sensor_control_array[p].position.x<(winXfactor+20))
			{	
				var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallRightOff.png");
			} 
			else 
			{
				var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallLeftOff.png");
			}	
				
			// Set skin to off
			sensor_control_array[p].setTexture(mainoffsensorplate);
			// Switch off interactivity
			sensor_control_array[p].interactive = false;
				
			// Delete the stacks on the sensors which are not active anymore
			deletestack(p);
		} 
		sensor_control_array[p].interactive = false;
	}	
}
	
/****************************************************
	Switch 1 sensor off
****************************************************/
function switchSensorOff(s)
{
	// Switch the sensor skin to off
	if(sensor_control_array[s].position.x<(winXfactor+20))
	{	
		var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallRightOff.png");
	} 
	else 
	{
		var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallLeftOff.png");
	}	
				
	// sensor_control_array[p].alpha=0;
	sensor_control_array[s].setTexture(mainoffsensorplate);
	sensor_control_array[s].interactive = false;

	sensor_control_array[s].interactive = false;
		
}
/****************************************************
	Switch 1 sensor on
****************************************************/
function switchsensorson()
{
	//$("#debuglog").append("Switching sensors off");	
	for (var p=0; p <= sensor_switch.length-1; p++){
				
		// Switch the sensor skins to show which ones are still active
		if(sensor_control_array[p].position.x<(winXfactor+20))
		{	
			var _mainonsensorplate = PIXI.Texture.fromImage("game/images/BallRight.png");
		} 
		else 
		{
			var _mainonsensorplate = PIXI.Texture.fromImage("game/images/BallLeft.png");
		}	
			
		sensor_control_array[p].setTexture(_mainonsensorplate);
		sensor_control_array[p].interactive = true;
	} 
}

/****************************************************
	Switch 1 sensor off
****************************************************/
function selectedSensor(s)
{
	// Switch the sensor skin to show which one was selected
	if(sensor_control_array[s].position.x<(winXfactor+20))
	{	
		var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallRightSel.png");
	} 
	else 
	{
		var mainoffsensorplate = PIXI.Texture.fromImage("game/images/BallLeftSel.png");
	}	
				
	sensor_control_array[s].setTexture(mainoffsensorplate);

	sensor_control_array[s].interactive = false;
		
	winning_sensor_switch[s] = 1;
}
	
/****************************************************
	Disable sensors
****************************************************/
function disableSensors()
{
	for (var p=0; p <= sensor_control_array.length-1; p++)
	{
		interactiveOff(p);
	}
}
	
/****************************************************
	Enable sensors
****************************************************/
function enableSensors()
{
	if(game_stage<2)
	{
		for (var p=0; p <= sensor_control_array.length-1; p++)
		{
			interactiveOn(p);
		}
	}
}

/****************************************************
	Disable object interactive mode
****************************************************/
function interactiveOff(obj)
{
	sensor_control_array[obj].interactive = false;	
}

/****************************************************
	Enable object interactive mode
****************************************************/
function interactiveOn(obj)
{
	sensor_control_array[obj].interactive = true;	
}