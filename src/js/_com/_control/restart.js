/******************************************
	Restart game
******************************************/
function restartGame()
{	
	$("#debuglog").html("");
	
	c_i_dice = 4;	
	
	// Move the game chips back into position
	//showtablechips(); <-- This may not be needed anymore
		
	// Swiitch the sensors back on
	switchsensorson();
	
	// Reset winning dice
	engine_dice_result = ["-","-","-","-"];
	r_d_1_int = "";
	r_d_2_int = "";
	r_d_3_int = "";
	
	// Reset bonus
	bonus = false;
	bonus_amount = 0;
		
	// Reset game stage
	game_stage = 1;
	
	// Set the game stage property
	game_properties.stage = "one";
	game_status="play";
		
	// Calculate the stake
	stake=0;
		
	// Set the stake in the scoreboard
	setStake(stake);
		
	// Add the winnings to the balance
	setBalance(balance);
		
	// Reset winnings	
	setWinnings(0);
	total_game_winnings = 0;
		
	// Reset losses
	losses = 0;
		
	// Reset stage wins which is required for the bonus feature
	stage_wins = [0,0,0];
	
	// Reset stage stake
	stage_stake = [0,0,0];
	
	// Store playable stages for each game
	playable_stages = 0;
		
	// Reset sensor values
	sensor_values = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		
	// Switch all the sensors on
	sensor_switch = [1,1,1,1,1,1,1,1,1,1,1,1,1,1];
		
	// Reset winning switches
	winning_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	selected_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		
	// Remove all the chips from the stage
	for (var p=0; p <= stacked_chips_index.length; p++)
	{
		if(stacked_chips[p])
		{
			stacked_chips[p].alpha = 0;
		}
	}
		
	// Reset stacks
	game_chips = [];
	stacked_chips = [];
	stacked_chips_index = [];
		
	/***************************************
		Reset visual elements 
	***************************************/
		
	// Set game indicator text
	setGameBtnTxt(GDK.locale.get("g_main_button_roll_dice"));
	
	// Disable main game btn
	disableMainBtn();
	
	// Set the main game button
	game_ind_elements[0].setTexture(game_ind_button_up_skin);
			
	// Remove highlight from the court
	setCourt(court_skin,"default");
		
	// Reset the 3 dice
	resetStageDice();
	
	// Clear dice results
	clearDice();
		
	// Show the repeat bet button if there is anything there
	showRepeatBet();
	
	// Hide the label
	hideLabels();

	// Popultae points history
	//populatePointsHistory();
	
	// Reset trophy
	trophy_icon1.position.x = -5000;
	trophy_icon1.position.y = -5000;
	
	trophy_icon2.position.x = -5000;
	trophy_icon2.position.y = -5000;

	trophy_icon3.position.x = -5000;
	trophy_icon3.position.y = -5000;
}