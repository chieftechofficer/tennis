/*********************************************
	Show/hide intro
*********************************************/

function hideIntro(flag)
{
	hide_intro_switch = flag;
}

/*********************************************
	Intro navigation also used for how 
	to play section in help content
*********************************************/

var slide_count = 1;
var num_of_slides = 12;

function previousSlide() 
{
	if(slide_count>1)
	{
		// Hide the current slide
		$("#slide"+slide_count).addClass("hidden");
		
		// Subtract 1 from the current slide
		slide_count--;
		
		// Show the next slide
		$("#slide"+slide_count).removeClass("hidden");
		
		if(slide_count<num_of_slides)
		{
			$("#nextArrow").removeClass("fade-control");
		}
		
		if(slide_count==1)
		{
			$("#prevArrow").addClass("fade-control");
		}
		
		/*
			Slide show height
			This is required mainly for desktop as mobile uses touch events
		*/
		
		if($("#slide"+slide_count).height()>(winYfactor-50))
		{
			$("#default_content").height(winYfactor+"px");
			$("#default_content").addClass("overY");	
		} else {
			$("#default_content").height($("#slide"+slide_count).height()+"px");
			$("#default_content").removeClass("overY");		
		}
		
		/*
			Reset div position for mobile in case a user has scrolled down/up
		*/
		
		// Check if the device is desktop or mobile
		switch(game_properties.touchevents)
		{	
			case "on":
				$( "#howtoplaylist" ).animate({ 'top': help_content_default_buffer+'px'}, 0);
				// Reset top buffer
				top_buffer = help_content_default_buffer;
			break;
		}
		
		//$("#debuglog").html(slide_count);
	}
}

function nextSlide() 
{
    if(slide_count<num_of_slides){
	
		// Hide the current slide
		$("#slide"+slide_count).addClass("hidden");
		
		// Add 1 from the current slide
		slide_count++;
		
		// Show the next slide
		$("#slide"+slide_count).removeClass("hidden");
		
		if(slide_count>1)
		{
			$("#prevArrow").removeClass("fade-control");
		}
		
		if(slide_count==num_of_slides)
		{
			$("#nextArrow").addClass("fade-control");
		}
		
		/*
			Slide show height
			This is required mainly for desktop as mobile uses touch events
		*/
		
		if($("#slide"+slide_count).height()>(winYfactor-50))
		{
			$("#default_content").height(winYfactor+"px");
			$("#default_content").addClass("overY");	
		} else {
			$("#default_content").height($("#slide"+slide_count).height()+"px");
			$("#default_content").removeClass("overY");		
		} 
		
		/*
			Reset div position for mobile in case a user has scrolled down/up
		*/
		
		// Check if the device is desktop or mobile
		switch(game_properties.touchevents)
		{	
			case "on":
				$( "#howtoplaylist" ).animate({ 'top': help_content_default_buffer+'px'}, 0);
				// Reset top buffer
				top_buffer = help_content_default_buffer;
			break;
		}
		
		//$("#debuglog").html(slide_count);
  }
}

function closeHelp()
{	
	/*
		Reset help content to the first slide
	*/
	
	// Hide the current slide
	$("#slide"+slide_count).addClass("hidden");
	
	// Show slide 1 
	$("#slide1").removeClass("hidden");
	
	// Set the slide count to 1
	slide_count=1;
	
	// Disable previous arrow
	$("#prevArrow").addClass("fade-control");
	
	// Hide the help content
	$('#screen_overlay').addClass('hidden');
	
	// Indicate that the help content is closed
	help_content = false;
	
	/*
		Reset div position for mobile in case a user has scrolled down/up
	*/
		
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{	
		case "on":
			$( "#howtoplaylist" ).animate({ 'top': help_content_default_buffer+'px'}, 0);
			// Reset top buffer
			top_buffer = help_content_default_buffer;
		break;
	}
}

/***************************************
	Close notifier
***************************************/

function closeNotifier()
{
	$("#playerNotifier").hide();
}