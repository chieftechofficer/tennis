// obj = the object where the tick should be placed next to
// menu = the menu where the tick needs to be added
var store_ticks = [];

function highlightSelected(obj,menu)
{	
	
	//$("#debuglog").append(obj.position.x + "<br>");
	//$("#debuglog").append(menu[0].position.x);
	
	// Load the tick image
	
	switch(game_properties.device)
	{
			case "Desktop":
				var tick_skin = PIXI.Texture.fromImage("game/images/Tick_m.png");
				var _h_ypos = obj.position.y-8;
			break;
	}
	
	var tick = new PIXI.Sprite(tick_skin);
	
	tick.position.x = obj.position.x+obj.width+10;
	tick.position.y = _h_ypos;
	
	// Add the obj to the control array
	store_ticks.push(tick);
	
	// Add tick to the control array
	menu[0].addChild(tick);
	
}

// Loop through the items in the options 
// menu and highlight as required

function highlightSelectedMenuItems()
{	

	removeTicksFromNavItems();

	// Loop through the 
	for (var i=0;i<options_menu_items.length;i++)
	{
		
		if(options_menu_items[i].text==game_properties.odds_label || options_menu_items[i].text==game_properties.sounds)
		{
			//$("#debuglog").append("<br>Menu: " + options_menu_items[i].text + "   Odds_label: " + game_properties.odds_label);
			highlightSelected(options_menu_items[i],options_menu_items);
		}
		
		//$("#debuglog").append(options_menu_items[i].text);
	}
}

function removeTicksFromNavItems()
{
	for (var i=0;i<store_ticks.length;i++)
	{
		store_ticks[i].alpha=0;
	}
}