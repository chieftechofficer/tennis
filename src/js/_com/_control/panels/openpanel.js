function openPanel(target)
{
	hideArticles();
	
	switch(target)
	{
		case "whatis":
			$("#whatis").removeClass("hidden");
		break;
		case "howtoplay":
			$("#howtoplay").removeClass("hidden");
		break;
		case "diceprobabilities":
			$("#diceprobabilities").removeClass("hidden");
		break;
		case "trishotz":
			$("#trishotz").removeClass("hidden");
		break;
		case "faq":
			$("#faq").removeClass("hidden");
		break;
	}
	
	$("#screen_overlay").removeClass("hidden");
	
	//$("#debuglog").append("<br> Target: " + target);
}

function hideArticles()
{
		$("#whatis").addClass("hidden");
		$("#howtoplay").addClass("hidden");
		$("#diceprobabilities").addClass("hidden");
		$("#trishotz").addClass("hidden");
		$("#faq").addClass("hidden");
}