/******************************************
	Update the sensor values
******************************************/

update_sensor_values = function(value,s,x,y)
{
	// The value of the chips currently on the sensor
	var o_v = sensor_values[s*2 + 1];
		
	// Add the current and the new credit value of the stacked chips
	o_v = o_v + value;
		
	// Update the array with the correct value of the stack
	sensor_values[s*2 + 1] = o_v; 
		
	// Add a chip to the table stack
	createplacerchip(x,y,value,s);
		
	// Update the stacks to compress the chips
	compressStack(s,x,y);
}