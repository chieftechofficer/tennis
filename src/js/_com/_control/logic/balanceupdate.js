/*********************************************
	Balance update and processing
*********************************************/
// Pass in 5 paramters
// func = true or false // true meaning add and false meaning subtract
// value = amount of credit to be added or subtracted
// s = the sensor which was activated
// x = x position of activated sensor
// y = y position of activated sensor
	
balanceUpdate = function(func,value,s,x,y)
{
	if(func)
	{
		balance = balance+value;
		setBalance(balance);
	} 
	else 
	{
		// Check to see if the balance is greater than the chip value
		if(balance>=value)
		{	
			// Ensure stake does not go over 10000
			if((stake+value)<=10000)
			{
				// Stop voiceovers
				stopVoiceovers();
				stopGameSounds();
				
				// Set the balance
				balance = balance-value;
				setBalance(balance);
					
				// Set the stake
				stake = stake+value;
				rec_stake = stake;
				setStake(stake);
					
				/////////////////////////////////////////////////////////////////////////
				// If stake is greater than 0 then set the game in motion
				// Create the clear and undo buttons
	
				showClearUndo();
	
				hideRepeatBet();
						
				// Highlight the indicator bar to show its now time to click
				enableMainBtn();
						
				// Remember the last move
				last_move.push(s);
				last_move.push(value);
				last_move.push(x);
				last_move.push(y);
						
				// Oraganise the table
				update_sensor_values(value,s,x,y);
						
				// The labels work differntly on mobile 
				// so on Mobile do not show this 
				// Display a label
					
				// Check if the device is desktop or mobile
				switch(game_properties.device)
				{
					case "Desktop":			
						showLabel(s);
					break;
				}
			} else {
				// Show notifier to say stake is limited
				
				showNotice(winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px",(winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px","Stake is limited to (" + GDK.locale.getCurrency() + ")" + GDK.locale.formatCurrency(10000, true),2500);
				/*$("#notifierContent").html("Stake is limited to (" + GDK.locale.getCurrency() + ")" + GDK.locale.formatCurrency(10000, true));
				$("#playerNotifier").css("left", winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px");
				$("#playerNotifier").css("top", (winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px");
				$("#playerNotifier").fadeIn("slow").delay(2500).fadeOut("slow");*/
			}
		}
		else
		{
			// Show notifier to say there is insufficient funds
			showNotice(winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px",(winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px","You have insufficient funds. Please add funds to continue placing bets.",2500);
			
			/*$("#notifierContent").html("You have insufficient funds. Please add funds to continue placing bets.");
			$("#playerNotifier").css("left", winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px");
			$("#playerNotifier").css("top", (winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px");
			$("#playerNotifier").fadeIn("slow").delay(2500).fadeOut("slow");*/
		}
	}
}