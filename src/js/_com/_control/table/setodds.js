/****************************************
	Set the odds display game property
****************************************/
// Requires 1 parameter
// type = either f (fractional) or d (decimal) 
function setOdds(type)
{
	// Check which type of odds the user wishes to be displayed
	switch(type)
	{
		case "f":
			updateOddsDisplay("f");
		break;
		case "d":
			updateOddsDisplay("d");
		break;
	}
}