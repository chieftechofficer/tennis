/************************************************
	Control dice outcomes
************************************************/

/*
	Place dice
*/
function placeDiceResult(result,stage)
{	
	// Check which dice
	switch(stage)
	{
		case 1:
			dice_result_array[0].setTexture(result);
			dice_result_array[1].setText(sensor_labels[checkWinningLabel()]);
			//$("#debuglog").append("sensor_labels[checkWinningLabel()]: " +  sensor_labels[checkWinningLabel()] + "  " + checkWinningLabel() + "<br>");
			TweenLite.to(dice_result_array[2], 0.5, {alpha:1});
		break;
		case 2:
			dice_result_array[3].setTexture(result);
			dice_result_array[4].setText(sensor_labels[checkWinningLabel()]);
			//$("#debuglog").append("sensor_labels[checkWinningLabel()]: " +  sensor_labels[checkWinningLabel()] + "  " + checkWinningLabel() + "<br>");
			TweenLite.to(dice_result_array[5], 0.5, {alpha:1});
		break;
		case 3:
			dice_result_array[6].setTexture(result);
			dice_result_array[7].setText(sensor_labels[checkWinningLabel()]);
			//$("#debuglog").append("sensor_labels[checkWinningLabel()]: " +  sensor_labels[checkWinningLabel()] + "  " + checkWinningLabel() + "<br>");
			TweenLite.to(dice_result_array[8], 0.5, {alpha:1});
		break;	
	}
}

/*
	Check which label is the latest one to win 
*/
// Loop backwards through winning_sensor_switch
// and determine the last winning switch
function checkWinningLabel()
{
	// Determine which stage of the game we are on
	switch(game_stage)
	{
	case 1:
		for (var i=0; i<=winning_sensor_switch.length-1; i++)	
			if(winning_sensor_switch[i]==1)
				return i;
	break;
	case 2:
		for (var i=0; i<=winning_sensor_switch.length-1; i++)	
			if(winning_sensor_switch[i]==1)
				return i;
	break;
	case 3:
		for (var i=winning_sensor_switch.length-1; i>=0; i--)	
			if(winning_sensor_switch[i]==1)
				return i;
	break;
	}
}

/*
	Clear dice
*/
function clearDice()
{
	TweenLite.to(dice_result_array[2], 0.5, {alpha:0});
	TweenLite.to(dice_result_array[5], 0.5, {alpha:0});
	TweenLite.to(dice_result_array[8], 0.5, {alpha:0});
}
