/**************************************
	Repeat bet button method
**************************************/

function repeatBet()
{	
	// Set a variable to capture the bet value 
	var _bet_value = 0;
		
	// Calculate the bets that were placed on the sensors 
	for (var i=0; i < (sensor_values.length/2); i++) 
	{	
		_bet_value += repeat_bet[i*2+1];
	}
		
	// Check if there is enough money in the kitty proceed
	if(_bet_value<balance)
	{		
		hideRepeatBet();
		
		// Show clear and undo buttons
		showClearUndo();
		
		// Update the sensor values with the repeat bet
		for (var is=0; is < sensor_values.length; is++) 
		{	
			sensor_values[is] = repeat_bet[is];
		}
			
		// Create the chip stacks
		for (var j=0; j < (sensor_values.length/2); j++) 
		{	
			compressStack(j,sensor_positions[j*2],sensor_positions[j*2+1]);
		}
			
		// Update the balance
		// Set the balance
		balance = balance-_bet_value;
		setBalance(balance);
				
		// Set the stake
		stake = stake+_bet_value;
		setStake(stake);
			
		// Set the main game button text to full visibility
		enableMainBtn();
	} 
	else 
	{
		// Show notifier to say there is insufficient funds
		showNotice(winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px",(winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px","You have insufficient funds. Please add funds to continue placing bets.",2500);
		/*$("#notifierContent").html("You have insufficient funds. Please add funds to continue placing bets.");
		$("#playerNotifier").css("left", winXfactor-( $( "#playerNotifier" ).width()/2)-25 +"px");
		$("#playerNotifier").css("top", (winYfactor/2)-( $( "#playerNotifier" ).height()/2)-25 +"px");
		$("#playerNotifier").fadeIn("slow").delay(2500).fadeOut("slow");*/
	}
}

/**************************************
	Show repeat bet button 
**************************************/

function showRepeatBet()
{
	// Move the Repeat bet button into place it into the correct position
	/*if(game_button_array[2].position.y==(winYfactor+250))
	{
		TweenLite.to(game_button_array[2].position, 0.5, {y:510, ease:"Linear.easeOut"});
	}*/
	
	// Switch on interactivity for repeat bet button
	bet_btn_interactive_control[2].interactive = true;
	bet_btn_interactive_control[2].buttonMode = true;
	
	// Hide the repeat bet button
	TweenLite.to(game_button_array[2], 0.5, {alpha:1});
}

/**************************************
	Hide repeat bet button 
**************************************/

function hideRepeatBet()
{	
	/*if(game_button_array[2])
	{
		TweenLite.to(game_button_array[2].position, 0.5, {y:winYfactor+250, ease:"Linear.easeOut"});
	}*/
	
	// Switch off interactivity for repeat bet button
	bet_btn_interactive_control[2].interactive = false;
	bet_btn_interactive_control[2].buttonMode = false;
	
	// Hide the repeat bet button
	TweenLite.to(game_button_array[2], 0.5, {alpha:0});
}