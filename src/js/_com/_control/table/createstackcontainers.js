/***************************************
	Create comtainer for the 
	stacked chips
***************************************/	
// Stack containers are objects where 
// you place the stacked chips
 
function createstackcontainer(x,y)
{
	var _stack_container = new PIXI.DisplayObjectContainer(0xffffff);
	_stack_container.position.x = x;
	_stack_container.position.y = y;
	stage.addChild(_stack_container);
	stacked_chip_containers.push(_stack_container);
}