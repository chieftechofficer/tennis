/**************************************************
	Disable main game button
**************************************************/

function disableMainBtn()
{
	game_ind_elements[2].alpha = 0.1;
	game_ind_elements[0].interactive = false;
	game_ind_elements[0].buttonMode = false;
}

/**************************************************
	Enable main game button
**************************************************/

function enableMainBtn()
{
	game_ind_elements[2].alpha = 1;
	game_ind_elements[0].interactive = true;
	game_ind_elements[0].buttonMode = true;
}