/***************************************
	Compress chip stack
***************************************/
compressStack = function(s,x,y)
{		
	// Fetch the current amount of chips on the sensor
	var _c_chips = sensor_values[s*2];
	// Fetch the value of the chips on the sensor
	var _v_chips = sensor_values[s*2+1];
	var _ov_chips = _v_chips;
	
	//$("#debuglog").append("Calculating for Sensor: " + s + " _v_chips= " + _v_chips + " _c_chips= " + _c_chips);
		
	// Calculate to see if you can compress the chips
				
	var _fifty = Math.floor(_v_chips/chip_values[5]);
				
	if(_fifty>0){
		_v_chips = _v_chips - (_fifty*chip_values[5]);
	}
				
	var _twenfive = Math.floor(_v_chips/chip_values[4]);
				
	if(_twenfive>0){
		_v_chips = _v_chips - (_twenfive*chip_values[4]);
	}
				
	var _ten = Math.floor(_v_chips/chip_values[3]);
				
	if(_ten>0){
		_v_chips = _v_chips - (_ten*chip_values[3]);
	}
				
	var _five = Math.floor(_v_chips/chip_values[2]);
				
	if(_five>0){
		_v_chips = _v_chips - (_five*chip_values[2]);
	}
				
	var _two = Math.floor(_v_chips/chip_values[1]);
				
	if(_two>0){
		_v_chips = _v_chips - (_two*chip_values[1]);
	}
				
	var _one = Math.floor(_v_chips/chip_values[0]);
				
	if(_one>0){
		_v_chips = _v_chips - (_one*chip_values[0]);
	}
				
	// Count how many chips there are in the new stack
	var _stack_total = _fifty + _twenfive + _ten +_five + _two + _one;
		
	//$("#debuglog").append("Calculating for Sensor: " + s + " _stack_total= " + _stack_total + " _c_chips= " + _c_chips + " _ov_chips= " + _ov_chips);
		
	// Recreate the stack so the chips are all in order
	// First remove the old chips from the stage
	for (var p=0; p <= stacked_chips_index.length; p++){
		if(stacked_chips_index[p]==s){
			// Remove the chips from the stage
			// Using this line causes a bug --> stage.removeChild(stacked_chips[p]);
			// So use this instead
			stacked_chips[p].alpha = 0;
		}
	}
			
	// Reset the amount of chips stacked to 0
	sensor_values[s*2] = 0;
			
	// Loop through and create the new chips
	for (var p=0; p <_fifty; p++){
		createplacerchip(x, y, chip_values[5], s); // Chip 50
	}
	for (var p=0; p <_twenfive; p++){
		createplacerchip(x, y, chip_values[4], s); // Chip 25
	}
	for (var p=0; p <_ten; p++){
		createplacerchip(x, y, chip_values[3], s); // Chip 10
	}
	for (var p=0; p <_five; p++){
		createplacerchip(x, y, chip_values[2], s); // Chip 5
	}
	for (var p=0; p <_two; p++){
		createplacerchip(x, y, chip_values[1], s); // Chip 2
	}
	for (var p=0; p <_one; p++){
		createplacerchip(x, y, chip_values[0], s); // Chip 1
	}	
}