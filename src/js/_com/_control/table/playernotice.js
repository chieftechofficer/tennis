/**********************************************
	Player notice/notifier
**********************************************/

function showNotice(x,y,msg,delay)
{
	if(!notice_switch)
	{
		// Notifier switch
		notice_switch = true;
		
		// Control jquery
		$("#notifierContent").html(msg);
		$("#playerNotifier").css("left", x);
		$("#playerNotifier").css("top", y);
		$("#playerNotifier").fadeIn("slow").delay(delay).fadeOut("slow", function(){
			// Set notice switch to off to indicate that it is closed
			notice_switch = false;
		});
	}
}

function hideNotice()
{
	// Set switch to false
	notice_switch = false;
	
	// Hide notifier
	$("#playerNotifier").hide();
}