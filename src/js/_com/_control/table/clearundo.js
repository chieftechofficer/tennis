function undoLastMove()
{		
	var s = last_move[last_move.length-4];
	var value = last_move[last_move.length-3];
	var x = last_move[last_move.length-2];
	var y = last_move[last_move.length-1];
		
	if(stake>0){
		
		// Fetch the value of the chips currently on the sensor
		var o_v = sensor_values[s*2 + 1];
			
		// Subtract the current and the new credit value of the stacked chips
		o_v = o_v - value;
			
		// Update the array with the correct value of the stack
		sensor_values[s*2 + 1] = o_v;
			
		// Add the value back to the balance
		balance = balance+value;
		setBalance(balance);
		
		// Remove the value from stake
		stake = stake-value;
		rec_stake = stake;
		setStake(stake);
			
		// Update the stacks to compress the chips
		decompressStack(s,x,y,o_v);
			
		var t_l_m = [];
			
		// Remove the last move from the array
		for (var p=0; p < last_move.length-4; p++){
			t_l_m[p] = last_move[p];
		}
			
		last_move = [];
			
		for (var p=0; p < t_l_m.length; p++){
			last_move[p] = t_l_m[p];
		}
	}
		
	/////////////////////////////////////////////////////////////////
	// Remove highlight from game indicator and the court
	if(stake==0){
		
		/***********COME BACK TO THIS AND ADD A MESSAGE BOX *************/
		// Set game indicator text
		//setStageTxt(GDK.locale.get("g_place_your_bets"));
			
		// Remove the highlight on indicator bar
		disableMainBtn();
			
		// Hide the clear and undo buttons
		//TweenLite.to(game_button_array[0].position, 0.5, {y:winYfactor+250, ease:"Linear.easeOut"});
		//TweenLite.to(game_button_array[1].position, 0.5, {y:winYfactor+250, ease:"Linear.easeOut"});	
		hideclearundo();
	}
		
	// Hide the label
	hideLabels();
}
	
/**************************************
	Hide clear and undo buttons
**************************************/
function hideclearundo()
{
	// Switch off interactivity for clear and undo buttons
	bet_btn_interactive_control[0].interactive = false;
	bet_btn_interactive_control[1].interactive = false;
	
	bet_btn_interactive_control[0].buttonMode = false;
	bet_btn_interactive_control[1].buttonMode = false;
	
	// Hide the clear and undo buttons
	TweenLite.to(game_button_array[0], 0.5, {alpha:0});
	TweenLite.to(game_button_array[1], 0.5, {alpha:0});
}

/**************************************
	Show clear and undo buttons
**************************************/
function showClearUndo()
{
	// Switch on interactivity for clear and undo buttons
	bet_btn_interactive_control[0].interactive = true;
	bet_btn_interactive_control[1].interactive = true;
	
	bet_btn_interactive_control[0].buttonMode = true;
	bet_btn_interactive_control[1].buttonMode = true;
	
	// Show the clear and undo buttons
	TweenLite.to(game_button_array[0], 0.5, {alpha:1});
	TweenLite.to(game_button_array[1], 0.5, {alpha:1});
}