/*************************************
	Set main game button text
*************************************/

function setGameBtnTxt(t)
{
	// Replace line break
	t = t.replace("{{ line_break }}","\n");
	
	// Set text
	game_ind_elements[1].setText(t);
}