/***************************************
	Detect whether the screen is 
	being resized
***************************************/

window.onresize = resize;

/***************************************
	Resize the game according to the 
	users screen adjustment
***************************************/

function resize()
{
	/*********************************************
		Screen size and resolution
	*********************************************/
		
	// Reset the window values
	winHeight = window.innerHeight;
	winWidth = window.innerWidth;
	
	//$("#debuglog").html(winWidth + "      " + winHeight);
	
	// Use this to place objects
	winXfactor = (winWidth/2)+152.5;
	winYfactor = winHeight;
	
	/*********************************************
		Resize renderer		
	*********************************************/
	
	renderer.resize(winWidth,winHeight);

	/*********************************************
		Set the game settings
	*********************************************/

	gameSettings(winXfactor,winYfactor);
	
	/*********************************************
		Visual elements
	*********************************************/
	
	// Game bg
	bg_stage.position.x = game_bg_skin_pos[0];
	bg_stage.position.y = game_bg_skin_pos[1];
	
	// Court
	bg_default.position.x = game_court_pos[0];
	bg_default.position.y = game_court_pos[1];
	
	
	/********************************************
		Sensors
	********************************************/
	
	for (var i=0; i < 14; i++) 
	{	
		// Sensors
		sensor_control_array[i].position.x = sensor_positions[i*2];
		sensor_control_array[i].position.y = sensor_positions[i*2 + 1];
		
		// Stack containers
		stacked_chip_containers[i].position.x = sensor_positions[i*2];
		stacked_chip_containers[i].position.y = sensor_positions[i*2 + 1];
	}
	
	/********************************************
		Hide hover labels
	********************************************/	
	// If any labels are in view when the
	// screen is resized then hide them
	
	hideLabels();

	/********************************************
		Set stage labels
	********************************************/	
	
	switch(game_properties.odds)
	{
		case "f":
			setOdds("f");
		break;
		case "d":
			setOdds("d");
		break;
	}
	
	/********************************************
		Selected chip
	********************************************/
	
	table_chip_array[0].position.x = chip_positions[0];
	table_chip_array[1].position.x = chip_positions[0];
	table_chip_array[2].position.x = chip_positions[0];
	table_chip_array[3].position.x = chip_positions[0];
	table_chip_array[4].position.x = chip_positions[0];
	table_chip_array[5].position.x = chip_positions[0];
	
	/********************************************
		Control panel
	********************************************/
	
	control_panel.position.x = control_panel_position[0];
	control_panel.position.y = control_panel_position[1];

	/********************************************
		Stage logo
	********************************************/
	
	stage_logo.position.x = stage_logo_position[0];
	stage_logo.position.y = stage_logo_position[1];
	
	/********************************************
		Scoreboard
	********************************************/
		
	points_history_box[12].position.x = points_history_pos[0];
	points_history_box[12].position.y = points_history_pos[1];
	
	/********************************************
		Meter positions
	********************************************/
	
	/*
		Resize and position meters accordingly
	*/
	
	meterAdjust(5,0);
	meterAdjust(11,1);
	meterAdjust(17,2);
	
	/********************************************
		Dice outcome result containers
	********************************************/
	
	dice_result_array[2].position.x = dice_outcome_positions[0];
	dice_result_array[2].position.y = dice_outcome_positions[1];
	dice_result_array[5].position.x = dice_outcome_positions[2];
	dice_result_array[5].position.y = dice_outcome_positions[3];
	dice_result_array[8].position.x = dice_outcome_positions[4];
	dice_result_array[8].position.y = dice_outcome_positions[5];
	
	/*********************************************
		Menu and info buttons
	*********************************************/
	
	// Menu button
	gdk_menu_btn.position.x = menu_btn_positions[0];
	gdk_menu_btn.position.y = menu_btn_positions[1];
	
	// Info button
	info_btn.position.x = menu_btn_positions[2];
	info_btn.position.y = menu_btn_positions[3];
	
	/*********************************************
		Table dice
	*********************************************/
	
	animated_dice[0].position.x = dice_coords[0];
	animated_dice[1].position.x = dice_coords[2];
	animated_dice[2].position.x = dice_coords[4];
}