/*************************************
	Touch events outside of the GDK
*************************************/
// This does work but not be required
// Keep if for now just in case we may 
// come back to it
var start_touch = 0;
var end_touch = 0;
var swipe_dir = 'none';
// Top buffer
var top_buffer = help_content_default_buffer;

document.addEventListener('touchstart', function(e) {
	//e.preventDefault();
	// Determine if the help content is open
	if(help_content) {
		var touch = e.touches[0];
		start_touch = touch.pageY;
	}
}, false);

document.addEventListener('touchend', function(e) {
	// Determine if the help content is open
	if(help_content) {
		//e.preventDefault();
		var touch = e.changedTouches[0];
		end_touch = touch.pageY;
		
			
		/***********************************
			Swipe controls
		***********************************/
		/*
			Swipe direction
		*/
			
		if(start_touch>end_touch){
			swipe_dir = 'up';
		}
			
		if(start_touch<end_touch){
			swipe_dir = 'down';
		}	
			
		/*
			Capture screen height and slide height
			and set vars for direction movement calculation
		*/
			
		// List height
		var _list_height = $("#howtoplaylist").height();
			
		// List y pos
		/*var _offsets = $( "#howtoplaylist" ).offset();*/
		var _list_ypos,_offsets;
			
		// List y pos
		_offsets = $( "#howtoplaylist" ).offset();
		_list_ypos = _offsets.top;
		top_buffer = _list_ypos;
			
		/*
			Swipe up movement
		*/
			
		if(swipe_dir == 'up')
		{	
			if((top_buffer+_list_height)>winYfactor)
			{
				// Calculate new position
				
				$( "#howtoplaylist" ).animate({ 'top': (_list_ypos-250)+'px'}, 200, "swing", function(){
					// end of animation... if you want to add some code here.
				});
			}
		}
			
		/*
			Swipe down movement
		*/
			
		if(swipe_dir == 'down')
		{	
			if(_list_ypos<help_content_default_buffer)
			{			
				$( "#howtoplaylist" ).animate({ 'top': (_list_ypos+250)+'px'}, 200, "swing", function(){
					// end of animation... if you want to add some code here.
				});
			}
		}
			
		//$("#debuglog").html(help_content + "   " + swipe_dir + "   " + _list_height + "   " + winYfactor + "    " + _list_ypos );
			
		// Reset swipe direction so it is ready for the next swipe
		swipe_dir = 'none';
	}		
}, false);