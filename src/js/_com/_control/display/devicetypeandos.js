
function CheckDeviceAndTouch()
{
	// Check OS
	//var os = GDK.check.getOS();
	
	// Check if this device is a mobile device (not a tablet)
	var mobileDevice = GDK.check.isMobile();
	
	// Check if this device is a tablet
	var tabletDevice = GDK.check.isTablet();
	
	// Does this device support touch events
	var touchEvents = GDK.check.supportsTouchEvents();
	
	// Check if this is a mobile, tablet or desktop
	// By default game_properties.device is set to desktop - check settings.js
	if(mobileDevice)
		game_properties.device = "Mobile";
	
	if(tabletDevice)
		game_properties.device = "Tablet";
	
	// Check if this device supports touchevents
	// By default this game_properties.touchevents is set to off - check settings.js
	if(touchEvents)
		game_properties.touchevents = "on";
		
	// Use this for testing
	//game_properties.device = "Mobile";
	//game_properties.touchevents = "off";
	
}
