/************************************************
	Create placeholders for dice outcome
************************************************/

function createPlaceholder(x,y)
{
	
	/*
		Setup a container
	*/
	
	var _display_sprite = new PIXI.DisplayObjectContainer();
	
	// Position the result container
	_display_sprite.position.x = x-5000;
	_display_sprite.position.y = y-5000;
	
	// Setup the background
	var _sprite_bg = new PIXI.Sprite(dice_result_bg);
	
	// Add the result bg to the display object
	_display_sprite.addChild(_sprite_bg);
	
	/*
		Create a sprite to contain the result skin
	*/
	
	var _sprite = new PIXI.Sprite(blank_skin);

	_sprite.anchor.x = 0.5;
	_sprite.anchor.y = 0.5;
	
	// Sprite positioning
	_sprite.position.x = 29;
	_sprite.position.y = 29;
	
	// Add the dice result sprite to the display object
	_display_sprite.addChild(_sprite);	
	
	// Add the sprite to the control array
	dice_result_array.push(_sprite); // <-- array position 0/3/6
	
	/*
		Setup the text box
	*/
	
	var _dice_result_txt = new PIXI.Text("", dice_result_fs);
	
	_dice_result_txt.anchor.y = 0.5;
	
	// Position the text box
	_dice_result_txt.position.x = 65;
	_dice_result_txt.position.y = _sprite_bg.height/2;
	
	// Add the dice result text to the display object
	_display_sprite.addChild(_dice_result_txt);	
	
	// Add the textbox to the control array
	dice_result_array.push(_dice_result_txt); // <-- array position 1/4/7
	
	// Add sprite to control array
	dice_result_array.push(_display_sprite); // <-- array position 2/5/8
	
	// Add display sprite to stage
	stage.addChild(_display_sprite);
	
	// Hide the object
	TweenLite.to(dice_result_array[dice_result_array.length-1], 0.1, {alpha:0});
	
	// Position the display container in the correct position now that it is hidden
	dice_result_array[dice_result_array.length-1].position.x = x;
	dice_result_array[dice_result_array.length-1].position.y = y;
	
}