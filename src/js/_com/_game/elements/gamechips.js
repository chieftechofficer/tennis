/**********************************
	CHIPS
**********************************/
	
// Add the game chips to the table
// x position
// y position
// value is the chip worth
	
function setupGameChips(x,y,value)
{
	// Create textures for the chip accroding to the value
	var chipskin = PIXI.Texture.fromImage("game/images/Chips/Selector-chips/Selector-chips-"+ value +".png");
		   
	chip = new PIXI.Sprite(chipskin);
	chip.anchor.x = 0.5;
	chip.anchor.y = 0.5;
		
	// Move the sprite into position
	chip.position.x = x;
	chip.position.y = y;
	
	// Add chip to the stage
	control_panel.addChild(chip);
	
	// Add chip to the control array
	table_chip_array.push(chip);
}