/*****************************************
	Create placer chips
*****************************************/

function createplacerchip(x, y, value, s)
{	
	// How many chips are currently stacked on that sensor
	var o_c_v = sensor_values[s*2];
		
	//$("#debuglog").html(o_c_v);
	
	// Save the original y position
	var _o_y = y;
		
	// Determine which chip to create
	var table_chip_skin = PIXI.Texture.fromImage("game/images/Chips/Table-chips/Table-chips-"+value+".png");
		
	table_chip = new PIXI.Sprite(table_chip_skin);
		
	table_chip.anchor.x = 0.5;
	table_chip.anchor.y = 0.5;
		
	/*************************************
		Resize the chip according to 
		its position
	*************************************/	
		
	// Scale the chip accordingly
	// var scalesensor = (Math.round(y/80))/3;
	// var scalesensor = (Math.round(y/100))/10;
	var scalechip = Math.round(_o_y/100);
		
	switch (scalechip)
    {
    	case 0:
		scalechip = 0.24;
		break;
		case 1:
		scalechip = 0.28;
		break;
		case 2:
		scalechip = 0.32;
		break;
		case 3:
		scalechip = 0.36;
		break;
		case 4:
		scalechip = 0.40;
		break;
		case 5:
		scalechip = 0.44;
		break;
		case 6:
		scalechip = 0.48;
		break;
		case 7:
		scalechip = 0.52;
		break;
		case 8:
		scalechip = 0.56;
		break;
		case 9:
		scalechip = 0.60;
		break;
		case 10:
		scalechip = 0.64;
		break;
	}
							
	// Scale the chip
	table_chip.scale.x = table_chip.scale.y = scalechip;
		
	/**************************************
		Positioning
	**************************************/
	
	// Update the y position of the chip so it looks stacked
	y = (y - (((chip_height/12)) * o_c_v))-y;
	
	
	// Offset the chip if its a stcked chip
	if(o_c_v>0){
		table_chip.position.x = randomFromInterval(-2,2);
		// Set the y position
		table_chip.position.y = y-(scalechip*1);
	} else {
		table_chip.position.x = 0;
		// Set the y position
		table_chip.position.y = y;
	}
	
	// Add one to the stacked chips value
	sensor_values[s*2] = sensor_values[s*2] + 1;
		
	/**************************************
		Calculate where to stack 
		this chip 
	**************************************/
		
	stacked_chip_containers[s].addChild(table_chip);
	stacked_chips.push(table_chip);
		
	// Add the sensor to the stacked chips index
	stacked_chips_index.push(s);		
}