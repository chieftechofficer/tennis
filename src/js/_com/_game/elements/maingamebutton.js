/*************************************
	Game indicator 
*************************************/

function createMainGameButton(x,y)
{	
	game_ind = new PIXI.DisplayObjectContainer();
	
	var game_ind_button = new PIXI.Sprite(game_ind_button_up_skin);
	game_ind_button.position.x = 0;
	game_ind_button.position.y = 0;
	game_ind_button.interactive = true;
	game_ind_button.buttonMode = true;
	
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":
			game_ind_button.mousedown = function()
			{	
				switch (game_status)
				{
					case "over":
						// Apply point over skin
						this.setTexture(game_ind_button_down_po_skin);
						restartGame();
						stopVoiceovers();
						stopGameSounds();
						game_ind_elements[1].setStyle(game_ind_font_style);
						game_ind_elements[1].position.x = game_ind_elements[1].position.x-10;
						
						// Kill the bonus animations if they are playing
						if(bonus_sprites.length>0)
						{
							clearBonusSprites();
						}
					break;
					case "play":
						
						this.setTexture(game_ind_button_down_skin);
						
						if(stake>0)
						{
							// Prevent the user from clicking twice
							game_ind_elements[0].interactive = false;
							game_ind_elements[0].buttonMode = false;
							
							submitBetData();

						} 
						else 
						{
							/***********COME BACK TO THIS AND ADD A MESSAGE BOX *************/
							// Notify the user that they have insuficient funds
							
							//setStageTxt(GDK.locale.get("g_place_chips_table"));
						}
					break;
				}
			}
				
			game_ind_button.mouseup = function()
			{
				switch (game_status)
				{
					case "over":
						this.setTexture(game_ind_button_hover_po_skin);
					break;
					case "play":
						this.setTexture(game_ind_button_hover_skin);
					break;
				}
			}
				
			game_ind_button.mouseover = game_ind_button.mouseup = function()
			{
				switch (game_status)
				{
					case "over":
						this.setTexture(game_ind_button_hover_po_skin);
					break;
					case "play":
						this.setTexture(game_ind_button_hover_skin);
					break;
				}
			}
				
			game_ind_button.mouseout = function()
			{
				switch (game_status)
				{
					case "over":
						this.setTexture(game_ind_button_up_po_skin);
					break;
					case "play":
						this.setTexture(game_ind_button_up_skin);
					break;
				}
			}
		break;
		case "on":
			// Mobile touch and tap events	
			game_ind_button.touchstart = function()
			{	
				switch (game_status)
				{
					case "over":
					// Apply point over skin
						this.setTexture(game_ind_button_down_po_skin);
						restartGame();
						stopVoiceovers();
						stopGameSounds();
						game_ind_elements[1].setStyle(game_ind_font_style);
						game_ind_elements[1].position.x = game_ind_elements[1].position.x-10;
						
						// Kill the bonus animations if they are playing
						if(bonus_sprites.length>0)
						{
							clearBonusSprites();
						}
					break;
					case "play":
						this.setTexture(game_ind_button_down_skin);
						if(stake>0)
						{
							// Prevent the user from clicking twice
							game_ind_elements[0].interactive = false;
							game_ind_elements[0].buttonMode = false;
							
							submitBetData();

						}
					break;
				}
			}
				
			game_ind_button.touchend = function()
			{
				switch (game_status)
				{
					case "over":
						this.setTexture(game_ind_button_hover_po_skin);
					break;
					case "play":
						this.setTexture(game_ind_button_hover_skin);
					break;
				}
			}
					
		break;
	}
	// Add button to game indicator array
	game_ind_elements.push(game_ind_button); // Array position 0
	
	// Add button to the game indicator container object
	game_ind.addChild(game_ind_button);
	
	game_ind.position.x = x - (game_ind_elements[0].width/2);
	game_ind.position.y = y;
	control_panel.addChild(game_ind);
		
	// Game indicator info text
	var game_ind_info_text = new PIXI.Text("", game_ind_font_style);
	game_ind_info_text.anchor.x = 0.5;
	game_ind_info_text.anchor.y = 0.5;	
	//game_ind_info_text.alpha = 0.5;
	game_ind_elements.push(game_ind_info_text); // Array position 1
		
	// Set game indicator text
	setGameBtnTxt(GDK.locale.get("g_main_button_roll_dice"));
	
	game_ind_info_text.position.x = game_ind_elements[0].width/2;
	game_ind_info_text.position.y = game_ind_elements[0].height/2;
	game_ind.addChild(game_ind_info_text);
		
	game_ind_elements.push(game_ind); // Array position 2
	
	disableMainBtn();
}