/*************************************
	Sensors 
*************************************/
// Create Sensor method takes 3 parameters
// x = xpos
// y = ypos
// sensornum = the sensor id
function createsensorplate(x,y,sensornum)
{	
	if(x<(winXfactor+100))
	{	
		var mainactivesensorplate = PIXI.Texture.fromImage("game/images/BallRight.png");
		var maindownsensorplate = PIXI.Texture.fromImage("game/images/BallRightAnimDownstate.png");
		var sensorplate = new PIXI.Sprite(mainactivesensorplate);
		var hoveranim = PIXI.Texture.fromImage("game/images/BallRightAnimV2.png");
	} 
	else 
	{
		var mainactivesensorplate = PIXI.Texture.fromImage("game/images/BallLeft.png");
		var maindownsensorplate = PIXI.Texture.fromImage("game/images/BallLeftAnimDownstate.png");
		var sensorplate = new PIXI.Sprite(mainactivesensorplate);
		var hoveranim = PIXI.Texture.fromImage("game/images/BallLeftAnimV2.png");
	}
		
	// Set the hitarea to true
	sensorplate.interactive = true;
	sensorplate.buttonMode = true;
		
	// Center the chips anchor point
	sensorplate.anchor.x = 0.5;
	sensorplate.anchor.y = 0.5;
		
	// Move the sprite to its designated position
	sensorplate.position.x = x;
	sensorplate.position.y = y;
		
	// Scale the sensor accordingly
	// WHY 55? Because it looks good
	var scalesensor = Math.round(y/55);
		
	switch (scalesensor)
    {
    	case 1:
		scalesensor = 0.21;
		break;		
    	case 2:
		scalesensor = 0.22;
		break;
    	case 3:
		scalesensor = 0.23;
		break;
    	case 4:
		scalesensor = 0.24;
		break;
		case 5:
		scalesensor = 0.25;
		break;
		case 6:
		scalesensor = 0.26;
		break;
		case 7:
		scalesensor = 0.27;
		break;
		case 8:
		scalesensor = 0.28;
		break;
		case 9:
		scalesensor = 0.29;
		break;
		case 10:
		scalesensor = 0.30;
		break;
		case 11:
		scalesensor = 0.31;
		break;
		case 12:
		scalesensor = 0.32;
		break;
		case 13:
		scalesensor = 0.33;
		break;
		case 14:
		scalesensor = 0.34;
		break;
	}
	
	//$("#debuglog").append(scalesensor + ",");
						
	// Resize using the correct scale
	sensorplate.scale.x = sensorplate.scale.y = scalesensor;
		
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":
		// Mobile touch and tap events
		sensorplate.mousedown = function(touchData)
		{				
			this.setTexture(maindownsensorplate);
	
			showHoverLabel(sensornum);
						
			if(current_chip>0)
			{	
				// Play chip sounds
				if(sensor_values[sensornum*2+1]>=1)
				{
					GDK.audio.play("stackedchip");
				}
				else
				{
					GDK.audio.play("singlechip");
				}
							
				balanceUpdate(false,current_chip,sensornum,x,y);
			} 
			else
			{
				/***********COME BACK TO THIS AND ADD A MESSAGE BOX *************/
				
				//setStageTxt(GDK.locale.get("g_select_chip"));
			}
		}
				
		sensorplate.mouseup = function() 
		{	
			this.setTexture(hoveranim);
		}
					
		sensorplate.mouseover = function() 
		{	
			// Play sensor hover sound
			GDK.audio.play("sensorhover");
			// Show hover label
			showHoverLabel(sensornum);
			// Set hover skin
			this.setTexture(hoveranim);
		}
					
		sensorplate.mouseout = function() 
		{	
			this.setTexture(mainactivesensorplate);
		}
	break;
	case "on":
		// Mobile touch and tap events
		sensorplate.touchstart = function(touchData)
		{				
			this.setTexture(maindownsensorplate);
							
			if(current_chip>0)
			{	
				// Play chip sounds
				if(sensor_values[sensornum*2+1]>=1)
				{
					GDK.audio.play("stackedchip");
				}
				else
				{
					GDK.audio.play("singlechip");
				}
								
				balanceUpdate(false,current_chip,sensornum,x,y);
			} 
			else
			{
				/***********COME BACK TO THIS AND ADD A MESSAGE BOX *************/
				
				//setStageTxt(GDK.locale.get("g_select_chip"));
			}
		}
				
		sensorplate.touchend = sensorplate.touchend = function() 
		{	
			//hideLabels();
			this.setTexture(mainactivesensorplate);
		}
	break;
	}
	
	// Add sensor to stage
	stage.addChild(sensorplate);
		
	// Add sensor to control array
	sensor_control_array.push(sensorplate);
}