/**********************************************
	Method to setup stage odds labels
**********************************************/
// Requires 1 parameter
// type = either f or d 
function setupStageLabels(type)
{
	// Set some vars
	var _n;
	
	// Loop through the sensors and attach labels for each
	for(var i=0;i<14;i++)
	{	
		// Determine which type of odds to be displayed
		// Either fractional or decimal
		switch(type)
		{
			case "f":
				_n = sensor_odds_fractional[i];
			break;
			case "d":
				// Save the odds text
				_n = sensor_odds[i];
				
				// Check if it has a decimal point
				// If it doesn't then add a .0 for display purposes
				if (_n % 1 == 0) _n = _n.toFixed(1);
			break;
		}

		// Call the create label method
		createStageLabel(_n,sensor_labels[i],sensor_positions[i*2],sensor_positions[i*2 + 1],i);
	}
}
/**********************************************
	Create stage odds label method
**********************************************/
// This function requires 4 parameters
// oddsText = the odds to be displayed
// labelText = the label text is the description of that specific sensor ie: Return winner
// x = x position of the label
// y = y position of the label
// num = sensor number which the label belows to 
function createStageLabel(oddsText,labelText,x,y,num)
{	
	// Firstly we need to calculate a ratio for the distance of the label
	// WHY 55? Because it looks good
	var _distance_ratio = Math.round(y/55);
	var _scale_dice_ratio = Math.round(y/55);
		
	switch (_distance_ratio)
    {
		case 0:
		_distance_ratio = 28;
		_scale_dice_ratio = 0.50;
		break;
		case 1:
		_distance_ratio = 29;
		_scale_dice_ratio = 0.51;
		break;
		case 2:
		_distance_ratio = 30;
		_scale_dice_ratio = 0.52;
		break;
		case 3:
		_distance_ratio = 31;
		_scale_dice_ratio = 0.53
		break;
		case 4:
		_distance_ratio = 32;
		_scale_dice_ratio = 0.54;
		break;
		case 5:
		_distance_ratio = 33;
		_scale_dice_ratio = 0.55;
		break;
		case 6:
		_distance_ratio = 34;
		_scale_dice_ratio = 0.56;
		break;
		case 7:
		_distance_ratio = 34;
		_scale_dice_ratio = 0.57;
		break;
		case 8:
		_distance_ratio = 34;
		_scale_dice_ratio = 0.58;
		break;
		case 9:
		_distance_ratio = 36;
		_scale_dice_ratio = 0.59;
		break;
		case 10:
		_distance_ratio = 38;
		_scale_dice_ratio = 0.60;
		break;
		case 11:
		_distance_ratio = 40;
		_scale_dice_ratio = 0.61;
		break;
		case 12:
		_distance_ratio = 34;
		_scale_dice_ratio = 0.62;
		break;
		case 13:
		_distance_ratio = 38;
		_scale_dice_ratio = 0.63;
		break;
		case 14:
		_distance_ratio = 40;
		_scale_dice_ratio = 0.64;
		break;
	}
	
	// Setup label container
	var _stage_label =  new PIXI.DisplayObjectContainer();
	
	/*
		Create dice icon
	*/
	
	switch (num)
    {
		case 0:
		var _dice_icon = new PIXI.Sprite(sensor_0_dice_icon);
		break;
		case 1:
		var _dice_icon = new PIXI.Sprite(sensor_1_dice_icon);
		break;
		case 2:
		var _dice_icon = new PIXI.Sprite(sensor_2_dice_icon);
		break;
		case 3:
		var _dice_icon = new PIXI.Sprite(sensor_3_dice_icon);
		break;
		case 4:
		var _dice_icon = new PIXI.Sprite(sensor_4_dice_icon);
		break;
		case 5:
		var _dice_icon = new PIXI.Sprite(sensor_5_dice_icon);
		break;
		case 6:
		var _dice_icon = new PIXI.Sprite(sensor_6_dice_icon);
		break;
		case 7:
		var _dice_icon = new PIXI.Sprite(sensor_7_dice_icon);
		break;
		case 8:
		var _dice_icon = new PIXI.Sprite(sensor_8_dice_icon);
		break;
		case 9:
		var _dice_icon = new PIXI.Sprite(sensor_9_dice_icon);
		break;
		case 10:
		var _dice_icon = new PIXI.Sprite(sensor_10_dice_icon);
		break;
		case 11:
		var _dice_icon = new PIXI.Sprite(sensor_11_dice_icon);
		break;
		case 12:
		var _dice_icon = new PIXI.Sprite(sensor_12_dice_icon);
		break;
		case 13:
		var _dice_icon = new PIXI.Sprite(sensor_13_dice_icon);
		break;
	}

	// Scale the dice
	_dice_icon.scale.x = _dice_icon.scale.y = _scale_dice_ratio;
	
	/* 
		Setup the label textbox and position the icon
	*/
	
	// Left label
	if(x<winXfactor) 
	{	
		// Dice icon
		_dice_icon.position.x = 0-(_dice_icon.width/2);
		_dice_icon.position.y = 8;
		
		// Label display object position
		_stage_label.position.x = x+_distance_ratio;
		_stage_label.position.y = y-20;
		
		// Label text property
		var _stage_label_text = new PIXI.Text(labelText, sl_label_font);
		_stage_label_text.anchor.x = 0;
		_stage_label_text.anchor.y = 0;
		_stage_label_text.position.x = 0+(_dice_icon.width/2);
		
		// Label odds text
		var _stage_odds_text = new PIXI.Text(oddsText, sl_odds_font);
		_stage_odds_text.anchor.x = 0;
		_stage_odds_text.anchor.y = 0;
		_stage_odds_text.position.x = 0+(_dice_icon.width/2);
		_stage_odds_text.position.y = _stage_label_text.height-23;
	}
	// Right label
	if(x>winXfactor)
	{	
		// Dice icon
		_dice_icon.position.x = 0-(_dice_icon.width/2);
		_dice_icon.position.y = 8;
	
		// Label display object position
		_stage_label.position.x = x-_distance_ratio;
		_stage_label.position.y = y-20;
		
		// Label text property
		var _stage_label_text = new PIXI.Text(labelText, sl_label_font);
		
		_stage_label_text.anchor.x = 1;
		_stage_label_text.anchor.y = 0;
		
		_stage_label_text.position.x = 0-(_dice_icon.width/2);
		
		// Label odds text
		var _stage_odds_text = new PIXI.Text(oddsText, sl_odds_font);
		_stage_odds_text.anchor.x = 1;
		_stage_odds_text.anchor.y = 0;
		_stage_odds_text.position.x = 0-(_dice_icon.width/2);
		_stage_odds_text.position.y = _stage_label_text.height-23;
	}
	// Top label
	if((x==winXfactor) && (y<=500))
	{
		
		// Dice icon
		_dice_icon.position.x = 0-_dice_icon.width-10;
		_dice_icon.position.y = -5;
		
		// Label display object position
		_stage_label.position.x = x;
		_stage_label.position.y = y+25;
		
		// Label text property
		var _stage_label_text = new PIXI.Text(labelText, sl_label_font);
		_stage_label_text.anchor.x = 0;
		_stage_label_text.anchor.y = 0;
		_stage_label_text.position.x = -10;
		_stage_label_text.position.y = -8;
		
		// Label odds text
		var _stage_odds_text = new PIXI.Text(oddsText, sl_odds_font);
		_stage_odds_text.anchor.x = 0;
		_stage_odds_text.anchor.y = 0;
		_stage_odds_text.position.x = 0-(_dice_icon.width/2);
		_stage_odds_text.position.y = _stage_label_text.height-33;
	}
	// Bottom label
	if((x==winXfactor) && (y>=500))
	{
		// Dice icon
		_dice_icon.position.x = 0-_dice_icon.width-10;
		_dice_icon.position.y = -5;
		
		// Label display object position
		_stage_label.position.x = x;
		_stage_label.position.y = y+25;
		
		// Label text property
		var _stage_label_text = new PIXI.Text(labelText, sl_label_font);
		_stage_label_text.anchor.x = 0;
		_stage_label_text.anchor.y = 0;
		_stage_label_text.position.x = -10;
		_stage_label_text.position.y = -8;
		
		// Label odds text
		var _stage_odds_text = new PIXI.Text(oddsText, sl_odds_font);
		_stage_odds_text.anchor.x = 0;
		_stage_odds_text.anchor.y = 0;
		_stage_odds_text.position.x = 0-(_dice_icon.width/2);
		_stage_odds_text.position.y = _stage_label_text.height-33;
	}
	
	// Add the odds textbox to the control array
	stage_odds_text_control_arr.push(_stage_label);
	
	// Add the textbox to the label object
	_stage_label.addChild(_stage_label_text);
	
	// Add the textbox to the label object
	_stage_label.addChild(_stage_odds_text);
	
	// Add the dice icon
	_stage_label.addChild(_dice_icon);
	
	// Add the label object to the stage
	stage_elements[0].addChild(_stage_label);
}

/*********************************************
	Update odds display
*********************************************/
// Use this method to update between fractional 
// and decimal odds display
// Requires 1 parameter
// type = either f or d 
function updateOddsDisplay(type){
	// Delete the old labels
	deleteStageOddsLabels(type);
	// Set the game property
	switch(type)
	{
		case "f":
			game_properties.odds = "f";
			game_properties.odds_label = GDK.locale.get("g_set_odds_fractional");
		break;
		case "d":
			game_properties.odds = "d";
			game_properties.odds_label = GDK.locale.get("g_set_odds_decimal");
		break;
	}
	//setOdds(type);
	// Create the new ones
	setupStageLabels(type);
}

/****************************************
	Method to delete current stage 
	odds labels
****************************************/
// Requires 1 parameter
// type = either f or d 
function deleteStageOddsLabels(type)
{
	// Loop through the stage odds label objects and remove them
	for(var i=0;i<14;i++){
		stage_elements[0].removeChild(stage_odds_text_control_arr[i]);
	}
	
	// Reset the control array
	stage_odds_text_control_arr = [];
}

