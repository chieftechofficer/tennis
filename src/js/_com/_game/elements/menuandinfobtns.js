function setupMenuInfoBtns()
{
	/*************************************
		GDK menu button
	*************************************/
		
	// Add the drop down menu button
	gdk_menu_btn = new PIXI.Sprite(gdk_menu_btn_skin);
	gdk_menu_btn.position.x = menu_btn_positions[0];
	gdk_menu_btn.position.y = menu_btn_positions[1];
		
	gdk_menu_btn.interactive = true;
	gdk_menu_btn.buttonMode = true;
	
	/*************************************
		Button states
	*************************************/
	
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":
			gdk_menu_btn.mousedown = function()
			{	
				// Stop the bg tune
				mainLoop.stop();
		
				// Open Odobo menu
				GDK.ui.menu();
			}
		break;
		case "on":
			// Mobile touch and tap events	
			gdk_menu_btn.touchstart = function()
			{	
				// Stop the bg tune
				mainLoop.stop();
		
				// Open Odobo menu
				GDK.ui.menu();			
			}
		break;
	}
		
	stage.addChild(gdk_menu_btn);
		
	/*************************************
		Info menu button
	*************************************/
		
	// Add the drop down menu button
	info_btn = new PIXI.Sprite(info_btn_skin);
	info_btn.position.x = menu_btn_positions[2];
	info_btn.position.y = menu_btn_positions[3];
		
	info_btn.interactive = true;
	info_btn.buttonMode = true;
	
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":	
			info_btn.mousedown = function()
			{	
				$( "#screen_overlay" ).removeClass( "hidden" );
				// Indicate that the help content is open
				help_content = true;
				$( "#closeBtn" ).html( "X" );
			}
		break;	
		case "on":
			// Mobile touch and tap events	
			info_btn.touchstart = function()
			{	
				$( "#screen_overlay" ).removeClass( "hidden" );	
				// Indicate that the help content is open
				help_content = true;	
				$( "#closeBtn" ).html( "X" );
			}
		break;
	}
		
	stage.addChild(info_btn);	
}