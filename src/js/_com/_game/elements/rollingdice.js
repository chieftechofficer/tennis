/*************************************
	Dice for multi-role feature
*************************************/

function createRollingDice()
{
	// Dice1
	animated_dice[0] = new PIXI.Sprite(dice1_skin);
	animated_dice[0].position.x = dice_coords[0];
	animated_dice[0].position.y = dice_coords[1];
	stage.addChild(animated_dice[0]);

	// Dice2
	animated_dice[1] = new PIXI.Sprite(dice2_skin);
	animated_dice[1].position.x = dice_coords[2];
	animated_dice[1].position.y = dice_coords[3];
	stage.addChild(animated_dice[1]);

	// Dice3
	animated_dice[2] = new PIXI.Sprite(dice3_skin);
	animated_dice[2].position.x = dice_coords[4];
	animated_dice[2].position.y = dice_coords[5];
	stage.addChild(animated_dice[2]);	
	
}