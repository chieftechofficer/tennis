function setupChipSelector(x,y,type)
{
	/***************************************
		Set skin
	***************************************/
	// Check if the device is mobile, tablet or desktop
	switch(game_properties.device)
	{
		case "Desktop":
			switch(type)
			{
				case "up":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_up.png");
				break;
				case "down":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_down.png");
				break;
			}
		break;
		case "Tablet":
			switch(type)
			{
				case "up":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_up.png");
				break;
				case "down":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_down.png");
				break;
			}
		break;
		case "Mobile":
			switch(type)
			{
				case "up":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_up.png");
				break;
				case "down":
					var _c_sel_skin = new PIXI.Texture.fromImage("game/images/Chip_select_down.png");
				break;
			}
		break;
	}
	
	var _c_sel = new PIXI.Sprite(_c_sel_skin);
	
	/**************************************
		Configure selector
	**************************************/
	
	_c_sel.position.x = x;
	_c_sel.position.y = y;
	
	_c_sel.buttonMode = true;
	_c_sel.interactive = true;
	
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":
			_c_sel.mousedown = function(data)
			{	
				switch(type)
				{
					case "up":
						setChipValue("u");
					break;
					case "down":
						setChipValue("d");
					break;
				}
			}
		break;
		case "on":
			_c_sel.touchstart = function(data)
			{	
				switch(type)
				{
					case "up":
						setChipValue("u");
					break;
					case "down":
						setChipValue("d");
					break;
				}
			}
		break;
	}
	
	chip_selectors.push(_c_sel);
	
	control_panel.addChild(_c_sel);
}

function setChipValue(d)
{
	// Variable to store chip array location
	var _s_c_v_t = 0;
	
	// Loop through the chip array and check to 
	// see which position is currently active
	for(i=0;i<chip_values.length;i++)
	{
		if(chip_values[i]==current_chip)
		{
			_s_c_v_t=i;
			break;
		}
	}
	
	// Play sound
	//GDK.audio.play("singlechip");
	
	// Which direction should the chips move in
	switch(d)
	{
		case "u":
			if((_s_c_v_t+1)<chip_values.length)
			{	
				// Move the currently selected chip
				table_chip_array[_s_c_v_t].position.y = winYfactor+250;
				//TweenLite.to(table_chip_array[_s_c_v_t], 0.5, {alpha:0});
				// Move the newly selected chip into position
				table_chip_array[_s_c_v_t+1].position.x = chip_positions[0];
				table_chip_array[_s_c_v_t+1].position.y = chip_positions[1];
				TweenLite.fromTo(table_chip_array[_s_c_v_t+1], 0.5, {alpha:0},{alpha:1});
				// Set the current chip to the selected chip
				current_chip = chip_values[_s_c_v_t+1];
			} 
			else
			{	
				// Move the currently selected chip
				table_chip_array[_s_c_v_t].position.y = winYfactor+250;
				//TweenLite.to(table_chip_array[_s_c_v_t], 0.5, {alpha:0});
				// Move the newly selected chip into position
				table_chip_array[0].position.x = chip_positions[0];
				table_chip_array[0].position.y = chip_positions[1];
				TweenLite.fromTo(table_chip_array[0], 0.5, {alpha:0},{alpha:1});
				// Set the current chip to the selected chip
				current_chip = chip_values[0];
			}
		break;
		case "d":
			if(_s_c_v_t==0)
			{	
				// Move the currently selected chip
				table_chip_array[0].position.y = winYfactor+250;
				// Move the newly selected chip into position
				table_chip_array[chip_values.length-1].position.x = chip_positions[0];
				table_chip_array[chip_values.length-1].position.y = chip_positions[1];
				TweenLite.fromTo(table_chip_array[chip_values.length-1], 0.5, {alpha:0},{alpha:1});
				// Set the current chip to the selected chip
				current_chip = chip_values[chip_values.length-1];
			}
			else 
			{
				// Move the currently selected chip
				table_chip_array[_s_c_v_t].position.y = winYfactor+250;
				// Move the newly selected chip into position
				table_chip_array[_s_c_v_t-1].position.x = chip_positions[0];
				table_chip_array[_s_c_v_t-1].position.y = chip_positions[1];
				TweenLite.fromTo(table_chip_array[_s_c_v_t-1], 0.5, {alpha:0},{alpha:1});
				// Set the current chip to the selected chip
				current_chip = chip_values[_s_c_v_t-1];		
			}
		break;
	}
	
}