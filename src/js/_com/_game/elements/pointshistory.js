/***********************************
	Points history create method
***********************************/
	
function createPointsHistory()
{
	/*************************************
		Set some vars for the rows 
		and cols
	*************************************/
	
	var _col1_x = 17;
	var _col2_x = 157;
	var _col3_x = 297;
	
	// Y positions for rows
	var _row1 = 23;
	var _row2 = 48;
	var _row3 = 70;
	var _row4 = 90;
	
	/*************************************
		Create the display comntainer
	*************************************/
	
	var _points_history = new PIXI.DisplayObjectContainer();
		
	/*************************************
		Background 
	*************************************/
	
	var _ph_bg = new PIXI.Sprite(ph_bg_skin);
	
	_ph_bg.position.x = 0;
	_ph_bg.position.y = 0;
	
	_points_history.addChild(_ph_bg);
	
	/*************************************
		Setup the title
	*************************************/
		
	//var _p_title = new PIXI.Text(GDK.locale.get("g_points_history"), {font: ph_title_font, fill: t_col, align: "left"});
	var _p_title = new PIXI.Text("", {font: ph_title_font, fill: t_col, align: "left"});
		
	_p_title.anchor.x = 0;
	_p_title.anchor.y = 0;
		
	_p_title.position.x = 5;
	_p_title.position.y = 0;
		
	_points_history.addChild(_p_title);
		
	/*************************************
		Position the pointshistory 
		display object
	*************************************/
		
	_points_history.position.x = points_history_pos[0];
	_points_history.position.y = points_history_pos[1];
		
	/*************************************
		Setup the column titles
	*************************************/
		
	/*
		Dice1, Dice2, Dice3
	*/
	
	////////////////////////// Row1
	var _p_dice1 = new PIXI.Text(GDK.locale.get("g_points_history_dice1"), {font: ph_text_font, fill: t_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
	
	_p_dice1.anchor.x = 0;
	_p_dice1.anchor.y = 0;
		
	_p_dice1.position.x = _col1_x;
	_p_dice1.position.y = _row1;
		
	_points_history.addChild(_p_dice1);
		
	points_history_box.push(_p_dice1); // <-- Array position 3
			
	var _p_dice2 = new PIXI.Text(GDK.locale.get("g_points_history_dice2"), {font: ph_text_font, fill: t_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_dice2.anchor.x = 0;
	_p_dice2.anchor.y = 0;
		
	_p_dice2.position.x = _col2_x;
	_p_dice2.position.y = _row1;
		
	_points_history.addChild(_p_dice2);
		
	points_history_box.push(_p_dice2); // <-- Array position 4
		
	var _p_dice3 = new PIXI.Text(GDK.locale.get("g_points_history_dice3"), {font: ph_text_font, fill: t_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_dice3.anchor.x = 0;
	_p_dice3.anchor.y = 0;
		
	_p_dice3.position.x = _col3_x;
	_p_dice3.position.y = _row1;
		
	_points_history.addChild(_p_dice3);
		
	points_history_box.push(_p_dice3); // <-- Array position 5
		
	/*************************************
		Setup the data rows
	*************************************/
		
	////////////////////////// Row2
	var _p_row1_1 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row1_1.anchor.x = 0;
	_p_row1_1.anchor.y = 0;
		
	_p_row1_1.position.x = _col1_x;
	_p_row1_1.position.y = _row2;
		
	_points_history.addChild(_p_row1_1);
		
	points_history_box.push(_p_row1_1); // <-- Array position 3
		
	var _p_row1_2 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row1_2.anchor.x = 0;
	_p_row1_2.anchor.y = 0;
		
	_p_row1_2.position.x = _col2_x;
	_p_row1_2.position.y = _row2;
		
	_points_history.addChild(_p_row1_2);
		
	points_history_box.push(_p_row1_2); // <-- Array position 4
		
	var _p_row1_3 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row1_3.anchor.x = 0;
	_p_row1_3.anchor.y = 0;
		
	_p_row1_3.position.x = _col3_x;
	_p_row1_3.position.y = _row2;
		
	_points_history.addChild(_p_row1_3);
		
	points_history_box.push(_p_row1_3); // <-- Array position 5
		
	////////////////////////// Row3
	var _p_row2_1 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row2_1.anchor.x = 0;
	_p_row2_1.anchor.y = 0;
		
	_p_row2_1.position.x = _col1_x;
	_p_row2_1.position.y = _row3;
		
	_points_history.addChild(_p_row2_1);
		
	points_history_box.push(_p_row2_1); // <-- Array position 6
		
	var _p_row2_2 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row2_2.anchor.x = 0;
	_p_row2_2.anchor.y = 0;
		
	_p_row2_2.position.x = _col2_x;
	_p_row2_2.position.y = _row3;
		
	_points_history.addChild(_p_row2_2);
		
	points_history_box.push(_p_row2_2); // <-- Array position 7
		
	var _p_row2_3 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row2_3.anchor.x = 0;
	_p_row2_3.anchor.y = 0;
		
	_p_row2_3.position.x = _col3_x;
	_p_row2_3.position.y = _row3;
		
	_points_history.addChild(_p_row2_3);
		
	points_history_box.push(_p_row2_3); // <-- Array position 8
		
	////////////////////////// Row4
	var _p_row3_1 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row3_1.anchor.x = 0;
	_p_row3_1.anchor.y = 0;
		
	_p_row3_1.position.x = _col1_x;
	_p_row3_1.position.y = _row4;
		
	_points_history.addChild(_p_row3_1);
		
	points_history_box.push(_p_row3_1); // <-- Array position 9
		
	var _p_row3_2 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row3_2.anchor.x = 0;
	_p_row3_2.anchor.y = 0;
		
	_p_row3_2.position.x = _col2_x;
	_p_row3_2.position.y = _row4;
		
	_points_history.addChild(_p_row3_2);
		
	points_history_box.push(_p_row3_2); // <-- Array position 10
		
	var _p_row3_3 = new PIXI.Text("-", {font: ph_text_font, fill: sc_col, align: "left",wordWrap: true, wordWrapWidth: wordWrapWidth});
		
	_p_row3_3.anchor.x = 0;
	_p_row3_3.anchor.y = 0;
		
	_p_row3_3.position.x = _col3_x;
	_p_row3_3.position.y = _row4;
		
	_points_history.addChild(_p_row3_3);
		
	points_history_box.push(_p_row3_3); // <-- Array position 11
		
	// Add the points history display object to the control array
	points_history_box.push(_points_history); // <-- Array position 12
	
	// Add the complete points history box to the game
	stage.addChild(_points_history);
		
	// Popultae points history
	populatePointsHistory();
}