/****************************************
	Create the stage logo
****************************************/

function createStageLogo(x,y)
{
	// Create sprite
	stage_logo = new PIXI.Sprite(stage_logo_skin);
	stage_logo.anchor.x = 0.5;
	stage_logo.position.x = x;
	stage_logo.position.y = y;
	
	stage.addChild(stage_logo);
}

/*******************************************
	Hide stage logo
*******************************************/

function hideStageLogo()
{
	// Fist check if the pointshistory flag is set to true
	// If true then the ph will be displayed
	if(showHidePHFlag)
		stage_logo.width = 0;
}

/*******************************************
	Show points history
*******************************************/

function showStageLogo()
{
	// Fist check if the pointshistory flag is set to true
	//if(showHidePHFlag)
	if(stage_logo)
		stage_logo.width = 442;
}