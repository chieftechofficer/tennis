function setupStage()
{	
	/****************************************
		Load background and court skins
	****************************************/
	
	// This must be included to ensure the game 
	// screen has a complete background.
	var _stage_background_skin =  PIXI.Texture.fromImage("game/images/BlackBG.jpg");
	var _stage_background = new PIXI.Sprite(_stage_background_skin);
	_stage_background.width = winWidth;
	_stage_background.height = winHeight;
	_stage_background.position.x = 0;
	_stage_background.position.y = 0;
	stage.addChild(_stage_background);
	
	// Apply the background skin
	bg_stage = new PIXI.Sprite(bg_stage_skin);
	bg_stage.anchor.x = 0.5;
	bg_stage.anchor.y = 0;
	bg_stage.position.x = game_bg_skin_pos[0];
	bg_stage.position.y = game_bg_skin_pos[1];
	stage.addChild(bg_stage);
	
	// Apply the court, field or pitch skin
	bg_default = new PIXI.Sprite(court_skin);
	bg_default.anchor.x = 0.5;
	bg_default.anchor.y = 0;
	bg_default.position.x = game_court_pos[0];
	bg_default.position.y = game_court_pos[1];
		
	// Add background to stage
	stage.addChild(bg_default);
	
	
	// Set balance
	setBalance(balance);
		
	/***********COME BACK TO THIS AND ADD A MESSAGE BOX *************/
	// Set game indicator text
				
	//setStageTxt(GDK.locale.get("g_place_your_bets"));
}	