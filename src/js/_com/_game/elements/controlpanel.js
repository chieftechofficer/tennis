/***************************************
	Control panel 
***************************************/
// This is used to house the controls 
// for the game

function setupControlPanel()
{
	// Setup display object
	control_panel = new PIXI.DisplayObjectContainer();
	control_panel.position.x = control_panel_position[0];
	control_panel.position.y = control_panel_position[1];
	
	// Setup the background
	var _control_panel_bg = new PIXI.Sprite(control_panel_skin);
	_control_panel_bg.position.x = 0;
	_control_panel_bg.position.y = 0;
	
	// Setup control panel
	control_panel.addChild(_control_panel_bg);
	
	// Add the control panel to the stage
	stage.addChild(control_panel);
	
}