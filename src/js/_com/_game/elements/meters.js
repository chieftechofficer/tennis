/****************************************
	Create the various meters
****************************************/

// But first create a container for the meters
function createMeterContainer(x,y)
{
	meter_container = new PIXI.Sprite(blank_skin);
	meter_container.anchor.x = 0.5;
	meter_container.anchor.y = 0;
	meter_container.position.x = x;
	meter_container.position.y = y;
	stage.addChild(meter_container);
	//$("#debuglog").html("Meter positions: " + meter_container_positions[0] + ", " + meter_container_positions[1]);	
}

/*
	Credit/balance, Stake and Bank/Winnings
*/

function createMeter(x,y,label,value)
{
	// Create a display object
	var _meter = new PIXI.DisplayObjectContainer();
	_meter.position.x = x;
	_meter.position.y = y;
	
	/*
		Create left side
	*/
	
	var _meter_left = new PIXI.Sprite(meter_skin_left);
	_meter_left.position.x = 0;
	_meter_left.position.y = 0;
	
	// Add left to the meter display object
	_meter.addChild(_meter_left);
	
	// Add the sprite to the meter control array
	meter_control_array.push(_meter_left); // Array position 0/6/12
	
	/*
		Create center side
	*/
	
	var _meter_center = new PIXI.Sprite(meter_skin_center);
	_meter_center.position.x = _meter_left.width;
	_meter_center.position.y = 0;
	
	// Add left to the meter display object
	_meter.addChild(_meter_center);
	
	// Add the sprite to the meter control array
	meter_control_array.push(_meter_center); // Array position 1/7/13

	/*
		Create right side
	*/
	
	var _meter_right = new PIXI.Sprite(meter_skin_right);
	_meter_right.position.x = _meter_left.width+_meter_center.width;
	_meter_right.position.y = 0;
	
	// Add left to the meter display object
	_meter.addChild(_meter_right);
	
	// Add the sprite to the meter control array
	meter_control_array.push(_meter_right); // Array position 2/8/14
	
	/*
		Meter text box
	*/
	
	var _meter_text = new PIXI.Text(value, meter_text);
	_meter_text.position.x = (_meter_right.width+_meter_center.width+_meter_left.width)/2;
	_meter_text.position.y = 23;
	_meter_text.anchor.x = 0.5;
	
	// Add the meter text to the meter display object
	_meter.addChild(_meter_text);
	
	// Add the text tot he control array
	meter_control_array.push(_meter_text); // Array position 3/9/15
	
	/*
		Meter label
	*/
	
	var _meter_label = new PIXI.Text(label, meter_label);
	_meter_label.position.x = (_meter_right.width+_meter_center.width+_meter_left.width)/2;
	_meter_label.position.y = 55;
	_meter_label.anchor.x = 0.5;
	
	// Add the meter text to the meter display object
	_meter.addChild(_meter_label);
	
	// Add the label to the control array
	meter_control_array.push(_meter_label); // Array position 4/10/16
	
	/*
		Final bits
	*/
	
	// Add the meter display object to the meter control array
	meter_control_array.push(_meter); // Array position 5/11/17
		
	// Add the meter object to the stage
	meter_container.addChild(_meter);
	
}

/*************************************************
	Compare the text and label positions 
	and return the largest value
*************************************************/

function compareMeterSizes(m1Label,m1Text,m2Label,m2Text,m3Label,m3Text)
{
	return Math.max(m1Label,m1Text,m2Label,m2Text,m3Label,m3Text);
}

/*************************************************
	Resize meters according to the 
	biggest value to keep things 
	looking symetrical
*************************************************/
// This function takes 2 parameters which is the 
// meter that is being adjusted 
// and where in the horizontal/vertical stack is the meter placed
function meterAdjust(meter,placement)
{
	
	// Work out which position the label and text for this meter are within the meter control array
	switch(meter)
	{
		case 5:
			var _left_bg = meter_control_array[0];
			var _center_bg = meter_control_array[1];
			var _right_bg = meter_control_array[2];			
			var _text = meter_control_array[3];
			var _label = meter_control_array[4];
			
		break;	
		case 11:
			var _left_bg = meter_control_array[6];
			var _center_bg = meter_control_array[7];
			var _right_bg = meter_control_array[8];	
			var _text = meter_control_array[9];
			var _label = meter_control_array[10];
		break;	
		case 17:
			var _left_bg = meter_control_array[12];
			var _center_bg = meter_control_array[13];
			var _right_bg = meter_control_array[14];	
			var _text = meter_control_array[15];
			var _label = meter_control_array[16];
		break;	
	}
	
	/*
		Calculate positions of the meter elements
	*/
	
	// Left border bg is always 0
	var _left_bg_xpos = 0;
	
	// Center bg x position
	var _center_bg_xpos = meter_control_array[0].width; // The center bg should be placed at the end of the left bg
	
	// Now calculate the length of the center bg by comparing the textbox sizes and reszie the center bg to be the largest width
	var _center_bg_width = compareMeterSizes(meter_control_array[3].width,meter_control_array[4].width,meter_control_array[9].width,meter_control_array[10].width,meter_control_array[15].width,meter_control_array[16].width);
	
	// Right bg
	var _right_bg_xpos = meter_control_array[0].width+_center_bg_width; // Add the left bg width and the centerbg width
	
	// Calculate the new width of the meter by adding the left, center and right bg
	var _total_width = (meter_control_array[0].width*2)+_center_bg_width;
	
	/*
		Position the meter elements
	*/
	
	// Position the left bg
	_left_bg.position.x = _left_bg_xpos
	
	// Position the center bg
	_center_bg.position.x = _center_bg_xpos
	
	// Size the center bg
	_center_bg.width = _center_bg_width;

	// Position the right bg
	_right_bg.position.x = _right_bg_xpos
	
	// Position the text
	_text.position.x = _total_width/2;
	
	// Position the label
	_label.position.x = _total_width/2;
	
	//$("#debuglog").html("none");
	
	// Position the meter
	switch(game_properties.meterlayout)
	{
		case "v": // <-- Vertical
			meter_control_array[meter].position.x = 0;
			meter_control_array[meter].position.y = meter_container_positions[1] + (placement*_center_bg.height);
			//$("#debuglog").append("v<br> placement:" + (placement*_center_bg.height)+10 + "<br>placement: " + (placement*_center_bg.height)+10 + "<br>placement:" + (placement*_center_bg.height)+10);
			
			//$("#debuglog").html("Meter positions: " + meter_container_positions[0] + ", " + meter_container_positions[1]);
		break;
		case "h": // <-- Horizontal
			//$("#debuglog").html("h");
			meter_control_array[meter].position.x = (_total_width+10)*placement;
			meter_control_array[meter].position.y = 0;
		break;
	}
	
	//$("#debuglog").append("<br>**********************************<br>_left_bg_xpos: " + _left_bg_xpos + "<br>_center_bg_xpos: " + _center_bg_xpos + "<br>_right_bg_xpos: " + _right_bg_xpos + "<br>_total_width/2: " + _total_width/2 + "<br>(_total_width+10)*placement: " + (_total_width+10)*placement + "<br>**********************************<br>" );
	
	meter_container_width = (_total_width+10)*3;
	
	// Place the meter container
	meterplacement();
}

/*************************************************
	Move the meter container into place
*************************************************/

function meterplacement()
{
	// Position the meter
	switch(game_properties.meterlayout)
	{
		case "v": // <-- Vertical
			meter_container.position.x = meter_container_positions[0];
			
			//$("#debuglog").html("Meter positions: " + meter_container_positions[0] + ", " + meter_container_positions[1]);
		break;
		case "h": // <-- Horizontal
			// Plus 10 because there is a slight mis alignment with the court on the table
			meter_container.position.x = winXfactor - (meter_container_width/2)+10;
		break;
	}
	
	meter_container.position.y = meter_container_positions[1];
}