/*************************************
	Font styles
*************************************/
function setFontStyles()
{	
	// Use only for testing
	//locale = "zh-Hans";

	// Determine locale
	switch(locale)
	{
		 case "en": // English
		 	comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Varela Round";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "gb": // Bulgarian
		 	comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "zh-Hans": // Simplified Chineese
		 	comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 2;
		 break;
		 case "zh-Hant": // Traditional Chineese
			comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "fr": // French
		 	comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Varela Round";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "de": // German
		 	comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Varela Round";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "hu": // Hungarian
		 	comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "it": // Italian
		 	comfortaa = "Comfortaa";
			exo = "Exo";
			varela = "Varela Round";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "pl": // Polish
		 	comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "pt": // Portugueese
		 	comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "ro": // Romanian
		 	comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "es": // Spanish
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "sv": // Swedish
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "cs": // Czech
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "da": // Danish
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "el": // Greek
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "jp": // Japanese
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "no": // Norwegian
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "ru": // Russian
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
		 case "sk": // Slovak
			comfortaa = "Arial";
			exo = "Arial";
			varela = "Arial";
			arial = "Arial";
			font_size_ratio = 1;
		 break;
	}
	
	// Set the font sizes according to device
	switch(game_properties.device)
	{
		case "Desktop":
			// Dice result font style
			dice_result_fs = {font: "16px " + exo, fill: "white", align: "center"};
			// Game buttons
			btn_text_font_style = {font: "19px " + exo, fill: "white", align: "center"};
			// Hover labels
			label_font_style = {font: "16px " + comfortaa, fill: "white", align: "center"};
			left_label_ypos = 10;
			// Main game button text
			game_ind_font_style = {font: "bold 23px " + exo, fill: "white", align: "center"};
			game_ind_font_po_style = {font: "bold 18px " + exo, fill: "white", align: "left"};
			// Stage info text
			stage_info_font_style = {font: "23px " + comfortaa, fill: "white", align: "center"};
			// Meter font styles
			meter_label = {font: "22px " + exo, fill: "#e4b600", align: "center",stroke: "#000", strokeThickness: 2}; 
			meter_text = {font: "25px " + exo, fill: "#FFF", align: "center"}; 
			// Stage odds label font styles
			slwordWrapWidth = 250;
			sl_label_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			sl_odds_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			// NOTE: Points history styles are only required for desktop
			// Points history font styles
			ph_title_font = "18px " + comfortaa;
			ph_text_font = "14px " + comfortaa;
			t_col = "#FFFFFF";
			sc_col = "#FFCC00";
			wordWrapWidth = 100;
		break;
		case "Tablet":
			// Dice result font style
			dice_result_fs = {font: "19px " + exo, fill: "white", align: "center"};
			// Game buttons
			btn_text_font_style = {font: "19px " + exo, fill: "white", align: "center"};
			// Hover labels
			label_font_style = {font: "16px " + comfortaa, fill: "white", align: "center"};
			left_label_ypos = 10;
			// Main game button text
			game_ind_font_style = {font: "bold 23px " + exo, fill: "white", align: "center"};
			game_ind_font_po_style = {font: "bold 18px " + exo, fill: "white", align: "left"};
			// Stage info text
			stage_info_font_style = {font: "23px " + comfortaa, fill: "white", align: "center"};
			// Meter font styles
			meter_label = {font: "22px " + exo, fill: "#e4b600", align: "center",stroke: "#000", strokeThickness: 2}; 
			meter_text = {font: "25px " + exo, fill: "#FFF", align: "center"}; 
			// Stage odds label font styles
			slwordWrapWidth = 250;
			sl_label_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			sl_odds_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			// NOTE: Points history styles are only required for desktop
			// Points history font styles
			ph_title_font = "18px " + comfortaa;
			ph_text_font = "14px " + comfortaa;
			t_col = "#FFFFFF";
			sc_col = "#FFCC00";
			wordWrapWidth = 100;
		break;
		case "Mobile":
			// Dice result font style
			dice_result_fs = {font: "19px " + exo, fill: "white", align: "center"};
			// Game buttons
			btn_text_font_style = {font: "19px " + exo, fill: "white", align: "center"};
			// Hover labels
			label_font_style = {font: "16px " + comfortaa, fill: "white", align: "center"};
			left_label_ypos = 10;
			// Main game button text
			game_ind_font_style = {font: "bold 23px " + exo, fill: "white", align: "center"};
			game_ind_font_po_style = {font: "bold 18px " + exo, fill: "white", align: "left"};
			// Stage info text
			stage_info_font_style = {font: "23px " + comfortaa, fill: "white", align: "center"};
			// Meter font styles
			meter_label = {font: "22px " + exo, fill: "#e4b600", align: "center",stroke: "#000", strokeThickness: 2}; 
			meter_text = {font: "25px " + exo, fill: "#FFF", align: "center"}; 
			// Stage odds label font styles
			slwordWrapWidth = 250;
			sl_label_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			sl_odds_font = {font: " 17px " + comfortaa, fill: "#FFFFFF", align: "left",wordWrap: true, wordWrapWidth: slwordWrapWidth, stroke: "#2a60bd", strokeThickness: 5};
			// NOTE: Points history styles are only required for desktop
			// Points history font styles
			ph_title_font = "18px " + comfortaa;
			ph_text_font = "14px " + comfortaa;
			t_col = "#FFFFFF";
			sc_col = "#FFCC00";
			wordWrapWidth = 100;
		break;
	}	
}