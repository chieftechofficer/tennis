/****************************************
	In game buttons 
*****************************************/

function controlBtn(type,x,y)
{	
	// Button textbox
	var _btn_txt;
		
	switch (type)
    {
		case 1: // Undo
			_btn_txt = GDK.locale.get("g_undo_bet");
			
			// Buttons such as clear, repeat bet and undo
			var _game_btn_up = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_up.png");
			var _game_btn_down = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_down.png");
			var _game_btn_hover = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_hover.png");
			
		break;
		case 2: // Clear
			_btn_txt = GDK.locale.get("g_clear_bet");
			
			// Buttons such as clear, repeat bet and undo
			var _game_btn_up = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_red_up.png");
			var _game_btn_down = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_red_down.png");
			var _game_btn_hover = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_red_hover.png");
			
		break;
		case 3: // Repeat
			_btn_txt = GDK.locale.get("g_repeat_bet");
			
			// Buttons such as clear, repeat bet and undo
			var _game_btn_up = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_up.png");
			var _game_btn_down = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_down.png");
			var _game_btn_hover = PIXI.Texture.fromImage("game/images/Game_buttons/Game_buttons_hover.png");
		break;
	}
		
	// Create a display object to contain the button and textbox
	var _game_button_container = new PIXI.DisplayObjectContainer();
		
	/****************************************
		Create the button sprite
	****************************************/
		
	// Set the game button sprite
	var _game_button = new PIXI.Sprite(_game_btn_up);
		
	_game_button.position.x = 0;
	_game_button.position.y = 0;
		
	// var game_button_clear = new PIXI.Sprite(game_button_skin);
		
	_game_button.interactive = true;
	_game_button.buttonMode = true;
	
	// Check if the device is desktop or mobile
	switch(game_properties.touchevents)
	{
		case "off":
			_game_button.mouseover = _game_button.mouseup = function()
			{
				this.setTexture(_game_btn_hover);
			}
				
			_game_button.mouseout = function()
			{
				this.setTexture(_game_btn_up);
			}
		
			_game_button.mousedown = function()
			{	
				// Set the skin
				this.setTexture(_game_btn_down);
				// Call the function
				switch (type)
				{
					case 1:
						undoLastMove();
					break;
					case 2:
						resetgame();
					break;
					case 3:
						repeatBet();
					break;
				}
			}
		break;
		case "on":
			_game_button.touchstart = function()
			{	
				// Set the skin
				this.setTexture(_game_btn_down);
				// Call the function
				switch (type)
				{
					case 1:
						undoLastMove();
					break;
					case 2:
						resetgame();
					break;
					case 3:
						repeatBet();
					break;
				}
			}
		break;
	}
		
	/****************************************
		Create the text box
	****************************************/
	
	var _game_button_text = new PIXI.Text(_btn_txt, btn_text_font_style);
	_game_button_text.anchor.x = 0.5;
	_game_button_text.anchor.y = 0.5;
	_game_button_text.position.x = _game_button.width/2;
	_game_button_text.position.y = _game_button.height/2;
			
	/****************************************
		Combine the elements
	****************************************/
	
	// Add the interactive element to the control array
	bet_btn_interactive_control.push(_game_button);
	// Add the background to the container
	_game_button_container.addChild(_game_button);
	// Add the text to the container
	_game_button_container.addChild(_game_button_text);	
	// Position the button container on the stage
	_game_button_container.position.x = x;
	_game_button_container.position.y = y;
	// Add the container to the control array
	game_button_array.push(_game_button_container);
	// Add the button container to the stage
	control_panel.addChild(_game_button_container);
}