/*********************************************
	Set globale variables
**********************************************/

/*********************************************
	Stage and renderer
*********************************************/

var renderer,stage;

/*********************************************
	Audio
**********************************************/
	
var mainLoop;
	
/*********************************************
	Locale
**********************************************/
	
var locale;
	
/*********************************************
	Balance
**********************************************/
	
var balance;
	
/*********************************************
	Screen size
**********************************************/
	
var winWidth;
var winHeight;

// Use this to place objects
var winXfactor;
var winYfactor;
		
/*********************************************
	Dice
**********************************************/
// Dice values for each stage of the game. Some stages have 2 dice with 
// different outcomes
// * is the amount of times the label appears on a dice

/////////// Stage 1 - Both sides
// 6 = Sensor 7 * 3
// 1 = Sensor 6 * 3

/////////// Stage 2 - Top side
// 3 = Sensor 0 * 3
// 2 = Sensor 1 * 2
// 1 = Sensor 2 * 1
           
/////////// Stage 2 - Bottom side
// 3 = Sesnor 5 * 3
// 2 = Sensor 4 * 2
// 1 = Sensor 3 * 1
           
/////////// Stage 3 - Top side
// 4 = Sensor 8 * 3
// 5 = Sensor 9 * 2
// 6 = Sensor 10 * 1
           
/////////// Stage 3 - Bottom side
// 4 = Sensor 13 * 3
// 5 = Sensor 12 * 2
// 6 = Sensor 11 * 1
	
// Save the dice result from the engine in slots 1,2,3
var engine_dice_result = ["-","-","-","-"];
	
/*********************************************
	Sensors
**********************************************/
	
// Create an array for the sensors
var sensors = [];
	
// Sensor odds - NOTE: These numbers are used to calculate winnings too
var sensor_odds = [3.7,5.5,11.0,11.0,5.5,3.7,1.9,1.9,7.5,11.0,22.0,22.0,11.0,7.5];
	
// Sensor odds fractional display
var sensor_odds_fractional = ["27/10","9/2","10/1","10/1","9/2","27/10","9/10","9/10","13/2","10/1","21/1","21/1","10/1","13/2"];

// There are 2 values for each sensor
// First is the amount of chips currently on that sensor example 3 chips 
// Second is the value of the chips example £20
var sensor_values = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	
// Sensor switch to determine if its on or off
// 0 = off
// 1 = on or active
var sensor_switch = [1,1,1,1,1,1,1,1,1,1,1,1,1,1];
	
// Required to indicate whether this is a winning sensor or not
// Winning sensors should not be labelled as deactivated
// 0 = nothing won
// 1 = winning sensor
var winning_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var selected_sensor_switch = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	
// Create an array to control each sensor with
var sensor_control_array = [];
	
/*********************************************
	Labels
**********************************************/
// setinterval var which will hide the labels that appear over the sensors
var tc;
	
// Use this to help resize elements for different resolutions
var pixel_ratio = 7;

// Set the text for the labels on the stage
var sensor_labels;
	
/*********************************************
	Game Logic
**********************************************/
var game_stage = 1;
	
// The current status of the game
var game_status = "play";
	
// Add the sensor and the value of the chip added
var last_move = [];
	
// Store the repeat bet
var repeat_bet = [];
	
// The previous selected dice
var selected_dice_array = [0,0,0];

// Store playable stages for each game
var playable_stages = 0;

// Phases
var phases = 5;
	
// Set an interval for the end of play notification
// This will allow you to show the user an animation 
// or whatever before moving on.
var move_on_interval;
	
// Animate dice
var dice_anim_interval;
	
// BONUS FEATURE --- Important
// Stage wins which is used to determine the bonus feature
var bonus = false;
var bonus_amount = 0;
	
/*********************************************
	Scoreboard and winnings
**********************************************/

// Main game stake
var stake = 0;
// Stake used for reconnection
var stage_stake = [0,0,0];
// Store the stage win
var stage_wins = [0,0,0];
// Save the total winnings for game in this variable
var total_game_winnings = 0;
// Var to store losses
var losses = 0;
// Store the wins from each round and use this to populate the points history array
var stage_win_label = [];
	
/*********************************************
	Game properties
**********************************************/
// odds = this is for the decimal or fractional odds display
// odds_label = use this to check which option should be highlighted in the menu
// playing_area = is the skin of the current stage of the game
// game_type = the game that is being played Tennis, football, cricket etc
// sounds = lets us know whether the user has it switched on or off 
// device = Which device the game is being loaded onto ie desktop mobile or tablet
// stage = is the current stage the game is on
// touchevents = this is to determine if the current device supports touch events
// meterlayout = h or v; which will layout the meters horizontally or vertically
// NOTE: There may be a case where a device supports both but this is not currently built in to the game
var game_properties = { odds: "d", odds_label: "Decimal", playing_area: "default", game_type: "Tennis", sounds: "Sounds On", device: "Desktop", stage: "one", touchevents: "off", meterlayout: "h" };
//var game_properties = { odds: "d", odds_label: "Decimal", playing_area: "default", game_type: "Tennis", sounds: "Sounds On", device: "Tablet", stage: "one", touchevents: "off" };
	
/*********************************************
	Stats
**********************************************/
	
var games_played = 0;

/**********************************************
	Odobo comms
**********************************************/

// How many sensors per stage
var sensors_per_stage = [2,3,3,3,3];

// Sensor locations per stage
var sensor_loc_p_s = [[6,7],[0,1,2],[3,4,5],[8,9,10],[11,12,13]]; 

// Map dice face to sensors 
var dice_sensor_map = [3,2,1,1,2,3,1,6,4,5,6,6,5,4];

// Setup labels to communicate to the engine
var spots = [];

for (i=0;i<sensor_switch.length;i++)
{
	spots[i] = "SPOT_" + i;
}

// Stage names for engine
var availableStages = [];
availableStages.push("STAGE_1");
availableStages.push("STAGE_2_TOP");
availableStages.push("STAGE_2_BOTTOM");
availableStages.push("STAGE_3_TOP");
availableStages.push("STAGE_3_BOTTOM");

var stage_groups = [
						availableStages[0],6, // Stage 1
						availableStages[2],3, // Stage 2 bottom
						availableStages[4],11, // Stage 3 bottom
						availableStages[1],0, // Stage 2 top
						availableStages[3],8 // Stage 3 top
					];

/*********************************************
	Chips
**********************************************/
	
// Interactive chip
var chip;
	
// Game chip
var table_chip;
	
// Array to store the gamechips (selectable ones) on the table
var table_chip_array = [];
	
// Array to store the chips worth
var chip_values = [];
var chip_values = [100,200,500,1000,2000,5000]; // Use this until you have solved the bug #5
	
// Current chip
var current_chip;
		
// Required for the stacks
var game_chips = [];
var stacked_chips = [];
var stacked_chips_index = [];
	
// Store the level of each chip 
var stacked_chip_containers = [];
	
// Chip size variables
var y_chip_factor = -45; // Used to adjust the chip y positions
var chip_height = 66;
var chip_size_ratio = 0.5;
	
// Chip positions
var chip_positions;
	
// Chip selectors
var chip_selectors = [];
	
/*********************************************
	Other visual elements
**********************************************/

// GDK menu button and info button
var gdk_menu_btn, info_btn;

// Positioning of the GDK menu and info buttons
var menu_btn_positions = [];

// Game indicator which slides across the bottom
var game_ind = new PIXI.DisplayObjectContainer();

// This array is told hold to the objects contained 
// within the game indicator 
var game_ind_elements = [];
	
//  Array to store the game buttons 
var game_button_array = [];
	
// Create an array to store stage objects such as the animating dice
var stage_elements = [];
	
// Create an array to store the object labels 
var object_label_array = [];
	
// Create an array to store the hover/touch label values
var label_vals = [];
	
// Array to hold all the odds textboxes 
// Use this to control the odds display 
// when a user switches between fractional
// and decimal odds display 
var stage_odds_text_control_arr = [];
	
// Set the co-ordinates for the sensorplates
var sensor_positions;
	
// Co-ordinates for the dice placement on the indicator
var dice_coords;
	
// Chip positions
var chip_positions;
	
// Chip selector positions
var chip_sel_pos;
	
// Points history
var points_history_box = [];

// Points history data
var points_history_data = ["-","-","-","-","-","-","-","-","-"];

// Points history position
var points_history_pos = [];

// Show/hide points history flag
var showHidePHFlag = true;

// Main game button (Roll dice) positions
var main_game_button_pos = [];

// Game background skin position
var game_bg_skin_pos = [];

// Court,field,pitch position
var game_court_pos = [];

// Control panel
var control_panel;

// Control panel positioning
var control_panel_position = [];

// Control panel control array
var control_panel_array = [];

// Clear, undo and repeat bet button positions
var bet_btn_positions = [];

// Clear, undo and repeat bet button interactive control array
var bet_btn_interactive_control = [];

// Stage logo
var stage_logo;

// Stage logo position
var stage_logo_position = [];

// Dice outcome
var dice_result_array = [];

// Dice outcome positions
var dice_outcome_positions = [];

// Dice outcome skin
var dice_result_skin;

// Help content open flag 
var help_content = false;

// Help content default buffer
var help_content_default_buffer = 16;

// Hide intro switch
// false will show intro
var hide_intro_switch = true;

// Bonus screen y factor
// These are used to place the bonus screen in the top or 
// bottom half of the screen depending on which side won
var bonusScreenYFactorReceiver,bonusScreenYFactorServer;

// Switch to determine if the notifier is open or closed
var notice_switch = false;

/*********************************************
	Store the stage text
*********************************************/

var current_stage_text;

/*********************************************
	Meters formally topnav
**********************************************/

// Control array
var meter_control_array = [];

// Meter container
var meter_container;

// Meter container position
var meter_container_positions = [];

// Set a var to store the meter container width
var meter_container_width;

// Trophy icon
var trophy_icon1,trophy_icon2,trophy_icon3;

/*********************************************
	Skins
**********************************************/
	
// Court skin vars
var court_skin,court_skin_left_hl,court_skin_mid_hl,court_skin_right_hl;
	
// Animated dice skins
var dice1,dice2,dice3;
var dice1_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowONE_R1/ThrowONE_R1_04.png");
var dice2_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTWO_R1/ThrowTWO_R1_04.png");
var dice3_skin = PIXI.Texture.fromImage("game/images/DiceAnim/ThrowTHREE_R4/ThrowTHREE_R4_04.png");
var animated_dice = [dice1,dice2,dice3];
	
// GDK menu btn skin
var gdk_menu_btn_skin = PIXI.Texture.fromImage("game/images/MenuBtn.png");

// Info button skin
var info_btn_skin = PIXI.Texture.fromImage("game/images/InfoBtn.png");
	
// Game background skin
var bg_stage_skin;
	
// Indicator text background
var ind_txt_bg_s_l = PIXI.Texture.fromImage("game/images/Stage_txt_bg_01.png");
var ind_txt_bg_s_c = PIXI.Texture.fromImage("game/images/Stage_txt_bg_02.png");
var ind_txt_bg_s_r = PIXI.Texture.fromImage("game/images/Stage_txt_bg_03.png");
	
// Main game button
var game_ind_button_up_skin,game_ind_button_hover_skin,game_ind_button_down_skin,game_ind_button_dice_rolling,game_ind_button_up_po_skin,game_ind_button_hover_po_skin,game_ind_button_down_po_skin;
	
// Chips
var chipskin1 = PIXI.Texture.fromImage("game/images/Chips/1_down.png");
var chipskin2 = PIXI.Texture.fromImage("game/images/Chips/2_down.png");
var chipskin5 = PIXI.Texture.fromImage("game/images/Chips/5_down.png");
var chipskin10 = PIXI.Texture.fromImage("game/images/Chips/10_down.png");
var chipskin25 = PIXI.Texture.fromImage("game/images/Chips/25_down.png");
var chipskin50 = PIXI.Texture.fromImage("game/images/Chips/50_down.png");
	
var skin_array = [];
		
skin_array.push(chipskin1);
skin_array.push(chipskin2);
skin_array.push(chipskin5);
skin_array.push(chipskin10);
skin_array.push(chipskin25);
skin_array.push(chipskin50);
	
// Create the skins and sprites for the arrows
var hover_label_arrow_left_skin = PIXI.Texture.fromImage("game/images/Arrow_left.png");	
var hover_label_arrow_right_skin = PIXI.Texture.fromImage("game/images/Arrow_right.png");
var hover_label_arrow_up_skin = PIXI.Texture.fromImage("game/images/Arrow_up.png");
var hover_label_arrow_down_skin = PIXI.Texture.fromImage("game/images/Arrow_down.png");

// Meters
var meter_skin_left,meter_skin_center,meter_skin_right;

// Control panel
var control_panel_skin = PIXI.Texture.fromImage("game/images/Control_panel_bg.png");	

// Points history background skin
var ph_bg_skin;

// Stage logo skin
var stage_logo_skin;

// Blank image used for dice result
var blank_skin = PIXI.Texture.fromImage("game/images/blank.png");

// Dice result background
var dice_result_bg;
	
// Sensor dice icons
var sensor_0_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice3.png"); 
var sensor_1_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice2.png"); 
var sensor_2_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice1.png"); 
var sensor_3_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice1.png"); 
var sensor_4_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice2.png"); 
var sensor_5_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice3.png"); 
var sensor_6_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice1.png"); 
var sensor_7_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice6.png"); 
var sensor_8_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice4.png"); 
var sensor_9_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice5.png"); 
var sensor_10_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice6.png"); 
var sensor_11_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice6.png"); 
var sensor_12_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice5.png"); 
var sensor_13_dice_icon = PIXI.Texture.fromImage("game/images/StageDice/Stage-dice4.png");

// Trophy icon
var trophy_icon_skin = PIXI.Texture.fromImage("game/images/Trophy_Icon.png");

/*************************************
	Game font style definitions
*************************************/
	
// Hover label font styles
var label_font_style;
var left_label_ypos;
	
// Text for the main game button
var game_ind_font_style,game_ind_font_po_style;
	
// Stage info text
var stage_info_font_style;
	
// Stage odds label font styles
var sl_label_font,sl_odds_font,slwordWrapWidth;
	
// Points history font styles
var ph_title_font,ph_text_font,t_col,sc_col,wordWrapWidth;
	
// Meter font styles
var meter_label,meter_text;
	
// Font type vars
var comfortaa,exo,varela,arial;
	
// Game button text such as clear, undo and repeat bet
var btn_text_font_style;

// Dice result font style
var dice_result_fs;

// Dice result skins
var result_dice_1, result_dice_2, result_dice_3, result_dice_4, result_dice_5, result_dice_6;

// Set a ratio for foreign language text which has wide characters
var font_size_ratio;
	
/*********************************************
	Game settings function
**********************************************/
// This method is required to set any varlues
// which require winWidth to be set.
// winWidth is set in the game.js file because
// that is where we can determine if the device 
// is a desktop, mobile or tablet.
function gameSettings(w,h)
{
	// Set the width and height values for the game	
	if(game_properties.device=="Desktop")
		winXfactor = w;
		
	if(game_properties.device=="Tablet")
		winXfactor = w-25;
		
	if(game_properties.device=="Mobile")
		winXfactor = w-40;
	
	// Set the winYfactor
	winYfactor = h;
	
	// Preset chip positions
	chip_positions = [117, 168, winXfactor-325, winYfactor+250, winXfactor-325, winYfactor+250, winXfactor-325, winYfactor+250, winXfactor-325, winYfactor+250,winXfactor-325, winYfactor+250 ];	
	
	/***********************************************
		Labels
	***********************************************/
	
	sensor_labels = [GDK.locale.get("g_sensor_0_odds_label"),GDK.locale.get("g_sensor_1_odds_label"),GDK.locale.get("g_sensor_2_odds_label"),GDK.locale.get("g_sensor_3_odds_label"),GDK.locale.get("g_sensor_4_odds_label"),GDK.locale.get("g_sensor_5_odds_label"),GDK.locale.get("g_sensor_6_odds_label"),GDK.locale.get("g_sensor_7_odds_label"),GDK.locale.get("g_sensor_8_odds_label"),GDK.locale.get("g_sensor_9_odds_label"),GDK.locale.get("g_sensor_10_odds_label"),GDK.locale.get("g_sensor_11_odds_label"),GDK.locale.get("g_sensor_12_odds_label"),GDK.locale.get("g_sensor_13_odds_label")];
	
	/**********************************************
		Placement properties for various devices
	**********************************************/
	
	// Determine if this device has a wide screen but a short height
	if((winWidth>1100) && (winHeight<815))
	{	
		// Set meter layout property
		game_properties.meterlayout = "v";
		
		// Edit winXfactor to move all objects to the left so 
		// we can squeeze the meters down the right hand side
		
		winXfactor -= 100;
		
		/*
			Placement properties
		*/
		
		// Points history positioning
		points_history_pos = [winXfactor-210,-14];
		// Clear, undo and repeat bet positions
		bet_btn_positions = [23,216,23,264,23,216];
		// Control panel positioning
		control_panel_position = [winXfactor-550,20];
		// Positioning of the GDK menu and info buttons
		menu_btn_positions = [winXfactor-550,winYfactor-120,winXfactor-485,winYfactor-120];
		// Chip selector positions
		chip_sel_pos = [157,143,21,143];
		// Main game button position (Roll dice)
		main_game_button_pos = [114,15];
		// Meter container position
		meter_container_positions = [winWidth-250,10];
		// Game background skin position
		game_bg_skin_pos = [winXfactor+7.5,-100];
		// Court,field,pitch position
		game_court_pos = [winXfactor,125];
		// Stage logo position
		stage_logo_position = [winXfactor+9,-14];
		// Dice outcome
		dice_outcome_positions = [winXfactor-550,345,winXfactor-550,400,winXfactor-550,455];
		// Starting co-ordinates for the 3 main dice
		dice_coords = [winXfactor-250,50,winXfactor-350,40,winXfactor-100,60];
		// Bonus screen y factor
		bonusScreenYFactorReceiver = (winYfactor/2)-120;
		bonusScreenYFactorServer = (winYfactor/2)-45;
	
		/*********************************************
			Sensors
		*********************************************/
					
		// Set the co-ordinates for the sensors
		sensor_positions = [winXfactor-235,165,
							winXfactor-242,211,
							winXfactor-249,260,
							winXfactor-268,386,
							winXfactor-279,450,
							winXfactor-291,518,
							winXfactor,206,
							winXfactor,445,
							winXfactor+240,165,
							winXfactor+247,211,
							winXfactor+254,260,
							winXfactor+272,386,
							winXfactor+281,450,
							winXfactor+293,518
							];
		
	} else {
		
		// Set meter layout property
		game_properties.meterlayout = "h";
		
		// Check if the device is mobile, tablet or desktop
		switch(game_properties.device)
		{
			case "Desktop":
				// Points history positioning
				points_history_pos = [winXfactor-212,101];
				// Clear, undo and repeat bet positions
				bet_btn_positions = [23,216,23,264,23,216];
				// Control panel positioning
				control_panel_position = [winXfactor-800,100];
				// Positioning of the GDK menu and info buttons
				menu_btn_positions = [winXfactor-800,winYfactor-150,winXfactor-735,winYfactor-150];
				// Chip selector positions
				chip_sel_pos = [157,143,21,143];
				// Main game button position (Roll dice)
				main_game_button_pos = [114,15];
				// Meter container position
				meter_container_positions = [winXfactor-350,winYfactor-150];
				// Game background skin position
				game_bg_skin_pos = [winXfactor+7.5,15];
				// Court,field,pitch position
				game_court_pos = [winXfactor,240];
				// Stage logo position
				stage_logo_position = [winXfactor+9,101];
				// Dice outcome
				dice_outcome_positions = [winXfactor-800,420,winXfactor-800,475,winXfactor-800,530];
				// Starting co-ordinates for the 3 main dice
				dice_coords = [winXfactor-250,140,winXfactor-350,150,winXfactor-25,160];	
				// Bonus screen y factor
				bonusScreenYFactorReceiver = 430;
				bonusScreenYFactorServer = 390;
				
				/*********************************************
					Sensors
				*********************************************/
					
				// Set the co-ordinates for the sensors
				sensor_positions = [winXfactor-235,280,
									winXfactor-242,326,
									winXfactor-249,375,
									winXfactor-268,501,
									winXfactor-279,565,
									winXfactor-291,633,
									winXfactor,321,
									winXfactor,560,
									winXfactor+240,280,
									winXfactor+247,326,
									winXfactor+254,375,
									winXfactor+272,501,
									winXfactor+281,565,
									winXfactor+293,633
									];	
			break;
			case "Tablet":
				// Points history positioning
				points_history_pos = [winXfactor-210,-14];
				// Clear, undo and repeat bet positions
				bet_btn_positions = [23,216,23,264,23,216];
				// Control panel positioning
				control_panel_position = [winXfactor-620,20];
				// Positioning of the GDK menu and info buttons
				menu_btn_positions = [winXfactor-620,winYfactor-75,winXfactor-555,winYfactor-75];
				// Chip selector positions
				chip_sel_pos = [157,143,21,143];
				// Main game button position (Roll dice)
				main_game_button_pos = [114,15];
				// Meter container position
				meter_container_positions = [winXfactor-350,winYfactor-100];
				// Game background skin position
				game_bg_skin_pos = [winXfactor+7.5,-100];
				// Court,field,pitch position
				game_court_pos = [winXfactor,125];
				// Stage logo position
				stage_logo_position = [winXfactor+9,-14];
				// Dice outcome
				dice_outcome_positions = [winXfactor-620,345,winXfactor-620,400,winXfactor-620,455];
				// Starting co-ordinates for the 3 main dice
				dice_coords = [winXfactor-250,50,winXfactor-350,40,winXfactor-100,60];
				// Bonus screen y factor
				bonusScreenYFactorReceiver = (winYfactor/2);
				bonusScreenYFactorServer = (winYfactor/2)-45;
	
				/*********************************************
					Sensors
				*********************************************/
					
				// Set the co-ordinates for the sensors
				sensor_positions = [winXfactor-235,165,
									winXfactor-242,211,
									winXfactor-249,260,
									winXfactor-268,386,
									winXfactor-279,450,
									winXfactor-291,518,
									winXfactor,206,
									winXfactor,445,
									winXfactor+240,165,
									winXfactor+247,211,
									winXfactor+254,260,
									winXfactor+272,386,
									winXfactor+281,450,
									winXfactor+293,518
									];	
			break;
			case "Mobile":
				// Points history positioning
				points_history_pos = [winXfactor-210,-14];
				// Clear, undo and repeat bet positions
				bet_btn_positions = [23,216,23,264,23,216];
				// Control panel positioning
				control_panel_position = [winXfactor-590,20];
				// Positioning of the GDK menu and info buttons
				menu_btn_positions = [winXfactor-590,winYfactor-100,winXfactor-515,winYfactor-100];
				// Chip selector positions
				chip_sel_pos = [157,143,21,143];
				// Main game button position (Roll dice)
				main_game_button_pos = [114,15];
				// Meter container position
				meter_container_positions = [winXfactor-350,winYfactor-100];
				// Game background skin position
				game_bg_skin_pos = [winXfactor+7.5,-100];
				// Court,field,pitch position
				game_court_pos = [winXfactor,125];
				// Stage logo position
				stage_logo_position = [winXfactor+9,-14];
				// Dice outcome
				dice_outcome_positions = [winXfactor-590,345,winXfactor-590,400,winXfactor-590,455];
				// Starting co-ordinates for the 3 main dice
				dice_coords = [winXfactor-250,50,winXfactor-350,40,winXfactor-100,60];
				// Bonus screen y factor
				bonusScreenYFactorReceiver = (winYfactor/2)+35;
				bonusScreenYFactorServer = (winYfactor/2)-45;
		
				/*********************************************
					Sensors
				*********************************************/
					
				// Set the co-ordinates for the sensors
				sensor_positions = [winXfactor-235,165,
									winXfactor-242,211,
									winXfactor-249,260,
									winXfactor-268,386,
									winXfactor-279,450,
									winXfactor-291,518,
									winXfactor,206,
									winXfactor,445,
									winXfactor+240,165,
									winXfactor+247,211,
									winXfactor+254,260,
									winXfactor+272,386,
									winXfactor+281,450,
									winXfactor+293,518
									];	
			break;
		}
	}
}	