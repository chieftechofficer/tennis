function stage3()
{		

	// Use this to populate the points history box
	var _winning_value = "-";
	
	// 0 = Sensor 7
	if(engine_dice_result[1]==6)
	{	
		// Determine which dice should be rolled
		// There are 3 options here
		
		/////////// Stage 3 - Bottom side
		// 4 = Sensor 13 * 3
		// 5 = Sensor 12 * 2
		// 6 = Sensor 11 * 1
				
		// Check which side of the dice is active then
		// 0 = Sensor 13
		if(engine_dice_result[3]==4)
		{
			label_vals[0] = sensor_positions[13*2];
			label_vals[1] = sensor_positions[13*2+1];
			label_vals[2] = 13;
			selectedSensor(13);
			_winning_value = GDK.locale.get("g_sensor_13_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[11] = 0;
			sensor_switch[12] = 0;
		}
		// 1 = Sensor 12
		if(engine_dice_result[3]==5)
		{
			label_vals[0] = sensor_positions[12*2];
			label_vals[1] = sensor_positions[12*2+1];
			label_vals[2] = 12;
			selectedSensor(12);
			_winning_value = GDK.locale.get("g_sensor_12_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[11] = 0;
			sensor_switch[13] = 0;
		} 
		// 2 = Sensor 11
		if(engine_dice_result[3]==6)
		{
			label_vals[0] = sensor_positions[11*2];
			label_vals[1] = sensor_positions[11*2+1];
			label_vals[2] = 11;
			selectedSensor(11);
			_winning_value = GDK.locale.get("g_sensor_11_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[12] = 0;
			sensor_switch[13] = 0;
		} 		
		// Switch sensors off
		switchsensorsoff();
		
		/////////// Stage 3 - Bottom side
		// 4 = Sensor 13 * 3
		// 5 = Sensor 12 * 2
		// 6 = Sensor 11 * 1
		
		if(sensor_values[26]>=1 && engine_dice_result[3]==4) // <-- Sensor 13
		{
			// Calculate winnings
			winning_sensor_switch[13]=1;
			setWinningSkin(13);

			// Set the stage win
			addStageWin(stage_wins[2]);		
		}		
		if(sensor_values[24]>=1 && engine_dice_result[3]==5) // <-- Sensor 12
		{
			// Calculate winnings
			winning_sensor_switch[12]=1;
			setWinningSkin(12);

			// Set the stage win
			addStageWin(stage_wins[2]);
		}
		if(sensor_values[22]>=1 && engine_dice_result[3]==6) // <-- Sensor 11
		{
			// Calculate winnings
			winning_sensor_switch[11]=1;
			setWinningSkin(11);

			// Set the stage win
			addStageWin(stage_wins[2]);
		}
	}
	// 1 = Sensor 6
	if(engine_dice_result[1]==1)
	{

		/////////// Stage 3 - Top side
		// 4 = Sensor 8 * 3
		// 5 = Sensor 9 * 2
		// 6 = Sensor 10 * 1
				
		// Check which side of the dice is active then				
		// 4 = Sensor 8
		if(engine_dice_result[3]==4)
		{
			label_vals[0] = sensor_positions[8*2];
			label_vals[1] = sensor_positions[8*2+1];
			label_vals[2] = 8;
			selectedSensor(8);
			_winning_value = GDK.locale.get("g_sensor_8_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[9] = 0;
			sensor_switch[10] = 0;
		}
		// 5 = Sensor 9
		if(engine_dice_result[3]==5)
		{
			label_vals[0] = sensor_positions[9*2];
			label_vals[1] = sensor_positions[9*2+1];
			label_vals[2] = 9;
			selectedSensor(9);
			_winning_value = GDK.locale.get("g_sensor_9_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[8] = 0;
			sensor_switch[10] = 0;
		} 
		// 6 = Sensor 10
		if(engine_dice_result[3]==6)
		{
			label_vals[0] = sensor_positions[10*2];
			label_vals[1] = sensor_positions[10*2+1];
			label_vals[2] = 10;
			selectedSensor(10);
			_winning_value = GDK.locale.get("g_sensor_10_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[8] = 0;
			sensor_switch[9] = 0;
		}
		
		// Switch sensors off
		switchsensorsoff();
		
		/////////// Stage 3 - Top side
		// 4 = Sensor 8 * 3
		// 5 = Sensor 9 * 2
		// 6 = Sensor 10 * 1
		
		if(sensor_values[16]>=1 && engine_dice_result[3]==4) // <-- Sensor 8
		{	
			// Calculate winnings
			winning_sensor_switch[8]=1;
			setWinningSkin(8);
					
			// Set the stage win
			addStageWin(stage_wins[2]);
		}
		if(sensor_values[18]>=1 && engine_dice_result[3]==5) // <-- Sensor 9
		{
			// Calculate winnings
			winning_sensor_switch[9]=1;
			setWinningSkin(9);
			
			// Set the stage win
			addStageWin(stage_wins[2]);		
		}
		if(sensor_values[20]>=1 && engine_dice_result[3]==6) // <-- Sensor 10
		{
			// Calculate winnings
			winning_sensor_switch[10]=1;		
			setWinningSkin(10);

			// Set the stage win
			addStageWin(stage_wins[2]);					
		}
	} 
	
	if(stage_wins[2]>0){
		// Play winning Chyme
		GDK.audio.play("sensorwin");
	} else {
		GDK.audio.play("sensorloose");	
	}
		
}