/********************************************
	Initiate game
********************************************/

function init(w,h)
{
	/**********************************************
		Locale
	**********************************************/
	
	locale = GDK.locale.code();
		
	/**********************************************
		Check device and touch settings
	**********************************************/
	
	CheckDeviceAndTouch();
	
	/*********************************************
		Game settings
	**********************************************/
	
	gameSettings(w,h);
		
	// Call the scale ratio method 
	// This will set the device property which will 
	// scale/fit/organise all the visual elements to 
	// fit accordingly
	scaleRatio();
	
	/******************************************
		Set the font styles
	******************************************/
	
	setFontStyles();
	
	/******************************************
		Skins
	******************************************/
	
	setAllSkins();
	
	/******************************************
		Game elements
	******************************************/
		
	// Set up the stage
	setupStage();
	
		
	// Create the scoreboard
	// We do not know if this will work. Must ask Odobo about info so leave it out for now.
	//if(game_properties.device=="Desktop")
		createPointsHistory();
		createStageLogo(stage_logo_position[0],stage_logo_position[1]);
	
	// Setup control panel
	setupControlPanel();
	
	// Setup the GDK menu and info btns
	setupMenuInfoBtns();
		
	// Setup the game chips
	setupGameChips(chip_positions[0],chip_positions[1],chip_values[0]); // Chip 1
	setupGameChips(chip_positions[2],chip_positions[3],chip_values[1]); // Chip 2
	setupGameChips(chip_positions[4],chip_positions[5],chip_values[2]); // Chip 5
	setupGameChips(chip_positions[6],chip_positions[7],chip_values[3]); // Chip 10
	setupGameChips(chip_positions[8],chip_positions[9],chip_values[4]); // Chip 25
	setupGameChips(chip_positions[10],chip_positions[11],chip_values[5]); // Chip 50
		
	// Setup chip selectors
	setupChipSelector(chip_sel_pos[0],chip_sel_pos[1],"up");
	setupChipSelector(chip_sel_pos[2],chip_sel_pos[3],"down");
		
	// Setup sensors
	for (var i=0; i < 14; i++) 
	{	
		createsensorplate(sensor_positions[i*2], sensor_positions[i*2 + 1], i);
	}
		
	// Setup stack containers
	for (var ij=0; ij < 14; ij++)
	{	
		createstackcontainer(sensor_positions[ij*2], sensor_positions[ij*2 + 1], ij);
	}
	
	/*
		Stage labels
	*/
	
	// Create a container for the stage odds label text
	var _stage_label_container = new PIXI.DisplayObjectContainer();
	stage_elements.push(_stage_label_container); // Stage element 0
	stage.addChild(_stage_label_container);
	
	// Add stage odds labels
	if(GDK.settings.game.get("labelOddsDisplay"))
	{
		setupStageLabels("d");
	} else {
		setupStageLabels("f");
	}
	
	/*
		Trophy icons
		One for each stage
	*/
	
	trophy_icon1 = new PIXI.Sprite(trophy_icon_skin);
	trophy_icon1.position.x = -5000;
	trophy_icon1.position.y = -5000;
	trophy_icon1.anchor.x = 0.5;
	trophy_icon1.anchor.y = 0.67;
	trophy_icon1.scale.x = trophy_icon1.scale.y = 0.3;
	stage.addChild(trophy_icon1);
	
	trophy_icon2 = new PIXI.Sprite(trophy_icon_skin);
	trophy_icon2.position.x = -5000;
	trophy_icon2.position.y = -5000;
	trophy_icon2.anchor.x = 0.5;
	trophy_icon2.anchor.y = 0.67;
	trophy_icon2.scale.x = trophy_icon2.scale.y = 0.3;
	stage.addChild(trophy_icon2);
	
	trophy_icon3 = new PIXI.Sprite(trophy_icon_skin);
	trophy_icon3.position.x = -5000;
	trophy_icon3.position.y = -5000;
	trophy_icon3.anchor.x = 0.5;
	trophy_icon3.anchor.y = 0.67;
	trophy_icon3.scale.x = trophy_icon3.scale.y = 0.3;
	stage.addChild(trophy_icon3);
		
	/*
		Add the main game button
	*/
	
	createMainGameButton(main_game_button_pos[0],main_game_button_pos[1]);
		
	/*
		Setup clear, undo and repeat buttons
	*/
	
	controlBtn(1,bet_btn_positions[0],bet_btn_positions[1]); // Undo
	controlBtn(2,bet_btn_positions[2],bet_btn_positions[3]); // Clear all
	controlBtn(3,bet_btn_positions[4],bet_btn_positions[5]); // Repeat
	
	/* 
		Hide the clear, undo and repeat bet
	*/
	
	hideclearundo();
	hideRepeatBet();
	
	// New dice function
	createRollingDice();
	
	/* 
		Setup meter container
	*/
	
	createMeterContainer(meter_container_positions[0],meter_container_positions[1]);
	
	/* 
		Setup meters	
	*/
	
	createMeter(0,0,GDK.locale.get("g_topbar_balance") + "(" + GDK.locale.getCurrency() + ")", GDK.locale.formatCurrency(balance, true));
	createMeter(0,0,GDK.locale.get("g_topbar_stake"), "00.00");
	createMeter(0,0,GDK.locale.get("g_topbar_winnings"), "00.00");
	
	/*
		Resize and position meters accordingly
	*/
	
	meterAdjust(5,0);
	meterAdjust(11,1);
	meterAdjust(17,2);
	
	/* 
		Create the placeholders for the dice outcome
	*/
	
	createPlaceholder(dice_outcome_positions[0],dice_outcome_positions[1]);
	createPlaceholder(dice_outcome_positions[2],dice_outcome_positions[3]);
	createPlaceholder(dice_outcome_positions[4],dice_outcome_positions[5]);
		
	console.log(GDK.user.screenName());
		
	// Play welcome message
	//playSound(s_welcome,s_welcome);
	
	/*
		Open intro
	*/
	if(!hide_intro_switch)
	{
		$( "#screen_overlay" ).removeClass( "hidden" );	
		
		// Indicate that the help content is open
		help_content = true;	
		
		$( "#closeBtn" ).html("Skip intro");
	}
	
}
