/***********************************
	Launch game dice
***********************************/

function launchDice()
{	

	animDice();

	/*********************************************
		Setup the stage and configure elements
	*********************************************/
	
	// Save the repeat bet data
	saveCurrentBet();
	
	// Stop all sounds						
	stopVoiceovers();
	stopGameSounds();
							
	// Hide poinst history
	showStageLogo();
							
	// Disable the sensors
	disableSensors();
							
	// Hide the player notifier
	hideNotice();
	
	// Hide the clear and undo button
	hideclearundo();
	
	// hidetablechips(); <-- not sure about this so delete if not needed
	hideLabels();
	
	// Set the game status var to rolling
	// This tells the game that the dice
	// are busy animating in or specifically
	// rolling onto the stage
	game_status = "rolling";
	
	// Set the main game button to show that the dice is rolling
	game_ind_elements[0].setTexture(game_ind_button_dice_rolling);
	
	// Deactivate the button so it cannot be pushed 
	// while the dice are rolling
	game_ind_elements[0].buttonMode = false;
	game_ind_elements[0].interactive = false;
	
	// Set the text to dice rolling on the game button
	setGameBtnTxt("");
}

/*********************************************
	Generate dice results
*********************************************/

function generateDiceResults(st)
{

}