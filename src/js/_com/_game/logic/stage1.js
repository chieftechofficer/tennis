function stage1()
{
	// Switch off sensors
	if(engine_dice_result[1]==6)
	{
		label_vals[0] = sensor_positions[7*2];
		label_vals[1] = sensor_positions[7*2+1];
		label_vals[2] = 7;
		selectedSensor(7);
		_winning_value = GDK.locale.get("g_sensor_7_odds_label");
		// Set the sensors for this side of the playing area to off
		sensor_switch[0] = 0;
		sensor_switch[1] = 0;
		sensor_switch[2] = 0;
		sensor_switch[6] = 0;
		sensor_switch[8] = 0;
		sensor_switch[9] = 0;
		sensor_switch[10] = 0;
	}
					
	// 1 = Top side
	if(engine_dice_result[1]==1)
	{
		label_vals[0] = sensor_positions[6*2];
		label_vals[1] = sensor_positions[6*2+1];
		label_vals[2] = 6;
		selectedSensor(6);
		_winning_value = GDK.locale.get("g_sensor_6_odds_label");
		// Set the sensors for this side of the playing area to off
		sensor_switch[3] = 0;
		sensor_switch[4] = 0;
		sensor_switch[5] = 0;
		sensor_switch[7] = 0;
		sensor_switch[11] = 0;
		sensor_switch[12] = 0;
		sensor_switch[13] = 0;
	}
	
	// Switch sensors off
	switchsensorsoff();
	
	/////////// Stage 1 - Both sides
	// 6 = Sensor 7 * 3
	// 1 = Sensor 6 * 3
			
	if(sensor_values[12]>=1 && engine_dice_result[1]==1)
	{
		// Calculate winnings
		winning_sensor_switch[6]=1;			
		setWinningSkin(6);
				
		// Set the stage win
		addStageWin(stage_wins[0]);
	}
			
	if(sensor_values[14]>=1 && engine_dice_result[1]==6)
	{	
		winning_sensor_switch[7]=1;
		setWinningSkin(7);

		// Set the stage win
		addStageWin(stage_wins[0]);	
	}
	
	if(stage_wins[0]>0)
	{
		// Play winning Chyme
		GDK.audio.play("sensorwin");
	} else {
		GDK.audio.play("sensorloose");
	}
	
}