function stage2()
{
	// Use this to populate the points history box
	var _winning_value = "-";
			
	// Determine which dice should be rolled
	// There are 3 options here 
	// 0 = Bottom side
	if(engine_dice_result[1]==6)
	{	
		
		/////////// Stage 2 - Bottom side
		// 3 = Sensor 5 * 3
		// 2 = Sensor 4 * 2
		// 1 = Sensor 3 * 1

		// Check which side of the dice is active then
		// 0 = Sensor 5
		if(engine_dice_result[2]==3)
		{
			label_vals[0] = sensor_positions[5*2];
			label_vals[1] = sensor_positions[5*2+1];
			label_vals[2] = 5;
			selectedSensor(5);
			_winning_value = GDK.locale.get("g_sensor_5_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[3] = 0;
			sensor_switch[4] = 0;
		}
		// 1 = Sensor 4
		if(engine_dice_result[2]==2)
		{
			label_vals[0] = sensor_positions[4*2];
			label_vals[1] = sensor_positions[4*2+1];
			label_vals[2] = 4;
			selectedSensor(4);
			_winning_value = GDK.locale.get("g_sensor_4_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[3] = 0;
			sensor_switch[5] = 0;
		} 		
		// 2 = Sensor 3
		if(engine_dice_result[2]==1)
		{
			label_vals[0] = sensor_positions[3*2];
			label_vals[1] = sensor_positions[3*2+1];
			label_vals[2] = 3;
			selectedSensor(3);
			_winning_value = GDK.locale.get("g_sensor_3_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[4] = 0;
			sensor_switch[5] = 0;
		} 
				
		// Switch sensors off
		switchsensorsoff();
		
		/////////// Stage 2 - Bottom side
		// 3 = Sensor 5 * 3
		// 2 = Sensor 4 * 2
		// 1 = Sensor 3 * 1
		
		if(sensor_values[6]>=1 && engine_dice_result[2]==1) // <-- Sensor 3
		{
			// Calculate winnings
			winning_sensor_switch[3]=1;
			setWinningSkin(3);
					
			// Set the stage win
			addStageWin(stage_wins[1]);					
		}
		if(sensor_values[8]>=1 && engine_dice_result[2]==2) // <-- Sensor 4
		{
			// Calculate winnings
			winning_sensor_switch[4]=1;
			setWinningSkin(4);
					
			// Set the stage win
			addStageWin(stage_wins[1]);
		}
		if(sensor_values[10]>=1 && engine_dice_result[2]==3) // <-- Sensor 5
		{
			// Calculate winnings
			winning_sensor_switch[5]=1;
			setWinningSkin(5);

			// Set the stage win
			addStageWin(stage_wins[1]);	
		}			
	}
	// 1 = Top side
	if(engine_dice_result[1]==1)
	{			
		
		/////////// Stage 2 - Top side
		// 3 = Sensor 0 * 3
		// 2 = Sensor 1 * 2
		// 1 = Sensor 2 * 1
		
		// Check which side of the dice is active then
		// 1 = Sensor 0
		if(engine_dice_result[2]==3)
		{	
			label_vals[0] = sensor_positions[0*2];
			label_vals[1] = sensor_positions[0*2+1];
			label_vals[2] = 0;
			selectedSensor(0);
			_winning_value = GDK.locale.get("g_sensor_0_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[1] = 0;
			sensor_switch[2] = 0;
		}		
		// 2 = Sensor 1
		if(engine_dice_result[2]==2)
		{
			label_vals[0] = sensor_positions[1*2];
			label_vals[1] = sensor_positions[1*2+1];
			label_vals[2] = 1;
			selectedSensor(1);
			_winning_value = GDK.locale.get("g_sensor_1_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[0] = 0;
			sensor_switch[2] = 0;
		} 
		// 3 = Sensor 2
		if(engine_dice_result[2]==1)
		{
			label_vals[0] = sensor_positions[2*2];
			label_vals[1] = sensor_positions[2*2+1];
			label_vals[2] = 2;
			selectedSensor(2);
			_winning_value = GDK.locale.get("g_sensor_2_odds_label");
			// Set the sensors for this side of the playing area to off
			sensor_switch[0] = 0;
			sensor_switch[1] = 0;
		} 
				
		// Switch sensors off
		switchsensorsoff();
		
		/////////// Stage 2 - Top side
		// 3 = Sensor 0 * 3
		// 2 = Sensor 1 * 2
		// 1 = Sensor 2 * 1
				
		if(sensor_values[0]>=1 && engine_dice_result[2]==3) // <-- Sensor 0
		{
			// Calcualte winnings
			winning_sensor_switch[0]=1;
			setWinningSkin(0);
					
			// Set the stage win
			addStageWin(stage_wins[1]);	
		}
		if(sensor_values[2]>=1 && engine_dice_result[2]==2) // <-- Sensor 1
		{
			// Calcualte winnings
			winning_sensor_switch[1]=1;	
			setWinningSkin(1);
					
			// Set the stage win
			addStageWin(stage_wins[1]);	
		}
		if(sensor_values[4]>=1 && engine_dice_result[2]==1) // <-- <-- Sensor 2
		{
			// Calcualte winnings
			winning_sensor_switch[2]=1;	
			setWinningSkin(2);
					
			// Set the stage win
			addStageWin(stage_wins[1]);	
		}			
	} 
	
	if(stage_wins[1]>0){
		// Play winning Chyme
		GDK.audio.play("sensorwin");
	} else {
		GDK.audio.play("sensorloose");
	}
	
}