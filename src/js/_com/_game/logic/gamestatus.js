
/********************************************
	Calculate total playable stages
********************************************/

function playableStages(s)
{
	for(i=0;i<s.length;i++)
	{
		if(s[i]!="-")
		{
			playable_stages+=1;	
		}
	}
}

/********************************************
	Game over 
********************************************/

function gameOver()
{
	// Set the game status to over
	game_status = "over";
	games_played++;
			
	// Remove all the chips from the stage
	for (var p=0; p <= stacked_chips_index.length; p++)
	{
		if(stacked_chips[p]){
			stacked_chips[p].alpha = 0;
		}
	}
	
	// Set the sensors who have not won to off
	for (var wss=0; wss <= winning_sensor_switch.length-1; wss++)
	{	
		if(winning_sensor_switch[wss]<1)
		{
			switchSensorOff(wss);
		}
	}
			
	// Reset Stake
	setStake(0);
		
	// Check if there is a bonus win
	bonusWin();
			
	// Set the main game button
	game_ind_elements[0].setTexture(game_ind_button_up_po_skin);
	game_ind_elements[0].buttonMode = true;
	game_ind_elements[0].interactive = true;
	game_ind_elements[1].setStyle(game_ind_font_po_style);
	game_ind_elements[1].position.x = game_ind_elements[1].position.x+10;
			
	// Set the main game button text to point over
	setGameBtnTxt(GDK.locale.get("g_main_button_point_over"));
			
	// Update the points history array
	//updatePointsHistoryArray(true);
	populatePointsHistory();
			
	// Show poinst history
	hideStageLogo();	
}

/*******************************************
	Bonus win feature
*******************************************/
// The bonus feature should only kick in if 
// the player has won bets on all 3 stages
// and the stake is equal on each stage
// the bonus win is 50% of the stake placed
function bonusWin()
{	
	// Set an array to store the stake values
	var _stake_values = [];
	
	// Bonus ratio
	var _bonus_ratio = 0;
	
	if(bonus)
	{	
		
			// Play applause
			GDK.audio.play("applause");
			
			// Play raining dice
			playBonusAnim(15);
			
			/*
				Calculate bonus win which is 50% of the stake on the winning chips
			*/
			
			var bwin = bonus_amount;
			
			/*
				Show bonus screen
				
			*/
			
			$("#notifierContent").html("Congratulations!<br>Bonus win of (" + GDK.locale.getCurrency() + ")" + GDK.locale.formatCurrency(bwin, true));
			
			var _pw = $( "#playerNotifier" ).width();
			var _ph = $( "#playerNotifier" ).height();
			
			$("#playerNotifier").css("left", (winXfactor-(_pw/2))-25 +"px");
			//$("#playerNotifier").css("left", "0px");
			
			// Determine which side has won the point
			// to set the bonus screen y factor
			
			// Bottom side
			if(engine_dice_result[1]==6)
			{	
				$("#playerNotifier").css("top", (bonusScreenYFactorServer - _ph) + "px");
			}
			
			// Top side
			if(engine_dice_result[1]==1)
			{	
				$("#playerNotifier").css("top", bonusScreenYFactorReceiver + "px");
			}
			
			//$("#playerNotifier").css("top", "0px");
			
			//$("#debuglog").append("<br>bonusScreenYFactorServer: " + bonusScreenYFactorServer + "<br> $( #playerNotifier ).height(): " + $( "#playerNotifier" ).height() );
			
			// Fade in the bonus screen
			$("#playerNotifier").fadeIn("slow").delay(10000).fadeOut("slow", function() {
    			clearBonusSprites();
				setWinnings(total_game_winnings);
			});
	}
}