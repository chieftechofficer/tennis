/*****************************************
	Odobo communication and all 
	things engine related
*****************************************/

/*
	Required to build the object sent when a 
	user pushes the main game button
*/
function createRoundStake(stage, plainStakes){	

	var roundStake = {
		roundName : stage,
		stakes : plainStakes
	};
	
	return roundStake;

};

var createStake = function(identifier, betAmount){
	//console.log("betAmount: " + betAmount);
	var stake = {
		identifier : identifier,
		betAmount : betAmount
	};
	return stake;
};

/********************************************
	Check current stage
********************************************/
// This determines in which stage this 
// particular sensor can be found

function checkStage(s)
{
	for(i=0;i<stage_groups.length;i++)
	{
		if(s==stage_groups[i*2])
		{
			return stage_groups[i*2+1];
		}
	}
}

/********************************************
	Get spot number
********************************************/

function spotNum(spot)
{
	var _num;

	_num = spot.replace("SPOT_","");

	return _num;
}

/*********************************************
	Send bet data
*********************************************/

function submitBetData()
{
	/*	Odobo
		Calculate available stages
	*/
							
	var rounds = [];
							
	var start = 0;
							
	for (var i = 0; i < availableStages.length; i++) 
	{
		var stage = availableStages[i];
								
		var _sensors = sensors_per_stage[i];
								
		var stakes = [];
								
		// Set the start of the loop to the 
		// Correct sensor start location
		start=sensor_loc_p_s[i][0];
								
		for (var j = start; j < start+_sensors; j++) 
		{
			var betForSpot = sensor_values[j*2];
									
			if (betForSpot != 0)
			{
				stakes.push(createStake(spots[j], sensor_values[j*2+1]));
			}
		}
								
		if (stakes.length > 0) 
		{
			rounds.push(createRoundStake(stage, stakes));
		}
	}		
			
	GDK.game.publish("sportshotz:BetAction", {roundStakes:rounds});
}