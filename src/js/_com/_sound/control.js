/////////////////////////////////////////////////////////////////////
//////////////////////////// Sound Library //////////////////////////
// Play function requires 2 objects
// 1st = the object to be played
// 2nd = the object to be stopped  

function playSound(s_play,s_stop)
{
}

// Mute all sounds
function muteAllSounds()
{	
}

// Unmute all sounds
function unmuteAllSounds()
{	
}

// Stop sound 
// Requires 1 object which is the sound to stop
function stopSound(s_stop)
{
}

function stopGameSounds()
{
}

// Stop all voiceovers
function stopVoiceovers()
{	
}

// Required sound properties
var s_sensor = "";
var s_you_win = "";

function playSoundWin(_win,_sel)
{
}