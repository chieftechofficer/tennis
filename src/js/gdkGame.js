function gdkGame(){

	//The callback always have this two parameters
	var handleEchoEvent = function(packet, control) {
		alert(packet.message);
		//This call is needed when you finish the handling of the event
		control.done();
	};

	document.getElementById('subscribeEvent').addEventListener('click', function(){
		//Subscribe to a Game Engine Event
		/** http://library.odobo.ws/docs/current/api.html#GDK.game.subscribe **/
		GDK.game.subscribe("echogame:EchoEvent", handleEchoEvent);
	});

	document.getElementById('publishAction').addEventListener('click', function(){
		//Publishing data to the game engine.
		/** http://library.odobo.com/docs/current/api.html#GDK.game.publish **/
		GDK.game.publish("echogame:EchoAction", {
			message: document.getElementById("message").value
		});
	});

	document.getElementById('unsubscribeEvent').addEventListener('click', function(){
		//Unsubscribe to a Game Engine Event
		/** http://library.odobo.com/docs/current/api.html#GDK.game.publish **/
		GDK.game.unsubscribe("echogame:EchoEvent", handleEchoEvent);
	});

	var assetListHtml = "";

	for(var i = 0; i < imageAssets.length ; i++){
		var assetRelativePath = imageAssets[i];
		/** http://library.odobo.com/docs/current/api.html#GDK.game.path **/
		var assetGlobalPath = GDK.game.path(assetRelativePath);
		assetListHtml += '<li>GDK.game.path("' + assetRelativePath + '") : "' + assetGlobalPath +'"</li>';
	}

	document.getElementById("assetsList").innerHTML = assetListHtml;
}