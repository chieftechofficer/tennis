#Odobo's Game Submission Repository  

Welcome to Odobo's Game Submission Repository.
This repository is used to store all game client code and relevant resources. It works in conjunction with the project's 
wiki page (`Confluence <https://odobolimited.atlassian.net/wiki>`_) and our tracker 
(`JIRA <https://odobolimited.atlassian.net>`_).
___
##Workflow 
The Odobo team is responsible for the maintenance of this repository. Jannio developers can fork the 
repository to create their own branches, test suites and more. Should you wish to include additional resources in this 
repository, please contact your Technical Support Manager (TSM) for assistance.

The general workflow for a developer is to work on their own local copies of the game client. Once a game client is 
ready for submission a pull request should be created with the TSM (Andrew Visser) and SDM (Reza Malik) as reviewers. 
This will trigger the code review and certification process with Odobo.
___
##Contents 
- The `config` folder contains game client configuration files, such as the gcds.json file which specifies host server 
connection details.
- The `src` folder contains all game client code.